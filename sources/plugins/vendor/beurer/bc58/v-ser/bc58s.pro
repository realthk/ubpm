TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets serialport
DEFINES		+= BC58
INCLUDEPATH	+= ../../../../../
SOURCES		= DialogImport.cpp ../../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= ../res/bc58.qrc
TARGET		= ../../../../beurer-bc58-s

unix:!macx {
QMAKE_RPATHDIR	+= $ORIGIN/../../lib
}

win32 {
CONFIG		-= debug_and_release
}

macx {
}

system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../../bm58/v-ser/DialogImport.* .))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../../../../../mainapp/res/svg/plugin/*.svg ../res/svg))
QMAKE_CLEAN += $$OUT_PWD/DialogImport.*
QMAKE_CLEAN += $$OUT_PWD/../res/svg/*.svg
