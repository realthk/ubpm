#ifndef DLGIMPORT_H
#define DLGIMPORT_H

#define VID 0x0C45
#define PID 0x7406

#define MAINTAINER	QString("<a href='mailto:lazyt@mailbox.org?subject=UBPM Plugin %1'>Thomas Löwe</a>%2").arg(MODEL, HELPER)
#define VERSION		QString("1.2.1 [ HIDAPI %1 ]").arg(HID_API_VERSION_STR)
#define PRODUCER	"<a href='https://www.beurer.com/web/gb/products/medical/blood-presure/'>Beurer</a>"
#ifdef BC58
	#define MODEL	"BC58H"
	#define ALIAS	"BC 58"
	#define IMAGE	":/png/bc58.png"
	#define MANUAL	":/pdf/bc58.pdf"
	#define HELPER	", Andreas Hoppe"
#elif defined BM55
	#define MODEL	"BM55H"
	#define ALIAS	"BM 55"
	#define IMAGE	":/png/bm55.png"
	#define MANUAL	":/pdf/bm55.pdf"
	#define HELPER	", Werner Panocha"
#elif defined BM65
	#define MODEL	"BM65H"
	#define ALIAS	"BM 65"
	#define IMAGE	":/png/bm65.png"
	#define MANUAL	":/pdf/bm65.pdf"
	#define HELPER	""
#else
	#define MODEL	"BM58H"
	#define ALIAS	"BM 58"
	#define IMAGE	":/png/bm58.png"
	#define MANUAL	":/pdf/bm58.pdf"
	#define HELPER	""
#endif
#define ICON		":/svg/usb-hid.svg"

#define LOGFILE QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/ubpm-import.log"

#define TIMEOUT 1000

#include "ui_DialogImport.h"

#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QStandardPaths>
#include <QThread>
#include <QTimer>
#include <QTranslator>

#include "deviceinterface.h"

#include "hidapi.h"

class DialogImport : public QDialog, private Ui::DialogImport
{
	Q_OBJECT

public:

	explicit DialogImport(QWidget*, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*, struct SETTINGS*);

	bool failed = false;

private:

	hid_device *hid;

	quint8 cmd_init[9] = { 0x00, 0xAA, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4 };
	quint8 cmd_gcnt[9] = { 0x00, 0xA2, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4 };
	quint8 cmd_gmes[9] = { 0x00, 0xA3, 0x00, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4 };
	quint8 cmd_exit[9] = { 0x00, 0xF7, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4, 0xF4 };

	quint8 rawdata[8];
	QByteArray payload;
	QFile log;

	QVector <struct HEALTHDATA> *u1, *u2;

	struct SETTINGS *settings;

	bool abort = false;
	bool finished = true;

	int rc;
	int measurements = 0;

	bool sendINI();
	bool sendGDC();
	bool sendGME();
	bool sendEND();
	void decryptPayload();
	void logRawData(bool, int, quint8*);

private slots:

	void on_checkBox_auto_import_toggled(bool);

	void on_toolButton_toggled(bool);

	void on_pushButton_import_clicked();
	void on_pushButton_cancel_clicked();

	void reject();
};

#endif // DLGIMPORT_H
