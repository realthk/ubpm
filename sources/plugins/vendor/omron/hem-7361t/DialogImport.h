#ifndef DLGIMPORT_H
#define DLGIMPORT_H

#define MAINTAINER	QString("<a href='mailto:lazyt@mailbox.org?subject=UBPM Plugin %1'>Thomas Löwe</a>%2").arg(MODEL, HELPER)
#define VERSION		QString("1.2.0 [ BLUETOOTH ]")
#define PRODUCER	"<a href='https://omronhealthcare.com/blood-pressure'>OMRON Corporation</a>"
#ifdef HEM7600T
	#define MODEL	"HEM-7600T"
	#define ALIAS	"Evolv"
	#define IMAGE	":/png/hem-7600t.png"
	#define MANUAL	":/pdf/hem-7600t.pdf"
	#define MEMORY	100
	#define HELPER	", Alex Morris"
#elif HEM6232T
	#define MODEL	"HEM-6232T"
	#define ALIAS	"RS7 Intelli IT"
	#define IMAGE	":/png/hem-6232t.png"
	#define MANUAL	":/pdf/hem-6232t.pdf"
	#define MEMORY	100
	#define HELPER	", Thomas Wiedemann"
#elif HEM7155T
	#define MODEL	"HEM-7155T"
	#define ALIAS	"M4/M400 Intelli IT, X4 Smart"
	#define IMAGE	":/png/hem-7155t.png"
	#define MANUAL	":/pdf/hem-7155t.pdf"
	#define MEMORY	60
	#define HELPER	", Annett Heyder"
#else
	#define MODEL	"HEM-7361T"
	#define ALIAS	"M7/M500 Intelli IT, X7 Smart"
	#define IMAGE	":/png/hem-7361t.png"
	#define MANUAL	":/pdf/hem-7361t.pdf"
	#define MEMORY	100
	#define HELPER	", \"Elwood\""
#endif
#define ICON		":/svg/bluetooth.svg"

#define LOGFILE QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/ubpm-import.log"

#define UUID_300 QBluetoothUuid(QString("ecbe3980-c9a2-11e1-b1bd-0002a5d5c51b"))
#define UUID_311 QBluetoothUuid(QString("b305b680-aee7-11e1-a730-0002a5d5c51b"))
#define UUID_321 QBluetoothUuid(QString("db5b55e0-aee7-11e1-965e-0002a5d5c51b"))
#define UUID_361 QBluetoothUuid(QString("49123040-aee8-11e1-a74d-0002a5d5c51b"))
#define UUID_371 QBluetoothUuid(QString("4d0bf320-aee8-11e1-a0d9-0002a5d5c51b"))
#define UUID_381 QBluetoothUuid(QString("5128ce60-aee8-11e1-b84b-0002a5d5c51b"))
#define UUID_391 QBluetoothUuid(QString("560f1420-aee8-11e1-8184-0002a5d5c51b"))

#define cmd_init QByteArray::fromHex("0800000000100018")
#define cmd_done QByteArray::fromHex("080f000000000007")
#define cmd_fail QByteArray::fromHex("080f0f0f0f000008")

#include "ui_DialogImport.h"

#include <QBluetoothLocalDevice>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothServiceDiscoveryAgent>
#include <QLowEnergyController>
#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QStandardPaths>
#include <QTimer>
#include <QThread>
#include <QTranslator>

#include "deviceinterface.h"

class DialogImport : public QDialog, private Ui::DialogImport
{
	Q_OBJECT

public:

	explicit DialogImport(QWidget*, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*, struct SETTINGS*);

	bool failed = false;

private:

	QBluetoothLocalDevice *bld;
	QBluetoothDeviceDiscoveryAgent *bdda;
	QLowEnergyController *lec;
	QLowEnergyService *les_info, *les_data;
	QList<QBluetoothDeviceInfo> bdi;

	QByteArray payloads[2];
	int payload = 0;

	QFile log;

	QVector <struct HEALTHDATA> *u1, *u2;

	struct SETTINGS *settings;

	bool abort = false;
	bool finished = true;
	bool bt_finished;

	int searchBtController();
	bool waitBTFinished();
	void readBTInfo();
	bool readBTData();
	int buildCRC(QByteArray);
	void decryptPayload();
	void logRawData(bool, QLowEnergyCharacteristic, QByteArray);

private slots:

	void on_comboBox_controller_currentIndexChanged(int);

	void on_checkBox_auto_discover_toggled(bool);
	void on_checkBox_auto_connect_toggled(bool);
	void on_checkBox_auto_import_toggled(bool);

	void on_toolButton_toggled(bool);

	void bddaDeviceDiscovered(QBluetoothDeviceInfo);
	void bddaError(QBluetoothDeviceDiscoveryAgent::Error);
	void bddaFinished();

	void lecConnected();
	void lecDisconnected();
	void lecDiscoveryFinished();
	void lecError(QLowEnergyController::Error);

	void lesCharacteristicChanged(QLowEnergyCharacteristic, QByteArray);
	void lesCharacteristicRead(QLowEnergyCharacteristic, QByteArray);
	void lesCharacteristicWritten(QLowEnergyCharacteristic, QByteArray);

	void on_pushButton_discover_clicked();
	void on_pushButton_connect_clicked();
	void on_pushButton_import_clicked();
	void on_pushButton_cancel_clicked();

	void reject();
};

#endif // DLGIMPORT_H
