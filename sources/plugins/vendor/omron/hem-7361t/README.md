## Prerequisites

This plugin is "work in progress" and has the following limitations at the moment:

* Bluetooth controller with changeable MAC address is required (e.g. CSR8510 chipset, buy USB [dongle](https://codeberg.org/LazyT/ubpm/issues/26) for €5)
* install the [OMRON Connect](https://www.omronconnect.com) App on your Android/Apple and add your device (skip all personal data, no cloud account)
* clone the Bluetooth MAC address of your Android/Apple to the USB dongle (bdaddr on Linux/macOS or [MacAddressChanger](https://macaddresschanger.com) on Windows)
* connect the device via operating system to the USB dongle (Windows can only discover connected devices)

Select the Bluetooth controller with cloned MAC in UBPM and click "Discover Device". If your device is missing, try to discover again (scans only for 5 seconds).

After your device was found, click "Connect Device" and some information like producer, product and firmware should be displayed.

Now click "Import" and keep your fingers crossed…

##### Hint

You can also try to write a pairing key using the python app [omblepy](https://github.com/userx14/omblepy) to bypass the first 3 steps listed above.

## Troubleshooting

Doesn't work? Here are some hints:

* disable the internal Bluetooth controller in BIOS to force use of the external USB dongle
* discover again if the connection fails to avoid cached information
* delete the old connection via operating system and recreate again
* resync data once in the "OMRON Connect" App and repeat the previous step

## Cleanup

Maybe old data needs to be cleared to start from scratch. Try the following:

#### Windows

* delete "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\BTHPORT\Parameters\Devices\\\<mac>"

#### Linux

* delete "/var/lib/bluetooth/\<mac>"

#### macOS

* delete "/Library/Preferences/com.apple.Bluetooth.plist"
