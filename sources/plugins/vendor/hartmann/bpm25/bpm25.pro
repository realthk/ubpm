TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets serialport
DEFINES		+= BPM25
INCLUDEPATH	+= ../../../../
SOURCES		= DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= res/bpm25.qrc
TARGET		= ../../../veroval-bpm25

unix:!macx {
QMAKE_RPATHDIR	+= $ORIGIN/../../lib
}

win32 {
CONFIG		-= debug_and_release
}

macx {
}

system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../gce604/DialogImport.* .))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../../../../mainapp/res/svg/plugin/*.svg res/svg))
QMAKE_CLEAN += $$OUT_PWD/DialogImport.*
QMAKE_CLEAN += $$OUT_PWD/res/svg/*.svg
