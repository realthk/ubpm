APP_VERS = "1.7.2"

QT += core gui widgets network help charts sql svg xml printsupport

TARGET = ubpm

INCLUDEPATH += ../

SOURCES	+= MainWindow.cpp DialogAnalysis.cpp DialogHelp.cpp DialogMigration.cpp DialogRecord.cpp DialogSettings.cpp DialogUpdate.cpp
HEADERS	+= MainWindow.h   DialogAnalysis.h   DialogHelp.h   DialogMigration.h	DialogRecord.h   DialogSettings.h   DialogUpdate.h
FORMS	+= MainWindow.ui  DialogAnalysis.ui  DialogHelp.ui  DialogMigration.ui	DialogRecord.ui  DialogSettings.ui  DialogUpdate.ui

RESOURCES	+= res/ubpm.qrc res/icons.qrc
TRANSLATIONS	+= lng/mainapp_de.ts lng/mainapp_el.ts lng/mainapp_es.ts lng/mainapp_fa.ts lng/mainapp_fr.ts lng/mainapp_it.ts lng/mainapp_nb_NO.ts lng/mainapp_nl.ts lng/mainapp_pl.ts lng/mainapp_ru.ts

BASETRANSLATIONS = $$[QT_INSTALL_TRANSLATIONS]/qtbase_de.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_es.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_fr.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_it.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_pl.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_ru.qm

VERSION = $${APP_VERS}

system($$[QT_INSTALL_BINS]/rcc --no-compress --binary $$PWD/res/icons.qrc --output $$PWD/res/rcc/icons.rcc)

unix:!macx {
	APP_DATE = "$$system(date +%d.%m.%Y)"

	BIN_PWD=$$OUT_PWD

	QMAKE_LFLAGS += -Wl,-rpath=.

	INSTALLDIR = /tmp/ubpm.AppDir

	!system(command -v linuxdeployqt-continuous-x86_64.AppImage >/dev/null) { warning("linuxdeployqt (https://github.com/probonopd/linuxdeployqt/releases) missing, cant't build package!") }
	!exists($$[QT_INSTALL_PLUGINS]/sqldrivers/libqsqlcipher.so) { warning("SQLCipher (https://github.com/sjemens/qsqlcipher-qt5) missing, encryption not available!") }
	exists($$INSTALLDIR) { system(rm -r $$INSTALLDIR) }

	target.path = $$INSTALLDIR/usr/bin

	translations.path  = $$INSTALLDIR/usr/bin/Languages
	translations.files = lng/mainapp_*.qm ../plugins/shared/plugin/lng/plugins_*.qm $$BASETRANSLATIONS

	themes.path  = $$INSTALLDIR/usr/bin/Themes
	themes.files = qss/*.qss

	devices.path  = $$INSTALLDIR/usr/bin/Plugins
	devices.files = ../plugins/*.so

	help.path  = $$INSTALLDIR/usr/bin/Guides
	help.files = hlp/*.qch hlp/*.qhc

	lin.path  = $$INSTALLDIR
	lin.extra = cp ../../package/lin/ubpm.desktop $$INSTALLDIR/ubpm.desktop &&\
				cp res/ico/app.png $$INSTALLDIR/ubpm.png &&\
				mkdir -p $$INSTALLDIR/usr/share/metainfo && cp ../../package/lin/de.lazyt.ubpm.appdata.xml $$INSTALLDIR/usr/share/metainfo/ &&\
				ln -s ubpm.png $$INSTALLDIR/.DirIcon &&\
				export VERSION=$$VERSION &&\
				mkdir $$INSTALLDIR/usr/lib && cp $$[QT_INSTALL_PREFIX]/../../Tools/OpenSSL/binary/lib/libssl.so.1.1 $$INSTALLDIR/usr/lib && cp $$[QT_INSTALL_PREFIX]/../../Tools/OpenSSL/binary/lib/libcrypto.so.1.1 $$INSTALLDIR/usr/lib &&\
				linuxdeployqt-continuous-x86_64.AppImage $$INSTALLDIR/ubpm.desktop -appimage -no-copy-copyright-files -no-translations &&\
				mv Universal_Blood_Pressure_Manager-*-x86_64.AppImage ubpm-"$$VERSION".AppImage

	INSTALLS += target translations themes devices help lin
}

win32 {
	APP_DATE = "$$system(date /t)"

	BIN_PWD=$$OUT_PWD

	RC_ICONS = res/ico/app.ico
	RC_LANG  = "0x0407"

	QMAKE_TARGET_COMPANY     = "LazyT"
	QMAKE_TARGET_DESCRIPTION = "$${APP_NAME}"
	QMAKE_TARGET_COPYRIGHT   = "Thomas L\\366we, 2020-2022"
	QMAKE_TARGET_PRODUCT     = "UBPM"

	INSTALLDIR = $$(TMP)/ubpm.7zip

	MAKESFX = "cat $$shell_path(..\..\package\win\7zs2.sfx) $$shell_path($$(TMP)\ubpm.7z) >"

	!system(cat --help >nul 2>&1) { MAKESFX = "copy /b $$shell_path(..\..\package\win\7zs2.sfx) + $$shell_path($$(TMP)\ubpm.7z)" }
	!system(7z.exe >nul 2>&1) { warning("7zip (https://www.7-zip.org/download.html) missing, cant't build package!") }
	!exists($$[QT_INSTALL_PLUGINS]/sqldrivers/qsqlcipher.dll) { warning("SQLCipher (https://github.com/sjemens/qsqlcipher-qt5) missing, encryption not available!") }
	!exists($$(SYSTEMROOT)/System32/msvcr100.dll) { warning("MSVCR missing, TLS via Qt OpenSSL not available!") }
	exists($$INSTALLDIR) { system(rmdir /s /q $$shell_path($$INSTALLDIR)) }
	exists($$(TMP)/ubpm.7z) { system(del $$shell_path($$(TMP)/ubpm.7z)) }

	target.path = $$INSTALLDIR

	translations.path  = $$INSTALLDIR/Languages
	translations.files = lng/mainapp_*.qm ../plugins/shared/plugin/lng/plugins_*.qm $$BASETRANSLATIONS

	themes.path  = $$INSTALLDIR/Themes
	themes.files = qss/*.qss

	devices.path  = $$INSTALLDIR/Plugins
	devices.files = ../plugins/*.dll

	help.path  = $$INSTALLDIR/Guides
	help.files = hlp/*.qch hlp/*.qhc

	libraries.path  = $$INSTALLDIR
	libraries.files = $$[QT_INSTALL_LIBEXECS]/Qt5Core.dll $$[QT_INSTALL_LIBEXECS]/Qt5Gui.dll $$[QT_INSTALL_LIBEXECS]/Qt5Widgets.dll $$[QT_INSTALL_LIBEXECS]/Qt5Charts.dll $$[QT_INSTALL_LIBEXECS]/Qt5Network.dll $$[QT_INSTALL_LIBEXECS]/Qt5Help.dll $$[QT_INSTALL_LIBEXECS]/Qt5Sql.dll $$[QT_INSTALL_LIBEXECS]/Qt5Printsupport.dll $$[QT_INSTALL_LIBEXECS]/Qt5Svg.dll $$[QT_INSTALL_LIBEXECS]/Qt5SerialPort.dll $$[QT_INSTALL_LIBEXECS]/Qt5Bluetooth.dll $$[QT_INSTALL_LIBEXECS]/libgcc_s_seh-1.dll $$[QT_INSTALL_LIBEXECS]/libstdc++-6.dll $$[QT_INSTALL_LIBEXECS]/libwinpthread-1.dll $$[QT_INSTALL_PREFIX]/../../Tools/OpenSSL/Win_x64/bin/libcrypto-1_1-x64.dll $$[QT_INSTALL_PREFIX]/../../Tools/OpenSSL/Win_x64/bin/libssl-1_1-x64.dll $$(SYSTEMROOT)/System32/msvcr100.dll 

	plugins1.path  = $$INSTALLDIR/platforms
	plugins1.files = $$[QT_INSTALL_PLUGINS]/platforms/qwindows.dll
	plugins2.path  = $$INSTALLDIR/styles
	plugins2.files = $$[QT_INSTALL_PLUGINS]/styles/qwindowsvistastyle.dll
	plugins3.path  = $$INSTALLDIR/imageformats
	plugins3.files = $$[QT_INSTALL_PLUGINS]/imageformats/qsvg.dll
	plugins4.path  = $$INSTALLDIR/sqldrivers
	plugins4.files = $$[QT_INSTALL_PLUGINS]/sqldrivers/qsqlite.dll $$[QT_INSTALL_PLUGINS]/sqldrivers/qsqlcipher.dll
	plugins5.path  = $$INSTALLDIR/printsupport
	plugins5.files = $$[QT_INSTALL_PLUGINS]/printsupport/windowsprintersupport.dll
 
	win.path  = $$INSTALLDIR
	win.extra = 7z a -mx9 $$shell_path($$(TMP)\ubpm.7z) $$shell_path($$INSTALLDIR\*) &&\
				$$MAKESFX ubpm-"$$VERSION".exe

	INSTALLS += target translations themes devices help libraries plugins1 plugins2 plugins3 plugins4 plugins5 win

	CONFIG -= debug_and_release
}

macx {
	APP_DATE = "$$system(date +%d.%m.%Y)"

	BIN_PWD=$$OUT_PWD/ubpm.app/Contents/MacOS

	ICON = res/ico/app.icns

	QMAKE_INFO_PLIST = ../../package/mac/Info.plist

	INSTALLDIR = $$(TMPDIR)/ubpm

	!exists($$[QT_INSTALL_PLUGINS]/sqldrivers/libqsqlcipher.dylib) { warning("SQLCipher (https://github.com/sjemens/qsqlcipher-qt5) missing, encryption not available!") }
	exists($$INSTALLDIR) { system(rm -r $$INSTALLDIR) }
	exists(ubpm-"$$VERSION".dmg) { system(rm ubpm-"$$VERSION".dmg) }
	exists(ubpm.app) { system(rm -r ubpm.app) }

	target.path = $$INSTALLDIR

	translations1.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Languages
	translations1.files = lng/mainapp_*.qm ../plugins/shared/plugin/lng/plugins_*.qm $$BASETRANSLATIONS
	translations2.path  = $$INSTALLDIR/ubpm.app/Contents
	translations2.files = ../../package/mac/Resources

	themes.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Themes
	themes.files = qss/*.qss

	devices.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Plugins
	devices.files = ../plugins/*.dylib

	help.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Guides
	help.files = hlp/*.qch hlp/*.qhc

	mac.path  = $$INSTALLDIR/ubpm.app
	mac.extra = cp -R ../../package/mac/Resources $$INSTALLDIR/ubpm.app/Contents &&\
				cd $$INSTALLDIR &&\
				ln -s /Applications &&\
				$$[QT_INSTALL_BINS]/macdeployqt ubpm.app &&\
				hdiutil create -volname UBPM -srcfolder $$INSTALLDIR $$PWD/ubpm-"$$VERSION".dmg

	INSTALLS += target translations1 translations2 themes devices help mac
}

DEFINES += APPVERS=\"\\\"$${APP_VERS}\\\"\" APPDATE=\"\\\"$${APP_DATE}\\\"\"

!exists($$BIN_PWD/Guides)    { QMAKE_POST_LINK += $(MKDIR) $$shell_path($$BIN_PWD/Guides)    && $$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/hlp/*.q*)       $$shell_path($$BIN_PWD/Guides)  $$escape_expand(\n\t) }
!exists($$BIN_PWD/Languages) { QMAKE_POST_LINK += $(MKDIR) $$shell_path($$BIN_PWD/Languages) && $$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/lng/*.qm)       $$shell_path($$BIN_PWD/Languages) && $$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../plugins/shared/plugin/lng/*.qm) $$shell_path($$BIN_PWD/Languages) && $$QMAKE_COPY_FILE $$shell_path($$[QT_INSTALL_TRANSLATIONS]/qtbase_*.qm) $$shell_path($$BIN_PWD/Languages) $$escape_expand(\n\t) }
!exists($$BIN_PWD/Themes)    { QMAKE_POST_LINK += $(MKDIR) $$shell_path($$BIN_PWD/Themes)    && $$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/qss/*.qss)      $$shell_path($$BIN_PWD/Themes)  $$escape_expand(\n\t) }
!exists($$BIN_PWD/Plugins) : exists($$OUT_PWD/../plugins/*.so) | exists($$OUT_PWD/../plugins/*.dll) | exists($$OUT_PWD/../plugins/*.dylib) { QMAKE_POST_LINK += $(MKDIR) $$shell_path($$BIN_PWD/Plugins) && $$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../plugins/*.*) $$shell_path($$BIN_PWD/Plugins) $$escape_expand(\n\t) }
