#include "DialogHelp.h"

DialogHelp::DialogHelp(QWidget *parent, QString lng, struct SETTINGS *psettings) : QDialog(parent)
{
	setupUi(this);

	setWindowFlags(Qt::Window | Qt::WindowMinimizeButtonHint  | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);

	settings = psettings;
	requested = lng;

	if(QFile::exists(QString("%1/Guides/%2.qhc").arg(QApplication::applicationDirPath(), lng)))
	{
		language = lng;
		fallback = false;
	}
	else
	{
		language = "en";
		fallback = true;
	}

	QFile::copy(QString("%1/Guides/%2.qhc").arg(QApplication::applicationDirPath(), language), QString("%1/%2.qhc").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation), language));
	QFile::copy(QString("%1/Guides/%2.qch").arg(QApplication::applicationDirPath(), language), QString("%1/%2.qch").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation), language));

	helpEngine = new QHelpEngine(QString("%1/%2.qhc").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation), language));
	helpEngine->setupData();

	contentWidget = helpEngine->contentWidget();
	contentWidget->setMinimumWidth(300);

	verticalLayout->addWidget((QWidget*)contentWidget);

	helpBrowser->setHelpEngine(helpEngine);

	splitter->setStretchFactor(0, 0);
	splitter->setStretchFactor(1, 1);

	connect(contentWidget, &QHelpContentWidget::clicked, this, &DialogHelp::setSourceFromContent);
	connect(contentWidget, &QHelpContentWidget::activated, this, &DialogHelp::setSourceFromContent);
	connect(helpBrowser, &HelpBrowser::anchorClicked, this, &DialogHelp::anchorClicked);
}

void DialogHelp::showHelp(QString page)
{
	if(!initialized)
	{
		if(fallback)
		{
			if(!QFile::exists(QString("%1/Guides/en.qhc").arg(QApplication::applicationDirPath())))
			{
				QMessageBox::warning(nullptr, APPNAME, tr("%1 guide not found!").arg(language.toUpper()));

				close();

				return;
			}
			else
			{
				QMessageBox::information(nullptr, APPNAME, tr("%1 guide not found, showing EN guide instead.").arg(requested.toUpper()));
			}
		}

		contentWidget->expandAll();

		initialized = true;
	}

	setSourceFromPage(page);

	if(isHidden())
	{
		restoreGeometry(settings->help.geometry);

		show();
	}

	raise();
}

QString DialogHelp::getSource()
{
	return helpBrowser->source().toString();
}

void DialogHelp::setSource(QUrl url)
{
	helpBrowser->setSource(url);

	contentWidget->setCurrentIndex(contentWidget->indexOf(url));
}
void DialogHelp::setSourceFromPage(QString page)
{
	setSource(QString("qthelp://de.lazyt.ubpm/%1/%2.html").arg(language, page));
}

void DialogHelp::setSourceFromContent(QModelIndex index)
{
	setSource(helpEngine->contentModel()->contentItemAt(index)->url());
}

void DialogHelp::anchorClicked(QUrl link)
{
	setSource(link);
}

DialogHelp::~DialogHelp()
{
	QFile::remove(QString("%1/%2.qch").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation), language));
	QFile::remove(QString("%1/%2.qhc").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation), language));
}

void DialogHelp::reject()
{
	settings->help.geometry = saveGeometry();

	hide();
}
