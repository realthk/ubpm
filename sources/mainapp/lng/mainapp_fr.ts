<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Analyse des données</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Requête</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="206"/>
        <source>Results</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation>Distolique</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation>Pression pulsée</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation>Fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation>Irrégulier</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="169"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="199"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="48"/>
        <source>Could not create memory database!

%1</source>
        <translation>Impossible de créer une base de données mémoire&#xa0;!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="208"/>
        <source>No results for this query found!</source>
        <translation>Aucun résultat pour cette requête n&apos;a été trouvé&#xa0;!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Résultat</numerusform>
            <numerusform>Résultats</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Corresponde</numerusform>
            <numerusform>Correspondent</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Enregistrement</numerusform>
            <numerusform>Enregistrements</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>User %1</source>
        <translation>Utilisateur %1</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Manuel utilisateur</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="60"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>Le Manuel Utilisateur %1 n&apos;a pas été trouvé, Regarder pour le moment de guide EN</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="52"/>
        <source>%1 guide not found!</source>
        <translation>%1 guide pas trouvé !</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Migration des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation>Ouvrir la source des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Format de la date</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation>Détection d&apos;irrégularités</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation>Ligne de départ</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Éléments par ligne</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Délimiteur</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Format de l’heure</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation>Détection des mouvements</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation>Langue de la date</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation>Positions</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation>Irrégulier</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation>Fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation>Diastolique</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="783"/>
        <source>Migrate User 1</source>
        <translation>Migrer l&apos;Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="812"/>
        <source>Migrate User 2</source>
        <translation>Migrer l&apos;Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="838"/>
        <source>Copy to Custom</source>
        <translation>Copier dans Personnalisé</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="858"/>
        <source>Select predefined settings</source>
        <translation>Sélectionner les réglages prédéfinis</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="892"/>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="903"/>
        <source>Test Data Migration</source>
        <translation>Test de la migration des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="917"/>
        <source>Start Data Migration</source>
        <translation>Démarrer la migration des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="250"/>
        <source>Choose Data Source for Migration</source>
        <translation>Choisir la source de données pour la migration</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="250"/>
        <source>CSV File (*.csv)</source>
        <translation>Fichier CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="303"/>
        <source>%L1 Bytes</source>
        <translation>%L1 octets</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%1 Lines</source>
        <translation>%1 lignes</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="318"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossible d&apos;ouvrir «&#xa0;%1&#xa0;»&#xa0;!

Raison&#xa0;: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="340"/>
        <source>Startline can&apos;t be empty.</source>
        <translation>La ligne de début ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="346"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation>La ligne de début doit être plus petite que le nombre de lignes.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="354"/>
        <source>Line %1 = %2</source>
        <translation>Ligne %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="359"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>Le délimiteur ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="366"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation>La ligne de début ne contient pas le délimiteur «&#xa0;%1&#xa0;».</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="377"/>
        <source>Elements can&apos;t be empty.</source>
        <translation>Les éléments ne peuvent pas être vides.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="383"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation>Les éléments ne peuvent être inférieurs à 4.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="389"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation>Les éléments ne correspondent pas (trouvé %1).</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="396"/>
        <source>Date format can&apos;t be empty.</source>
        <translation>Le format de la date ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="402"/>
        <source>Single date format must also contain a time format.</source>
        <translation>Un format de date unique doit également contenir un format d&apos;heure.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="409"/>
        <source>Date position can&apos;t be empty.</source>
        <translation>La position de la date ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="415"/>
        <source>Date position must be smaller than elements.</source>
        <translation>La position de la date doit être plus petite que les éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="422"/>
        <source>Time position requires also a time parameter.</source>
        <translation>La position temporelle nécessite également un paramètre temporel.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="428"/>
        <source>Time format requires also a time position.</source>
        <translation>Le format horaire nécessite également une position horaire.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="434"/>
        <source>Time position must be smaller than elements.</source>
        <translation>La position temporelle doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="441"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation>La position systolique ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="447"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation>La position systolique doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="454"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation>La position diastolique ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="460"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation>La position diastolique doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="467"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation>La position de la fréquence cardiaque ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="473"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation>La position de la fréquence cardiaque doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="480"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation>La position irrégulière doit être plus petite que les éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="486"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation>Une position irrégulière nécessite également un paramètre irrégulier.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="493"/>
        <source>Movement position must be smaller than elements.</source>
        <translation>La position du mouvement doit être plus petite que les éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="499"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation>La position du mouvement nécessite également un paramètre de mouvement.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="506"/>
        <source>Comment position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="522"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation>Les positions ne peuvent pas exister deux fois.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="538"/>
        <source>Date format is invalid.</source>
        <translation>Le format de la date est invalide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="545"/>
        <source>Time format is invalid.</source>
        <translation>Le format de l&apos;heure est invalide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="555"/>
        <source>Date/Time format is invalid.</source>
        <translation>Le format de la date et de l&apos;heure est invalide.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="745"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation>
            <numerusform>La migration de %n enregistrement pour l&apos;utilisateur %1 a réussi.</numerusform>
            <numerusform>La migration de %n enregistrements pour l&apos;utilisateur %1 a réussi.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="749"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Saut de %n enregistrement invalide&#xa0;!</numerusform>
            <numerusform>Saut de %n enregistrements invalides&#xa0;!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="754"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Sauter %n enregistrement en double&#xa0;!</numerusform>
            <numerusform>Sauter %n enregistrements en double&#xa0;!</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Enregistrement Manuel</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Enregistrement de données</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Ajouter un enregistrement pour %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Sélectionner la date et la heure</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Entrer SYS</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>Entrer DIA</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Entrer BPM</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Rythme cardiaque irrégulier</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Entrez un commentaire facultatif</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Afficher un message pour une création / suppression / modification d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="33"/>
        <location filename="../DialogRecord.cpp" line="186"/>
        <source>Modify</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="131"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>L&apos;Enregistrement n&apos;a pas pu être supprimé

Aucune Entrée pour cette date et heure n&apos;existe</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="137"/>
        <source>Data record successfully deleted.</source>
        <translation>L&apos;enregistrement a été effacé avec succès</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="149"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Veuillez d&apos;abord entrer une valeur valide pour &quot;SYS&quot; !</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="159"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Veuillez d&apos;abord entrer une valeur valide pour &quot;DIA&quot; !</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="168"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Veuillez d&apos;abord entrer une valeur valide pour &quot;BPM&quot; !</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="190"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>L&apos;Enregistrement n&apos;a pas pu être modifié.

Aucune entrée pour cette date et cette heure existe.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="196"/>
        <source>Data Record successfully modified.</source>
        <translation>Les Données de l&apos;Enregistrement ont été modifiées avec Succès.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="205"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Les Données de l&apos;Enregistrement n&apos;ont pas pu être crées !

Une entrée avec la Date et l&apos;Heure existe déjà.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="211"/>
        <source>Data Record successfully created.</source>
        <translation>L&apos;Enregistrement de la Données est crée avec Succès.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="159"/>
        <source>Choose Database Location</source>
        <translation>Choisir l&apos;Emplacement de la Base De Données</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="169"/>
        <source>Choose Database Backup Location</source>
        <translation>Choisir l&apos;emplacement de la sauvegarde de la base de données</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="205"/>
        <source>Could not display manual!

%1</source>
        <translation>Impossible d&apos;afficher le manuel

%1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="328"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation>La base de données sélectionnée existe déjà.

Veuillez choisir l&apos;action préférée.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="330"/>
        <source>Overwrite</source>
        <translation>Écraser</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="331"/>
        <source>Merge</source>
        <translation>Fusionner</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="332"/>
        <source>Swap</source>
        <translation>Permuter</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="334"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation>Écraser :

Écrase la base de données sélectionnée avec la base de données actuelle.

Fusionner :

Fusionne la base de données sélectionnée avec la base de données actuelle.

Permuter :

Supprime la base de données actuelle et utilise la base de données sélectionnée.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="342"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation>La base de données actuelle est vide.

La base de données sélectionnée sera effacée à la sortie, si aucune donnée n&apos;est ajoutée.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="372"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>Le cryptage SQL ne peut être activé sans mot de passe et sera désactivé !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="379"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation>La sauvegarde de la base de données doit être située sur un disque dur, une partition ou un répertoire différent.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="384"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Veuillez entrer des valeurs valides pour les informations supplémentaires de l&apos;utilisateur 1 !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="391"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Veuillez entrer des valeurs valides pour les informations supplémentaires de l&apos;utilisateur 2 !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="398"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>L&apos;age saisi ne correspond pas à la tranche d&apos;age sélectionné !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="405"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>L&apos;age saisi ne correspond pas à la tranche d&apos;age sélectionné pour l&apos;utilisateur 2 !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="413"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Veuillez activer les symboles ou les lignes pour les graphiques !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="420"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Merci d&apos;entrer un adresse électronique valide&#xa0;!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="427"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Merci d&apos;entrer un objet à votre courriel&#xa0;!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="434"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>un message doit contenir $CHART, $TABLE et/ou $STATS !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="545"/>
        <source>User 1</source>
        <translation>Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="559"/>
        <source>User 2</source>
        <translation>Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="672"/>
        <source>Blood Pressure Report</source>
        <translation>Rapport sur la tension artérielle</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="673"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Cher Dr NomduPraticien,

veuillez trouver ci-joint les données de ma tension artérielle pour ce mois-ci.

Cordialement,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="715"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Abandonner la configuration et abandonner toutes les modifications ?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Base de Données</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="52"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="58"/>
        <source>Current Location</source>
        <translation>Emplacement Courant</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="74"/>
        <source>Change Location</source>
        <translation>Changer Emplacement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="100"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Crypter avec SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="103"/>
        <source>Encryption</source>
        <translation>Crypter</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="115"/>
        <source>Password</source>
        <translation>Mot de Passe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="128"/>
        <source>Show Password</source>
        <translation>Afficher Mot de Passe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="282"/>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="317"/>
        <location filename="../DialogSettings.ui" line="580"/>
        <source>Male</source>
        <translation>Homme</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="346"/>
        <location filename="../DialogSettings.ui" line="609"/>
        <source>Female</source>
        <translation>Femme</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="442"/>
        <location filename="../DialogSettings.ui" line="714"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <location filename="../DialogSettings.ui" line="574"/>
        <source>Mandatory Information</source>
        <translation>Informations Obligatoires</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="372"/>
        <location filename="../DialogSettings.ui" line="644"/>
        <source>Age Group</source>
        <translation>Tranche d&apos;Age</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="458"/>
        <location filename="../DialogSettings.ui" line="730"/>
        <source>Additional Information</source>
        <translation>Informations Complémentaires</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Height</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="547"/>
        <location filename="../DialogSettings.ui" line="819"/>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="844"/>
        <source>Device</source>
        <translation>Appareil</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="850"/>
        <location filename="../DialogSettings.cpp" line="62"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Importer des greffons [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="880"/>
        <source>Show Device Image</source>
        <translation>Afficher l&apos;image de l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="903"/>
        <source>Show Device Manual</source>
        <translation>Afficher le manuel de l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="948"/>
        <source>Open Website</source>
        <translation>Aller au site Web</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="961"/>
        <source>Maintainer</source>
        <translation>Mainteneur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="989"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="996"/>
        <source>Send E-Mail</source>
        <translation>Envoyer un courriel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1009"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1016"/>
        <source>Producer</source>
        <translation>Fabricant</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1033"/>
        <source>Chart</source>
        <translation>Graphique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1160"/>
        <source>X-Axis Range</source>
        <translation>Plage de l&apos;axe X</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1166"/>
        <source>Dynamic Scaling</source>
        <translation>Échelle dynamique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1147"/>
        <source>Healthy Ranges</source>
        <translation>Plages saines</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1064"/>
        <source>Symbols</source>
        <translation>Symboles</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1141"/>
        <source>Colored Areas</source>
        <translation>Zones Colorées</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="865"/>
        <source>Please choose Device Plugin…</source>
        <translation>Veuillez choisir le greffon pour l&apos;appareil…</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1288"/>
        <location filename="../DialogSettings.ui" line="1563"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1376"/>
        <location filename="../DialogSettings.ui" line="1651"/>
        <source>Diastolic</source>
        <translation>Diastolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1464"/>
        <location filename="../DialogSettings.ui" line="1739"/>
        <source>Heartrate</source>
        <translation>Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1836"/>
        <source>Table</source>
        <translation>Tableau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1921"/>
        <location filename="../DialogSettings.ui" line="2104"/>
        <source>Systolic Warnlevel</source>
        <translation>Niveau d&apos;Alerte Systolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1867"/>
        <location filename="../DialogSettings.ui" line="2164"/>
        <source>Diastolic Warnlevel</source>
        <translation>Niveau d&apos;Alerte Diastolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="153"/>
        <source>Automatic Backup</source>
        <translation>Sauvegarde automatique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="156"/>
        <source>Backup</source>
        <translation>Sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="173"/>
        <source>Current Backup Location</source>
        <translation>Emplacement actuel de la sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="186"/>
        <source>Change Backup Location</source>
        <translation>Modifier l&apos;emplacement de la sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="213"/>
        <source>Backup Mode</source>
        <translation>Mode de sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="223"/>
        <source>Daily</source>
        <translation>Quotidien</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="232"/>
        <source>Weekly</source>
        <translation>Hebdomadaire</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="241"/>
        <source>Monthly</source>
        <translation>Mensuel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="253"/>
        <source>Keep Copies</source>
        <translation>Conserver des copies</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="486"/>
        <location filename="../DialogSettings.ui" line="758"/>
        <source>Birthday</source>
        <translation>Anniversaire</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1073"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1097"/>
        <location filename="../DialogSettings.ui" line="1125"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1179"/>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1188"/>
        <location filename="../DialogSettings.ui" line="1216"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1245"/>
        <source>Show Heartrate</source>
        <translation>Afficher la Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1251"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Afficher La Fréquence Cardiaque en Grahpique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1261"/>
        <source>Print Heartrate</source>
        <translation>Imprimer Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1267"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Imprimer la Fréquence Cardiaque sur une feuille séparée</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1981"/>
        <location filename="../DialogSettings.ui" line="2218"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Avertissement de la Pression Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2035"/>
        <location filename="../DialogSettings.ui" line="2272"/>
        <source>Heartrate Warnlevel</source>
        <translation>Avertissement de la Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2337"/>
        <source>Statistic</source>
        <translation>Statistique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2343"/>
        <source>Bar Type</source>
        <translation>Type de Barre</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2349"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Afficher les Barres Médianes au lieu des Barres Moyennes</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2359"/>
        <source>Legend Type</source>
        <translation>Type de Légende</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2365"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Afficher les Valeurs en tant que Légende à la place des Descriptions</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2380"/>
        <source>E-Mail</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2388"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2407"/>
        <source>Subject</source>
        <translation>Sujet</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2425"/>
        <source>Message</source>
        <translation>Message</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2446"/>
        <source>Plugin</source>
        <translation>Greffon</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2452"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2458"/>
        <source>Log Device Communication to File</source>
        <translation>Enregistrer la communication du dispositif dans un fichier</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Import Measurements automatically</source>
        <translation>Importer automatiquement les mesures</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2475"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2481"/>
        <source>Discover Device automatically</source>
        <translation>Découvrir le dispositif automatiquement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2488"/>
        <source>Connect Device automatically</source>
        <translation>Connecter le dispositif automatiquement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2503"/>
        <source>Update</source>
        <translation>Mise à Jour</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2509"/>
        <source>Autostart</source>
        <translation>Démmarage Automatique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2515"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Vérifier si une Mise à Jour au Démarrage du Programme</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2528"/>
        <source>Notification</source>
        <translation>Notification</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2534"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Toujours montrer le Résultat après une vérification de Mise à Jour</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2550"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2567"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2584"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Mise à jour en Ligne</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Version Disponible</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Taille du Fichier de Mise à Jour</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Version Installée</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="199"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="219"/>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="167"/>
        <source>No new version found.</source>
        <translation>Pas de nouvelle version trouvée.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! AVERTISSEMENT SSL - LIRE ATTENTIVEMENT !!!

Problème de connexion réseau :

%1
Voulez-vous quand même continuer ?</numerusform>
            <numerusform>!!! AVERTISSEMENT SSL - LIRE ATTENTIVEMENT !!!

Problèmes de connexion réseau :

%1
Voulez-vous quand même continuer ?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Downloading update failed!

%1</source>
        <translation>Le Téléchargement de la Mise à Jour a échoué !

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Checking update failed!

%1</source>
        <translation>La Vérification de la Mise à Jour a échouée !

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="140"/>
        <source>Unexpected response from update server!</source>
        <translation>Réponse inattendu du serveur de mise à jour !</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="157"/>
        <source>*%1 not found</source>
        <translation>*%1 pas trouvé</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="187"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>La mise à jour n&apos;a pas la taille attendue !

%L1 : %L2

Réessayer de Télécharger …</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="191"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>La mise à jour a été enregistrée dans %1.

Lancer la nouvelle version maintenant ?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="204"/>
        <source>Could not start new version!</source>
        <translation>Impossible de lancer la nouvelle version !</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>Really abort download?</source>
        <translation>Interrompre vraiment le téléchargement ?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="211"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Impossible d&apos;enregistrer la mise à jour dans %1 !

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gestionnaire Universel de la Pression Artérielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Montrer Période Précédente</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Montrer Période Suivante</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Montrer Graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Montrer Tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="126"/>
        <location filename="../MainWindow.cpp" line="4313"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="127"/>
        <location filename="../MainWindow.cpp" line="4314"/>
        <source>Diastolic</source>
        <translation>Diastolique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pression Pulsée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Irrégulier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Vue Statistique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>¼ Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>½ Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>6 Heures</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>12 Heures</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Jour</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation>Hebdomadaire</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation>Mensuel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation>Trimestrielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation>Semestrielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation>Annuelle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation>Les 7 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation>Les 14 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation>Les 21 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation>Les 28 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation>Les 3 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation>Les 6 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation>Les 9 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation>Les 12 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation>Tous les Enregistrements</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1161"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1170"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1179"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1188"/>
        <source>Charts</source>
        <translation>Graphiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1213"/>
        <source>Database</source>
        <translation>Basse de Donnée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1217"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1229"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1244"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1331"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1334"/>
        <location filename="../MainWindow.ui" line="1337"/>
        <source>Quit Program</source>
        <translation>Quitter le programme</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1349"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1352"/>
        <location filename="../MainWindow.ui" line="1355"/>
        <source>About Program</source>
        <translation>À propos du programme</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1364"/>
        <source>Guide</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1367"/>
        <location filename="../MainWindow.ui" line="1370"/>
        <source>Show Guide</source>
        <translation>Afficher le guide</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Update</source>
        <translation>Mise à jour</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Check Update</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1544"/>
        <source>To PDF</source>
        <translation>En PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Export To PDF</source>
        <translation>Exporter comme PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1559"/>
        <source>Migration</source>
        <translation>Migration</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Migrate from Vendor</source>
        <translation>Migrer d&apos;un fournisseur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1809"/>
        <source>PayPal</source>
        <translation>PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1812"/>
        <location filename="../MainWindow.ui" line="1815"/>
        <source>Donate via PayPal</source>
        <translation>Donner par PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1827"/>
        <location filename="../MainWindow.ui" line="1830"/>
        <source>Donate via Liberapay</source>
        <translation>Donner par Liberapay</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Donate via Amazon</source>
        <translation>Donner par Amazon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <location filename="../MainWindow.ui" line="1860"/>
        <source>Donate via SEPA</source>
        <translation>Donner par SEPA</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1869"/>
        <source>Translation</source>
        <translation>Traduction</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <location filename="../MainWindow.ui" line="1875"/>
        <source>Contribute Translation</source>
        <translation>Contribuer à la traduction</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1884"/>
        <source>E-Mail</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1887"/>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Send E-Mail</source>
        <translation>Envoyer le courriel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1902"/>
        <source>Icons</source>
        <translation>Icônes</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1905"/>
        <location filename="../MainWindow.ui" line="1908"/>
        <source>Change Icon Color</source>
        <translation>Changer la couleur de l&apos;icône</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1919"/>
        <source>System</source>
        <translation>Système</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1922"/>
        <location filename="../MainWindow.ui" line="1925"/>
        <source>System Colors</source>
        <translation>Couleurs du système</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1933"/>
        <source>Light</source>
        <translation>Clair</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1936"/>
        <location filename="../MainWindow.ui" line="1939"/>
        <source>Light Colors</source>
        <translation>Couleurs claires</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1947"/>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <location filename="../MainWindow.ui" line="1953"/>
        <source>Dark Colors</source>
        <translation>Couleurs sombres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1132"/>
        <source>Donation</source>
        <translation>Faire un don</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="128"/>
        <location filename="../MainWindow.cpp" line="4315"/>
        <source>Heartrate</source>
        <translation>Fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1129"/>
        <source>Make Donation</source>
        <translation>Faire un don</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>Bugreport</source>
        <translation>Rapport d&apos;erreur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>Send Bugreport</source>
        <translation>Envoyer un rapport d&apos;erreur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Change Settings</source>
        <translation>Charger paramètres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>From Device</source>
        <translation>Depuis l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1436"/>
        <location filename="../MainWindow.ui" line="1439"/>
        <source>Import From Device</source>
        <translation>Importer depuis l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>From File</source>
        <translation>Depuis un fichier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1454"/>
        <location filename="../MainWindow.ui" line="1457"/>
        <source>Import From File</source>
        <translation>Importer depuis un fichier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>From Input</source>
        <translation>Depuis l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1469"/>
        <location filename="../MainWindow.ui" line="1472"/>
        <source>Import From Input</source>
        <translation>Importer depuis l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>To CSV</source>
        <translation>En CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Export To CSV</source>
        <translation>Exporter en CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>To XML</source>
        <translation>En XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Export To XML</source>
        <translation>Exporter en XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>To JSON</source>
        <translation>En JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Export To JSON</source>
        <translation>Exporter en JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To SQL</source>
        <translation>En SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To SQL</source>
        <translation>Exporter en SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>Print Chart</source>
        <translation>Imprimer le gaphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Print Chart View</source>
        <translation>Aperçu de l&apos;impression graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>Print Table</source>
        <translation>Imprimer le tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Print Table View</source>
        <translation>Aperçu de l&apos;impression tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Print Statistic</source>
        <translation>Imprimer les statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Print Statistic View</source>
        <translation>Aperçu de l&apos;impression statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1622"/>
        <source>Preview Chart</source>
        <translation>Aperçu du graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <location filename="../MainWindow.ui" line="1628"/>
        <source>Preview Chart View</source>
        <translation>Aperçu de la vue graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1637"/>
        <source>Preview Table</source>
        <translation>Aperçu tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <location filename="../MainWindow.ui" line="1643"/>
        <source>Preview Table View</source>
        <translation>Aperçu de la vue tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1652"/>
        <source>Preview Statistic</source>
        <translation>Aperçu des statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <location filename="../MainWindow.ui" line="1658"/>
        <source>Preview Statistic View</source>
        <translation>Aperçu de la vue statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1667"/>
        <location filename="../MainWindow.ui" line="1670"/>
        <location filename="../MainWindow.ui" line="1673"/>
        <source>Clear All</source>
        <translation>Effacer tout</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Clear User 1</source>
        <translation>Effacer Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Clear User 2</source>
        <translation>Effacer Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1766"/>
        <source>Analysis</source>
        <translation>Analyser</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1769"/>
        <location filename="../MainWindow.ui" line="1772"/>
        <source>Analyze Records</source>
        <translation>Analyser les enregistrements</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1791"/>
        <location filename="../MainWindow.ui" line="1794"/>
        <location filename="../MainWindow.ui" line="1797"/>
        <source>Time Mode</source>
        <translation>Mode Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="76"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="4283"/>
        <location filename="../MainWindow.cpp" line="4284"/>
        <source>Records For Selected User</source>
        <translation>Enregistrements pour l&apos;utilisateur sélectionné</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="84"/>
        <location filename="../MainWindow.cpp" line="85"/>
        <location filename="../MainWindow.cpp" line="4286"/>
        <location filename="../MainWindow.cpp" line="4287"/>
        <source>Select Date &amp; Time</source>
        <translation>Sélectionner la date et l&apos;heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="136"/>
        <location filename="../MainWindow.cpp" line="4317"/>
        <source>Systolic - Value Range</source>
        <translation>Systolique - Plage de valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="137"/>
        <location filename="../MainWindow.cpp" line="4318"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastolique - Plage de valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="140"/>
        <location filename="../MainWindow.cpp" line="4320"/>
        <source>Systolic - Target Area</source>
        <translation>Systolique - Zone cible</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="141"/>
        <location filename="../MainWindow.cpp" line="4321"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastolique - Zone cible</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <source>Athlete</source>
        <translation>Athlète</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1515"/>
        <source>Excellent</source>
        <translation>Excellente</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1515"/>
        <source>Optimal</source>
        <translation>Optimale</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1516"/>
        <source>Great</source>
        <translation>Superbe</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1517"/>
        <source>Good</source>
        <translation>Bonne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1516"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1727"/>
        <location filename="../MainWindow.ui" line="1730"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="4094"/>
        <location filename="../MainWindow.cpp" line="4095"/>
        <location filename="../MainWindow.cpp" line="4096"/>
        <location filename="../MainWindow.cpp" line="4097"/>
        <location filename="../MainWindow.cpp" line="4347"/>
        <location filename="../MainWindow.cpp" line="4348"/>
        <location filename="../MainWindow.cpp" line="4349"/>
        <location filename="../MainWindow.cpp" line="4350"/>
        <source>Switch To %1</source>
        <translation>Basculer vers %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="488"/>
        <location filename="../MainWindow.cpp" line="4094"/>
        <location filename="../MainWindow.cpp" line="4095"/>
        <location filename="../MainWindow.cpp" line="4347"/>
        <location filename="../MainWindow.cpp" line="4348"/>
        <source>User 1</source>
        <translation>Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="498"/>
        <location filename="../MainWindow.cpp" line="4096"/>
        <location filename="../MainWindow.cpp" line="4097"/>
        <location filename="../MainWindow.cpp" line="4349"/>
        <location filename="../MainWindow.cpp" line="4350"/>
        <source>User 2</source>
        <translation>Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="138"/>
        <location filename="../MainWindow.cpp" line="4319"/>
        <source>Heartrate - Value Range</source>
        <translation>Fréquence cardiaque - Plage de valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="142"/>
        <location filename="../MainWindow.cpp" line="4322"/>
        <source>Heartrate - Target Area</source>
        <translation>Fréquence cardiaque - Zone cible</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="354"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation>Impossible de créer le répertoire de sauvegarde.

Veuillez vérifier les paramètres de l&apos;emplacement de sauvegarde.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="383"/>
        <location filename="../MainWindow.cpp" line="406"/>
        <source>Could not backup database:

%1</source>
        <translation>Impossible de sauvegarder la base de données&#xa0;:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="396"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation>Impossible de supprimer une sauvegarde périmée&#xa0;:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="423"/>
        <source>Could not register icons.</source>
        <translation>Impossible d&apos;enregistrer les icônes.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="551"/>
        <source>Blood Pressure Report</source>
        <translation>Rapport pression artérielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1026"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1031"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <source>Low</source>
        <translation>Basse</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1517"/>
        <source>High Normal</source>
        <translation>Haute normale</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1518"/>
        <location filename="../MainWindow.cpp" line="1985"/>
        <location filename="../MainWindow.cpp" line="4326"/>
        <location filename="../MainWindow.cpp" line="4330"/>
        <location filename="../MainWindow.cpp" line="4334"/>
        <location filename="../MainWindow.h" line="211"/>
        <location filename="../MainWindow.h" line="215"/>
        <location filename="../MainWindow.h" line="219"/>
        <source>Average</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1518"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <source>Below Average</source>
        <translation>Sous la moyenne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <source>Poor</source>
        <translation>Pauvre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1665"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>l&apos;Analyse du plugin d&apos;importation «&#xa0;%1&#xa0;» a échoué&#xa0;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1678"/>
        <location filename="../MainWindow.cpp" line="1700"/>
        <location filename="../MainWindow.cpp" line="4293"/>
        <source>Switch Language to %1</source>
        <translation>Changer la langue en %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1725"/>
        <location filename="../MainWindow.cpp" line="1740"/>
        <location filename="../MainWindow.cpp" line="4301"/>
        <source>Switch Theme to %1</source>
        <translation>Changer le thème en %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1765"/>
        <location filename="../MainWindow.cpp" line="1785"/>
        <location filename="../MainWindow.cpp" line="4309"/>
        <source>Switch Style to %1</source>
        <translation>Changer le style en %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1856"/>
        <location filename="../MainWindow.cpp" line="1881"/>
        <source>Edit record</source>
        <translation>Modifier l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1985"/>
        <location filename="../MainWindow.cpp" line="4327"/>
        <location filename="../MainWindow.cpp" line="4331"/>
        <location filename="../MainWindow.cpp" line="4335"/>
        <location filename="../MainWindow.h" line="212"/>
        <location filename="../MainWindow.h" line="216"/>
        <location filename="../MainWindow.h" line="220"/>
        <source>Median</source>
        <translation>Médian</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1987"/>
        <source>Click to swap Average and Median</source>
        <translation>Cliquer pour échanger moyenne et médiane</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2082"/>
        <source>Click to swap Legend and Label</source>
        <translation>Cliquer pour échanger la légende et le label</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2159"/>
        <source>No records to preview for selected time range!</source>
        <translation>Aucun enregistrement à prévisualiser pour la plage de temps sélectionnée&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2191"/>
        <source>No records to print for selected time range!</source>
        <translation>Aucun enregistrement à imprimer pour la plage de temps sélectionnée&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2237"/>
        <location filename="../MainWindow.cpp" line="2287"/>
        <location filename="../MainWindow.cpp" line="2363"/>
        <location filename="../MainWindow.cpp" line="2494"/>
        <source>User %1</source>
        <translation>Utilisateur %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>DATE</source>
        <translation>DATE</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>TIME</source>
        <translation>TEMPS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>IHB</source>
        <translation>IHB</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>COMMENT</source>
        <translation>COMMENTAIRE</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>PPR</source>
        <translation>PPR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>MOV</source>
        <translation>MOUV</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2559"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>Impossible de créer un courriel car la génération de la base64 pour la pièce jointe «&#xa0;%1&#xa0;» a échoué&#xa0;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4007"/>
        <source>Chart</source>
        <translation>Graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4024"/>
        <source>Table</source>
        <translation>Tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4041"/>
        <source>Statistic</source>
        <translation>Statistique</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4059"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>Impossible d&apos;ouvrir le courriel «&#xa0;%1&#xa0;»&#xa0;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4069"/>
        <source>Could not start e-mail client!</source>
        <translation>Impossible de lancer le client de messagerie&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4075"/>
        <source>Select Icon Color</source>
        <translation>Sélectionnez la couleur de l&apos;icône</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4081"/>
        <source>Restart application to apply new icon color.</source>
        <translation>Redémarrez l&apos;application pour appliquer la nouvelle couleur de l&apos;icône.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4167"/>
        <source>Thanks to all translators:</source>
        <translation>Merci à tous les traducteurs&#xa0;:</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4167"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4187"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation>Veuillez acheter un bon d&apos;achat du montant souhaité par l&apos;intermédiaire de votre compte Amazon, sélectionnez Courriel à lazyt@mailbox.org comme mode de livraison et spécifiez «&#xa0;UBPM&#xa0;» comme message.

Merci beaucoup&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4204"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation>Veuillez m&apos;envoyer une demande par courriel à l&apos;adresse lazyt@mailbox.org afin que je puisse vous fournir les informations relatives à mon compte bancaire actuel.

Merci beaucoup&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2231"/>
        <location filename="../MainWindow.cpp" line="2281"/>
        <location filename="../MainWindow.cpp" line="2357"/>
        <location filename="../MainWindow.cpp" line="2488"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Crée avec UBPM pour
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2232"/>
        <location filename="../MainWindow.cpp" line="2282"/>
        <location filename="../MainWindow.cpp" line="2358"/>
        <location filename="../MainWindow.cpp" line="2489"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Logiciel libre et gratuit
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2625"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importer depuis CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2625"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>Fichier CSV (*.csv);;Fichier XML (*.xml);;FichierJSON (*.json);;Fichier SQL (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2681"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossible d&apos;ouvrir «&#xa0;%1&#xa0;»&#xa0;!

Raison&#xa0;: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1025"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Mesures&#xa0;: %1 | Irrégulier&#xa0;: %2 | Mouvement&#xa0;: %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="552"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Cher Dr NomduPraticien,

veuillez trouver ci-joint les données de ma tension artérielle pour ce mois-ci.

Meilleures salutations,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1030"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Mesures : 0 | Irrégulier : 0 | Mouvement : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2237"/>
        <location filename="../MainWindow.cpp" line="2287"/>
        <location filename="../MainWindow.cpp" line="2363"/>
        <location filename="../MainWindow.cpp" line="2494"/>
        <source>%1 (Age: %2, Height: %3cm, Weight: %4Kg, BMI: %5)</source>
        <translation>%1 (Âge : %2, Taille : %3cm, Poids : %4Kg, BMI : %5)</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2663"/>
        <location filename="../MainWindow.cpp" line="3896"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Importation réussie de %n enregistrement depuis %1.

     Utilisateur 1 : %2
     Utilisateur 2 : %3</numerusform>
            <numerusform>Importation réussie de %n enregistrements depuis %1.

     Utilisateur 1 : %2
     Utilisateur 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2667"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Saut de %n enregistrement invalide&#xa0;!</numerusform>
            <numerusform>Saut de %n enregistrements invalides&#xa0;!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2672"/>
        <location filename="../MainWindow.cpp" line="3900"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Sauter %n enregistrement en double&#xa0;!</numerusform>
            <numerusform>Sauter %n enregistrements en double&#xa0;!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3051"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Cela ne ressemble pas à une base de données UBPM&#xa0;!

Peut-être de mauvais paramètres de cryptage/mot de passe&#xa0;?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3104"/>
        <location filename="../MainWindow.cpp" line="3108"/>
        <source>Export to %1</source>
        <translation>Exporter vers %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3104"/>
        <location filename="../MainWindow.cpp" line="3108"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 Fichier (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3144"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossible de créer &quot;%1&quot; !

Motif : %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3150"/>
        <source>The database is empty, no records to export!</source>
        <translation>La base de données est vide, pas d&apos;enregistrement à exporter&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3501"/>
        <source>Morning</source>
        <translation>Matin</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3501"/>
        <source>Afternoon</source>
        <translation>Après-midi</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3540"/>
        <source>Week</source>
        <translation>Semaine</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3595"/>
        <source>Quarter</source>
        <translation>Trimestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3624"/>
        <source>Half Year</source>
        <translation>Semestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3642"/>
        <source>Year</source>
        <translation>Année</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3830"/>
        <source>Really delete all records for user %1?</source>
        <translation>Supprimer réellement tous les enregistrements de l&apos;utilisateur %1 ?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3845"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Tous les enregistrements de l&apos;utilisateur %1 sont supprimés et la base de données existante est sauvegardée dans ubpm.sql.bak.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3953"/>
        <source>Really delete all records?</source>
        <translation>Vraiment effacer tous les enregistrements&#xa0;?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3964"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Tous les enregistrements sont supprimés et la base de données existante ubpm.sql est mise à la poubelle.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4324"/>
        <location filename="../MainWindow.cpp" line="4328"/>
        <location filename="../MainWindow.cpp" line="4332"/>
        <location filename="../MainWindow.h" line="209"/>
        <location filename="../MainWindow.h" line="213"/>
        <location filename="../MainWindow.h" line="217"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4325"/>
        <location filename="../MainWindow.cpp" line="4329"/>
        <location filename="../MainWindow.cpp" line="4333"/>
        <location filename="../MainWindow.h" line="210"/>
        <location filename="../MainWindow.h" line="214"/>
        <location filename="../MainWindow.h" line="218"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4398"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Impossible d&apos;ouvrir le fichier du thème «&#xa0;%1&#xa0;»&#xa0;!

Motif&#xa0;: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1853"/>
        <location filename="../MainWindow.cpp" line="1870"/>
        <location filename="../MainWindow.cpp" line="5230"/>
        <source>Delete record</source>
        <translation>Effacer l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4251"/>
        <source>Could not load application translation for &quot;%1&quot;.</source>
        <translation>Impossible de charger la traduction de l&apos;application pour «&#xa0;%1&#xa0;».</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4260"/>
        <source>Could not load plugin translation for &quot;%1&quot;.</source>
        <translation>Impossible de charger la traduction du plugin pour «&#xa0;%1&#xa0;».</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4269"/>
        <source>Could not load base translation for &quot;%1&quot;.

Internal base translations (like &quot;Yes/No&quot;) are not available.

Don&apos;t show this warning again?</source>
        <translation>Impossible de charger la traduction de base pour «&#xa0;%1&#xa0;».

Les traductions de base internes (comme «&#xa0;Oui/Non&#xa0;») ne sont pas disponibles.

Ne plus afficher cet avertissement&#xa0;?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5157"/>
        <source>Show Heartrate</source>
        <translation>Afficher la fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5193"/>
        <location filename="../MainWindow.cpp" line="5204"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>Les symboles et les lignes ne peuvent pas être désactivés tous les deux&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5232"/>
        <source>Show record</source>
        <translation>Afficher l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1854"/>
        <location filename="../MainWindow.cpp" line="1877"/>
        <location filename="../MainWindow.cpp" line="5233"/>
        <source>Hide record</source>
        <translation>Masquer l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1872"/>
        <location filename="../MainWindow.cpp" line="5244"/>
        <source>Really delete selected record?</source>
        <translation>Vraiment effacer les enregistrements sélectionnés&#xa0;?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5149"/>
        <source>Dynamic Scaling</source>
        <translation>Mise à l&apos;échelle dynamique</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5150"/>
        <source>Colored Stripes</source>
        <translation>Rayures colorées</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5152"/>
        <source>Show Symbols</source>
        <translation>Afficher les symboles</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5153"/>
        <source>Show Lines</source>
        <translation>Afficher les lignes</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5155"/>
        <source>Colored Symbols</source>
        <translation>Symboles colorés</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5265"/>
        <source>Show Median</source>
        <translation>Afficher la médiane</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5266"/>
        <source>Show Values</source>
        <translation>Afficher les valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5317"/>
        <source>Really quit program?</source>
        <translation>Vraiment quitter le programme&#xa0;?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gestionnaire universel de la pression artérielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation>L&apos;application est déjà en cours d&apos;exécution et les instances multiples ne sont pas autorisées.

Passez à l&apos;instance en cours ou fermez-la et réessayez.</translation>
    </message>
</context>
</TS>
