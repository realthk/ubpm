<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Análisis de datos</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Consulta</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="206"/>
        <source>Results</source>
        <translation>Resultados</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation>Sistólica</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation>Diastólica</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation>Presión de pulso</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation>Frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation>F. cardíaca irregular</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Movement</source>
        <translation>Movimiento durante la toma</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="169"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="199"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="48"/>
        <source>Could not create memory database!

%1</source>
        <translation>¡No se pudo crear la base de datos!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="208"/>
        <source>No results for this query found!</source>
        <translation>¡No se han encontrado resultados para esta consulta!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 para %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Resultado</numerusform>
            <numerusform>Resultados</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>User %1</source>
        <translation>Usuario %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Coincidencia</numerusform>
            <numerusform>Coincidencias</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Registro</numerusform>
            <numerusform>Registros</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Guía</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="60"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>Guía %1 no encontrada, mostrando guía EN en su lugar.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="52"/>
        <source>%1 guide not found!</source>
        <translation>¡Guía %1 no encontrada!</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation type="unfinished">Fecha</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation type="unfinished">Sistólica</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation type="unfinished">Frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation type="unfinished">Hora</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation type="unfinished">Diastólica</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation type="unfinished">Comentario</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="783"/>
        <source>Migrate User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="812"/>
        <source>Migrate User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="838"/>
        <source>Copy to Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="858"/>
        <source>Select predefined settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="892"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="903"/>
        <source>Test Data Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="917"/>
        <source>Start Data Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="250"/>
        <source>Choose Data Source for Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="250"/>
        <source>CSV File (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="303"/>
        <source>%L1 Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%1 Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="318"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation type="unfinished">¡No se pudo abrir «%1»!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="340"/>
        <source>Startline can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="346"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="354"/>
        <source>Line %1 = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="359"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="366"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="377"/>
        <source>Elements can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="383"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="389"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="396"/>
        <source>Date format can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="402"/>
        <source>Single date format must also contain a time format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="409"/>
        <source>Date position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="415"/>
        <source>Date position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="422"/>
        <source>Time position requires also a time parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="428"/>
        <source>Time format requires also a time position.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="434"/>
        <source>Time position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="441"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="447"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="454"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="460"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="467"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="473"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="480"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="486"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="493"/>
        <source>Movement position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="499"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="506"/>
        <source>Comment position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="522"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="538"/>
        <source>Date format is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="545"/>
        <source>Time format is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="555"/>
        <source>Date/Time format is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="745"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="749"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation type="unfinished">
            <numerusform>¡%n registro inválido omitido!</numerusform>
            <numerusform>¡%n registros inválidos omitidos!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="754"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation type="unfinished">
            <numerusform>¡%n registro duplicado omitido!</numerusform>
            <numerusform>¡%n registros duplicados omitidos!</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Registro manual</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Registro de datos</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Añadir registro para %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Seleccionar fecha y hora</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Pulso irregular</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Introduzca la presión sistólica</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>Introduzca la presión diastólica</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Introduzca la frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Movimiento</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Introduzca un comentario opcional</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Mostrar un mensaje tras la creación, modificación o eliminación exitosa de un registro</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Usuario 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Usuario 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="33"/>
        <location filename="../DialogRecord.cpp" line="186"/>
        <source>Modify</source>
        <translation>Modificar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="131"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>¡No se pudo eliminar el registro!

No existe ningún registro con esta fecha y hora.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="137"/>
        <source>Data record successfully deleted.</source>
        <translation>Registr eliminado con éxito.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="149"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>¡Introduzca un valor válido para la presión sistólica!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="159"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>¡Introduzca un valor válido para la presión diastólica!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="168"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>¡Introduzca un valor válido para la frecuencia cardíaca!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="190"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>¡No se pudo modificar el registro!

No existe ningún registro con esta fecha y hora.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="196"/>
        <source>Data Record successfully modified.</source>
        <translation>Registr modificado con éxito.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="205"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>¡No se pudo crear el registro!

Ya existe un registro con esta fecha y hora.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="211"/>
        <source>Data Record successfully created.</source>
        <translation>Registr creado con éxito.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Base de datos</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="52"/>
        <source>Location</source>
        <translation>Ubicación</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="58"/>
        <source>Current Location</source>
        <translation>Ubicación actual</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="74"/>
        <source>Change Location</source>
        <translation>Cambiar ubicación</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="100"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Encriptar con SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="103"/>
        <source>Encryption</source>
        <translation>Encriptación</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="115"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="128"/>
        <source>Show Password</source>
        <translation>Mostrar contraseña</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="153"/>
        <source>Automatic Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="156"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="173"/>
        <source>Current Backup Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="186"/>
        <source>Change Backup Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="213"/>
        <source>Backup Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="223"/>
        <source>Daily</source>
        <translation type="unfinished">Diaria</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="232"/>
        <source>Weekly</source>
        <translation type="unfinished">Semanal</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="241"/>
        <source>Monthly</source>
        <translation type="unfinished">Mensual</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="253"/>
        <source>Keep Copies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="282"/>
        <source>User</source>
        <translation>Usuario</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <location filename="../DialogSettings.ui" line="574"/>
        <source>Mandatory Information</source>
        <translation>Información necesaria</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="317"/>
        <location filename="../DialogSettings.ui" line="580"/>
        <source>Male</source>
        <translation>Hombre</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="346"/>
        <location filename="../DialogSettings.ui" line="609"/>
        <source>Female</source>
        <translation>Mujer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="372"/>
        <location filename="../DialogSettings.ui" line="644"/>
        <source>Age Group</source>
        <translation>Grupo de edad</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="442"/>
        <location filename="../DialogSettings.ui" line="714"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="458"/>
        <location filename="../DialogSettings.ui" line="730"/>
        <source>Additional Information</source>
        <translation>Información adicional</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="486"/>
        <location filename="../DialogSettings.ui" line="758"/>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="547"/>
        <location filename="../DialogSettings.ui" line="819"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="844"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="850"/>
        <location filename="../DialogSettings.cpp" line="62"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Complementos de importación [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="865"/>
        <source>Please choose Device Plugin…</source>
        <translation>Por favor, elija un complemento de dispositivo…</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="880"/>
        <source>Show Device Image</source>
        <translation>Mostrar imagen del dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="903"/>
        <source>Show Device Manual</source>
        <translation>Mostrar manual del dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="948"/>
        <source>Open Website</source>
        <translation>Abrir página web</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="961"/>
        <source>Maintainer</source>
        <translation>Mantenedor</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="989"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="996"/>
        <source>Send E-Mail</source>
        <translation>Enviar mensaje por correo electrónico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1009"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1016"/>
        <source>Producer</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1033"/>
        <source>Chart</source>
        <translation>Gráfico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1064"/>
        <source>Symbols</source>
        <translation>Símbolos</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1073"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1097"/>
        <location filename="../DialogSettings.ui" line="1125"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1141"/>
        <source>Colored Areas</source>
        <translation>Áreas de colores</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1147"/>
        <source>Healthy Ranges</source>
        <translation>Rangos saludables</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1160"/>
        <source>X-Axis Range</source>
        <translation>Rango del eje X</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1166"/>
        <source>Dynamic Scaling</source>
        <translation>Escalado dinámico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1179"/>
        <source>Lines</source>
        <translation>Líneas</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1188"/>
        <location filename="../DialogSettings.ui" line="1216"/>
        <source>Width</source>
        <translation>Ancho</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1245"/>
        <source>Show Heartrate</source>
        <translation>Mostrar frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1251"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Mostrar frecuencia cardíaca en la vista de gráfico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1261"/>
        <source>Print Heartrate</source>
        <translation>Imprimir frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1267"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Imprimir frecuencia cardíaca en una página separada</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1288"/>
        <location filename="../DialogSettings.ui" line="1563"/>
        <source>Systolic</source>
        <translation>Sistólica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1376"/>
        <location filename="../DialogSettings.ui" line="1651"/>
        <source>Diastolic</source>
        <translation>Diastólica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1464"/>
        <location filename="../DialogSettings.ui" line="1739"/>
        <source>Heartrate</source>
        <translation>Frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1836"/>
        <source>Table</source>
        <translation>Tabla</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1867"/>
        <location filename="../DialogSettings.ui" line="2164"/>
        <source>Diastolic Warnlevel</source>
        <translation>Nivel de aviso para la presión diastólica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1921"/>
        <location filename="../DialogSettings.ui" line="2104"/>
        <source>Systolic Warnlevel</source>
        <translation>Nivel de aviso para la presión sistólica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1981"/>
        <location filename="../DialogSettings.ui" line="2218"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Nivel de aviso para la presión de pulso</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2035"/>
        <location filename="../DialogSettings.ui" line="2272"/>
        <source>Heartrate Warnlevel</source>
        <translation>Nivel de aviso para la frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2337"/>
        <source>Statistic</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2343"/>
        <source>Bar Type</source>
        <translation>Tipo de barra</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2349"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Mostrar barras de mediana en vez de barras de media</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2359"/>
        <source>Legend Type</source>
        <translation>Tipo de leyenda</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2365"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Mostrar values como leyendas en vez de como descripciones</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2380"/>
        <source>E-Mail</source>
        <translation>Correo electrónico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2388"/>
        <source>Address</source>
        <translation>Dirección de correo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2407"/>
        <source>Subject</source>
        <translation>Asunto</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2425"/>
        <source>Message</source>
        <translation>Mensaje</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2446"/>
        <source>Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2452"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2458"/>
        <source>Log Device Communication to File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Import Measurements automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2475"/>
        <source>Bluetooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2481"/>
        <source>Discover Device automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2488"/>
        <source>Connect Device automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2503"/>
        <source>Update</source>
        <translation>Actualizaciones</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2509"/>
        <source>Autostart</source>
        <translation>Inicio</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2515"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Buscar actualizaciones en línas cada vez que se inicie el programa</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2528"/>
        <source>Notification</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2534"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Mostrar siempre el resultado después de buscar actualizaciones</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2550"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2567"/>
        <source>Reset</source>
        <translation>Reestablecer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2584"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="159"/>
        <source>Choose Database Location</source>
        <translation>Escoja una ubicación para la base de datos</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="169"/>
        <source>Choose Database Backup Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="205"/>
        <source>Could not display manual!

%1</source>
        <translation>¡No se pudo mostar el manual!

%1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="328"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="330"/>
        <source>Overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="331"/>
        <source>Merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="332"/>
        <source>Swap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="334"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="342"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="372"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>¡La encriptación SQL no puede ser habilitada sin una contraseña y va a deshabilitarse!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="379"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="384"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Por favor, introduzca valores válidos para la información adicional del usuario 1.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="391"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Por favor, introduzca valores válidos para la información adicional del usuario 2.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="398"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>La edad introducida no coincide con el grupo de edad seleccionado para el usuario 1.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="405"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>La edad introducida no coincide con el grupo de edad seleccionado para el usuario 2.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="413"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Por favor, habilite símbolos o líneas para el gráfico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="420"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Por favor, introduzca una dirección de correo válida.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="427"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Por favor, introduzca un asunto para el mensaje.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="434"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>¡El mensaje de correo electrónico debe contener $CHART, $TABLE y/o $STATS!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="545"/>
        <source>User 1</source>
        <translation>Usuario 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="559"/>
        <source>User 2</source>
        <translation>Usuario 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="672"/>
        <source>Blood Pressure Report</source>
        <translation>Reporte de presión arterial</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="673"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Estimado Dr. Casas:

Le adjunto mis datos de presión arterial para ese mes.

Reciba un cordial saludo,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="715"/>
        <source>Abort setup and discard all changes?</source>
        <translation>¿Desea cancelar la configuración y descartar todos los cambios?</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Actualización en línea</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Versión disponible</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Tamaño de la actualización</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Versión instalada</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="199"/>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="219"/>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>¡ADVERTENCIA DE SSL - LEER DETENIDAMENTE!

Problema de conexión de red:

%1
¿Desea continuar de todos modos?</numerusform>
            <numerusform>¡ADVERTENCIA DE SSL - LEER DETENIDAMENTE!

Problemas de conexión de red:

%1
¿Desea continuar de todos modos?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Downloading update failed!

%1</source>
        <translation>¡La descarga de una actualización ha fallado!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Checking update failed!

%1</source>
        <translation>¡La comprobación de una actualización ha fallado!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="140"/>
        <source>Unexpected response from update server!</source>
        <translation>¡Respuesta inesperada del servidor de actualizaciones!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="157"/>
        <source>*%1 not found</source>
        <translation>*%1 no encontrado</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="167"/>
        <source>No new version found.</source>
        <translation>No se ha encontrado una nueva versión.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="187"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>¡La actualización no tiene el tamaño esperado!

%L1 : %L2

Reintente la descarga…</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="191"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Actualización guardada en %1.

¿Desea iniciar la nueva versión?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="204"/>
        <source>Could not start new version!</source>
        <translation>¡No se pudo iniciar la nueva versión!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="211"/>
        <source>Could not save update to %1!

%2</source>
        <translation>¡No se pudo guardar la actualización en %1!

%2</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>Really abort download?</source>
        <translation>¿De verdad desea cancelar la descarga?</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gestor universal de presión sanguínea</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Mostrar periodo anterior</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Mostrar periodo siguiente</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Vista de gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Vista de tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="126"/>
        <location filename="../MainWindow.cpp" line="4313"/>
        <source>Systolic</source>
        <translation>Sistólica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="127"/>
        <location filename="../MainWindow.cpp" line="4314"/>
        <source>Diastolic</source>
        <translation>Diastólica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Presión de pulso</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="128"/>
        <location filename="../MainWindow.cpp" line="4315"/>
        <source>Heartrate</source>
        <translation>Frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Irregular</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Movimiento</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Vista de estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>15 minutos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>30 minutos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Horaria</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>6 horas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>12 horas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Diaria</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation>Semanal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation>Mensual</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation>Trimestral</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation>Semestral</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation>Anual</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation>Última semana</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation>Últimas 2 semanas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation>Últimas 3 semanas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation>Últimas 4 semanas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation>Últimos 3 meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation>Últimos 6 meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation>Últimos 9 meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation>Últimos 12 meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation>Todos los valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1129"/>
        <source>Make Donation</source>
        <translation>Hacer donación</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1132"/>
        <source>Donation</source>
        <translation>Donación</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Configuration</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1161"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1170"/>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1179"/>
        <source>Style</source>
        <translation>Estilo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1188"/>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1213"/>
        <source>Database</source>
        <translation>Base de datos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1217"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1229"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1244"/>
        <source>Clear</source>
        <translation>Borrar valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1331"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1334"/>
        <location filename="../MainWindow.ui" line="1337"/>
        <source>Quit Program</source>
        <translation>Salir del programa</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1349"/>
        <source>About</source>
        <translation>Acerca de...</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1352"/>
        <location filename="../MainWindow.ui" line="1355"/>
        <source>About Program</source>
        <translation>Acerca de este programa</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1364"/>
        <source>Guide</source>
        <translation>Guía</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1367"/>
        <location filename="../MainWindow.ui" line="1370"/>
        <source>Show Guide</source>
        <translation>Mostrar guía</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Update</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Check Update</source>
        <translation>Buscar actualizaciones</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>Bugreport</source>
        <translation>Informe de errores</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>Send Bugreport</source>
        <translation>Informar de un error</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Change Settings</source>
        <translation>Modificar ajustes</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>From Device</source>
        <translation>Desde un dispositivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1436"/>
        <location filename="../MainWindow.ui" line="1439"/>
        <source>Import From Device</source>
        <translation>Importar desde un dispositivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>From File</source>
        <translation>Desde un archivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1454"/>
        <location filename="../MainWindow.ui" line="1457"/>
        <source>Import From File</source>
        <translation>Importar desde un archivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>From Input</source>
        <translation>Manualmente</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1469"/>
        <location filename="../MainWindow.ui" line="1472"/>
        <source>Import From Input</source>
        <translation>Importar manualmente</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>To CSV</source>
        <translation>A CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Export To CSV</source>
        <translation>Exportar a CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>To XML</source>
        <translation>A XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Export To XML</source>
        <translation>Exportar a XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>To JSON</source>
        <translation>A JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Export To JSON</source>
        <translation>Exportar a JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To SQL</source>
        <translation>A SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To SQL</source>
        <translation>Exportar a SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1544"/>
        <source>To PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Export To PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1559"/>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Migrate from Vendor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>Print Chart</source>
        <translation>Imprimir gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Print Chart View</source>
        <translation>Imprimir vista de gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>Print Table</source>
        <translation>Imprimir tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Print Table View</source>
        <translation>Imprimir vista de tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Print Statistic</source>
        <translation>Imprimir estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Print Statistic View</source>
        <translation>Imprimir vista de estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1622"/>
        <source>Preview Chart</source>
        <translation>Previsualizar gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <location filename="../MainWindow.ui" line="1628"/>
        <source>Preview Chart View</source>
        <translation>Previsualizar vista de gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1637"/>
        <source>Preview Table</source>
        <translation>Previsualizar tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <location filename="../MainWindow.ui" line="1643"/>
        <source>Preview Table View</source>
        <translation>Previsualizar vista de tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1652"/>
        <source>Preview Statistic</source>
        <translation>Previsualizar estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <location filename="../MainWindow.ui" line="1658"/>
        <source>Preview Statistic View</source>
        <translation>Previsualizar vista de estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1667"/>
        <location filename="../MainWindow.ui" line="1670"/>
        <location filename="../MainWindow.ui" line="1673"/>
        <source>Clear All</source>
        <translation>Borrar todos los valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Clear User 1</source>
        <translation>Borrar valores del usuario 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Clear User 2</source>
        <translation>Borrar valores del usuario 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1727"/>
        <location filename="../MainWindow.ui" line="1730"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="4094"/>
        <location filename="../MainWindow.cpp" line="4095"/>
        <location filename="../MainWindow.cpp" line="4096"/>
        <location filename="../MainWindow.cpp" line="4097"/>
        <location filename="../MainWindow.cpp" line="4347"/>
        <location filename="../MainWindow.cpp" line="4348"/>
        <location filename="../MainWindow.cpp" line="4349"/>
        <location filename="../MainWindow.cpp" line="4350"/>
        <source>Switch To %1</source>
        <translation>Cambiar a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1766"/>
        <source>Analysis</source>
        <translation>Análisis</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1769"/>
        <location filename="../MainWindow.ui" line="1772"/>
        <source>Analyze Records</source>
        <translation>Analizar registros</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1791"/>
        <location filename="../MainWindow.ui" line="1794"/>
        <location filename="../MainWindow.ui" line="1797"/>
        <source>Time Mode</source>
        <translation>Modo de tiempo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1809"/>
        <source>PayPal</source>
        <translation>PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1812"/>
        <location filename="../MainWindow.ui" line="1815"/>
        <source>Donate via PayPal</source>
        <translation>Donar con PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1827"/>
        <location filename="../MainWindow.ui" line="1830"/>
        <source>Donate via Liberapay</source>
        <translation>Donar con Liberapay</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Donate via Amazon</source>
        <translation>Donar con Amazon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <location filename="../MainWindow.ui" line="1860"/>
        <source>Donate via SEPA</source>
        <translation>Donar con SEPA</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1869"/>
        <source>Translation</source>
        <translation>Traducción</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <location filename="../MainWindow.ui" line="1875"/>
        <source>Contribute Translation</source>
        <translation>Contribuir traducción</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1884"/>
        <source>E-Mail</source>
        <translation>Correo electrónico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1887"/>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Send E-Mail</source>
        <translation>Enviar correo electrónico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1902"/>
        <source>Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1905"/>
        <location filename="../MainWindow.ui" line="1908"/>
        <source>Change Icon Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1919"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1922"/>
        <location filename="../MainWindow.ui" line="1925"/>
        <source>System Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1933"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1936"/>
        <location filename="../MainWindow.ui" line="1939"/>
        <source>Light Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1947"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <location filename="../MainWindow.ui" line="1953"/>
        <source>Dark Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="488"/>
        <location filename="../MainWindow.cpp" line="4094"/>
        <location filename="../MainWindow.cpp" line="4095"/>
        <location filename="../MainWindow.cpp" line="4347"/>
        <location filename="../MainWindow.cpp" line="4348"/>
        <source>User 1</source>
        <translation>Usuario 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="498"/>
        <location filename="../MainWindow.cpp" line="4096"/>
        <location filename="../MainWindow.cpp" line="4097"/>
        <location filename="../MainWindow.cpp" line="4349"/>
        <location filename="../MainWindow.cpp" line="4350"/>
        <source>User 2</source>
        <translation>Usuario 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="76"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="4283"/>
        <location filename="../MainWindow.cpp" line="4284"/>
        <source>Records For Selected User</source>
        <translation>Registros para el usuario seleccionado</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="84"/>
        <location filename="../MainWindow.cpp" line="85"/>
        <location filename="../MainWindow.cpp" line="4286"/>
        <location filename="../MainWindow.cpp" line="4287"/>
        <source>Select Date &amp; Time</source>
        <translation>Seleccionar fecha y hora</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="136"/>
        <location filename="../MainWindow.cpp" line="4317"/>
        <source>Systolic - Value Range</source>
        <translation>Sistólica — intervalo de valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="137"/>
        <location filename="../MainWindow.cpp" line="4318"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastólica — intervalo de valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="138"/>
        <location filename="../MainWindow.cpp" line="4319"/>
        <source>Heartrate - Value Range</source>
        <translation>Frecuencia cardíaca — intervalo de valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="140"/>
        <location filename="../MainWindow.cpp" line="4320"/>
        <source>Systolic - Target Area</source>
        <translation>Sistólica — área objetivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="141"/>
        <location filename="../MainWindow.cpp" line="4321"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastólica — área objetivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="142"/>
        <location filename="../MainWindow.cpp" line="4322"/>
        <source>Heartrate - Target Area</source>
        <translation>Frecuencia cardíaca — área objetivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="354"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="383"/>
        <location filename="../MainWindow.cpp" line="406"/>
        <source>Could not backup database:

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="396"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="423"/>
        <source>Could not register icons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="551"/>
        <source>Blood Pressure Report</source>
        <translation>Reporte de tensión arterial</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="552"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Estimado Dr. Casas:

Le adjunto mis datos de presión arterial para ese mes.

Reciba un cordial saludo,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1025"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Registros : %1  |  Irregulares : %2  |  Movidos : %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1026"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1030"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Registros : 0  |  Irregulares : 0  |  Movidos : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1031"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <source>Athlete</source>
        <translation>Atleta</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1515"/>
        <source>Excellent</source>
        <translation>Exelente</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1515"/>
        <source>Optimal</source>
        <translation>Óptimo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1516"/>
        <source>Great</source>
        <translation>Muy bueno</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1516"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1517"/>
        <source>Good</source>
        <translation>Bueno</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1517"/>
        <source>High Normal</source>
        <translation>Normal–alto</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1518"/>
        <location filename="../MainWindow.cpp" line="1985"/>
        <location filename="../MainWindow.cpp" line="4326"/>
        <location filename="../MainWindow.cpp" line="4330"/>
        <location filename="../MainWindow.cpp" line="4334"/>
        <location filename="../MainWindow.h" line="211"/>
        <location filename="../MainWindow.h" line="215"/>
        <location filename="../MainWindow.h" line="219"/>
        <source>Average</source>
        <translation>Promedio</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1518"/>
        <source>Hyper 1</source>
        <translation>Híper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <source>Below Average</source>
        <translation>Por debajo de la media</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <source>Hyper 2</source>
        <translation>Híper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <source>Poor</source>
        <translation>Pobre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <source>Hyper 3</source>
        <translation>Híper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1665"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>¡Ha ocurrido un error al escanear el complemento de importación «%1»!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1678"/>
        <location filename="../MainWindow.cpp" line="1700"/>
        <location filename="../MainWindow.cpp" line="4293"/>
        <source>Switch Language to %1</source>
        <translation>Cambiar idioma a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1725"/>
        <location filename="../MainWindow.cpp" line="1740"/>
        <location filename="../MainWindow.cpp" line="4301"/>
        <source>Switch Theme to %1</source>
        <translation>Cambiar tema a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1765"/>
        <location filename="../MainWindow.cpp" line="1785"/>
        <location filename="../MainWindow.cpp" line="4309"/>
        <source>Switch Style to %1</source>
        <translation>Cambiar estilo a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1853"/>
        <location filename="../MainWindow.cpp" line="1870"/>
        <location filename="../MainWindow.cpp" line="5230"/>
        <source>Delete record</source>
        <translation>Eliminar registro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1854"/>
        <location filename="../MainWindow.cpp" line="1877"/>
        <location filename="../MainWindow.cpp" line="5233"/>
        <source>Hide record</source>
        <translation>Ocultar registro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1856"/>
        <location filename="../MainWindow.cpp" line="1881"/>
        <source>Edit record</source>
        <translation>Editar registro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1872"/>
        <location filename="../MainWindow.cpp" line="5244"/>
        <source>Really delete selected record?</source>
        <translation>¿De verdad desear eliminar el registro seleccionado?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1985"/>
        <location filename="../MainWindow.cpp" line="4327"/>
        <location filename="../MainWindow.cpp" line="4331"/>
        <location filename="../MainWindow.cpp" line="4335"/>
        <location filename="../MainWindow.h" line="212"/>
        <location filename="../MainWindow.h" line="216"/>
        <location filename="../MainWindow.h" line="220"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1987"/>
        <source>Click to swap Average and Median</source>
        <translation>Pulse para intercambiar media y mediana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2082"/>
        <source>Click to swap Legend and Label</source>
        <translation>Pulse para intercambiar leyenda y etiqueta</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2159"/>
        <source>No records to preview for selected time range!</source>
        <translation>¡No hay registros que previsualizar en el intervalo temporal seleccionado!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2191"/>
        <source>No records to print for selected time range!</source>
        <translation>¡No hay registros que imprimir en el intervalo temporal seleccionado!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2231"/>
        <location filename="../MainWindow.cpp" line="2281"/>
        <location filename="../MainWindow.cpp" line="2357"/>
        <location filename="../MainWindow.cpp" line="2488"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Creado con UBPM para
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2232"/>
        <location filename="../MainWindow.cpp" line="2282"/>
        <location filename="../MainWindow.cpp" line="2358"/>
        <location filename="../MainWindow.cpp" line="2489"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Libre y de código abierto
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2237"/>
        <location filename="../MainWindow.cpp" line="2287"/>
        <location filename="../MainWindow.cpp" line="2363"/>
        <location filename="../MainWindow.cpp" line="2494"/>
        <source>User %1</source>
        <translation>Usuario %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2237"/>
        <location filename="../MainWindow.cpp" line="2287"/>
        <location filename="../MainWindow.cpp" line="2363"/>
        <location filename="../MainWindow.cpp" line="2494"/>
        <source>%1 (Age: %2, Height: %3cm, Weight: %4Kg, BMI: %5)</source>
        <translation>%1 (Edad: %2, Altura: %3cm, Peso: %4Kg, BMI: %5)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>DATE</source>
        <translation>FECHA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>TIME</source>
        <translation>HORA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>SYS</source>
        <translation>SISTÓLICA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>DIA</source>
        <translation>DIASTÓLICA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>PPR</source>
        <translation>PRESIÓN DE PULSO</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>BPM</source>
        <translation>FRECUENCIA CARDÍACA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>IHB</source>
        <translation>PULSO IRREGULAR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>COMMENT</source>
        <translation>COMENTARIO</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>MOV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2559"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>No se pudo crear el mensaje porque hubo un fallo al generar el código Base64 para el adjunto «%1»

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2625"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importar desde CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2625"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>Archivo CSV (*.csv);;Archivo XML (*.xml);;Archivo JSON (*.json);;Archivo SQL (*.sql)</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2663"/>
        <location filename="../MainWindow.cpp" line="3896"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>¡Se ha importado %n registro exitosamente desde %1.

     Usuario 1 : %2
     Usuario 2 : %3</numerusform>
            <numerusform>¡Se han importado %n registros exitosamente desde %1.

     Usuario 1 : %2
     Usuario 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2667"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>¡%n registro inválido omitido!</numerusform>
            <numerusform>¡%n registros inválidos omitidos!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2672"/>
        <location filename="../MainWindow.cpp" line="3900"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>¡%n registro duplicado omitido!</numerusform>
            <numerusform>¡%n registros duplicados omitidos!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2681"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>¡No se pudo abrir «%1»!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3051"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>¡No parece que sea una base de datos de UBPM!

¿Puede que la contraseña o los ajustes de encriptación sean incorrectos?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3104"/>
        <location filename="../MainWindow.cpp" line="3108"/>
        <source>Export to %1</source>
        <translation>Exportar a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3104"/>
        <location filename="../MainWindow.cpp" line="3108"/>
        <source>%1 File (*.%2)</source>
        <translation>Archivo %1 (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3144"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>¡No se pudo crear «%1»!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3150"/>
        <source>The database is empty, no records to export!</source>
        <translation>La base de datos está vacía, ¡no hay registros que exportar!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3501"/>
        <source>Morning</source>
        <translation>Mañana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3501"/>
        <source>Afternoon</source>
        <translation>Tarde</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3540"/>
        <source>Week</source>
        <translation>Semana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3595"/>
        <source>Quarter</source>
        <translation>Trimestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3624"/>
        <source>Half Year</source>
        <translation>Semestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3642"/>
        <source>Year</source>
        <translation>Año</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3830"/>
        <source>Really delete all records for user %1?</source>
        <translation>¿De verdad desea eliminar todos los registros del usuario %1?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3845"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Todos los registros del usuario %1 han sido eliminados, y la base de datos ha sido guardada en «ubpm.sql.bak»</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3953"/>
        <source>Really delete all records?</source>
        <translation>¿De verdad desea eliminar todos los registros?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3964"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Todos los registros han sido eliminados y la base de datos «ubpm.sql» ha sido movida a la papelera.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4007"/>
        <source>Chart</source>
        <translation>Gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4024"/>
        <source>Table</source>
        <translation>Tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4041"/>
        <source>Statistic</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4059"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>¡No se pudo abrir el correo &quot;%1&quot;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4069"/>
        <source>Could not start e-mail client!</source>
        <translation>¡No se pudo iniciar el cliente de correo!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4075"/>
        <source>Select Icon Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4081"/>
        <source>Restart application to apply new icon color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4167"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4167"/>
        <source>Thanks to all translators:</source>
        <translation>Gracias a todos los traductores:</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4187"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation>Compre un cupón por el importe deseado a través de su cuenta de Amazon, utilice el correo electrónico lazyt@mailbox.org como método de envío y especifique &quot;UBPM&quot; como mensaje.

¡Muchísimas gracias!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4204"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation>Envíeme una solicitud por correo electrónico a lazyt@mailbox.org para que pueda proporcionarle la información de mi cuenta bancaria.

¡Muchas gracias!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4251"/>
        <source>Could not load application translation for &quot;%1&quot;.</source>
        <translation>No se pudo cargar la traducción de la aplicación para &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4260"/>
        <source>Could not load plugin translation for &quot;%1&quot;.</source>
        <translation>No se pudo cargar la traducción de los complementos para &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4269"/>
        <source>Could not load base translation for &quot;%1&quot;.

Internal base translations (like &quot;Yes/No&quot;) are not available.

Don&apos;t show this warning again?</source>
        <translation>No se pudo cargar la traducción base para &quot;%1&quot;.

Las traducciones básicas internas (como &quot;Sí/No&quot;) no están disponibles.

¿No mostrar este mensaje de nuevo?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4324"/>
        <location filename="../MainWindow.cpp" line="4328"/>
        <location filename="../MainWindow.cpp" line="4332"/>
        <location filename="../MainWindow.h" line="209"/>
        <location filename="../MainWindow.h" line="213"/>
        <location filename="../MainWindow.h" line="217"/>
        <source>Minimum</source>
        <translation>Mínimo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4325"/>
        <location filename="../MainWindow.cpp" line="4329"/>
        <location filename="../MainWindow.cpp" line="4333"/>
        <location filename="../MainWindow.h" line="210"/>
        <location filename="../MainWindow.h" line="214"/>
        <location filename="../MainWindow.h" line="218"/>
        <source>Maximum</source>
        <translation>Máximo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4398"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>No se pudo abrir el archivo de tema «%1»

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5149"/>
        <source>Dynamic Scaling</source>
        <translation>Escalado dinámico</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5150"/>
        <source>Colored Stripes</source>
        <translation>Bandas de colores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5152"/>
        <source>Show Symbols</source>
        <translation>Mostrar símbolos</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5153"/>
        <source>Show Lines</source>
        <translation>Mostrar líneas</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5155"/>
        <source>Colored Symbols</source>
        <translation>Símbolos coloreados</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5157"/>
        <source>Show Heartrate</source>
        <translation>Mostrar frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5193"/>
        <location filename="../MainWindow.cpp" line="5204"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>No es posible deshabilitar símbolos y líneas al mismo tiempo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5232"/>
        <source>Show record</source>
        <translation>Mostrar toma</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5265"/>
        <source>Show Median</source>
        <translation>Mostrar mediana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5266"/>
        <source>Show Values</source>
        <translation>Mostrar valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5317"/>
        <source>Really quit program?</source>
        <translation>¿Está seguro de que desea salir?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gestor universal de presión sanguínea</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
