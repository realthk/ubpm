<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Analisi dei Dati</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Interrogazione</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="206"/>
        <source>Results</source>
        <translation>Risultati</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Ora</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation>Sistolica</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation>Diastolica</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation>Pressione P.</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation>Pulsazioni</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation>Irregolare</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Movement</source>
        <translation>Movimento</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Invisible</source>
        <translation>Invisibile</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="169"/>
        <source>Comment</source>
        <translation>Commento</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="199"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="48"/>
        <source>Could not create memory database!

%1</source>
        <translation>Impossibile creare il database di memoria!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="208"/>
        <source>No results for this query found!</source>
        <translation>Nessun risultato trovato per questa ricerca!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 per %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Risultato</numerusform>
            <numerusform>Risultati</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Corrispondenza</numerusform>
            <numerusform>Corrispondenze</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Registrazione</numerusform>
            <numerusform>Registrazioni</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>User %1</source>
        <translation>Utente %1</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="60"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>guida %1 non trovata, mostra invece la guida Inglese.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="52"/>
        <source>%1 guide not found!</source>
        <translation>guida %1 non trovata!</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Migrazione dei Dati</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Origine</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation>Apri Origine Dati</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Parametri</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Formato Data</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation>Rilevamento Irregolare</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation>Linea di partenza</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Elementi per Linea</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Delimitatore</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Formato Orario</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation>Rilevazione del Movimento</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation>Lingua della data</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation>Posizioni</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation>Irregolare</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation>Sistolica</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation>Pulsazioni</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation>Movimento</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation>Ora</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation>Diastolica</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation type="unfinished">Commento</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Risultato</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="783"/>
        <source>Migrate User 1</source>
        <translation>Migra Utente 1</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="812"/>
        <source>Migrate User 2</source>
        <translation>Migra Utente 2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="838"/>
        <source>Copy to Custom</source>
        <translation>Copia su Personalizzato</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="858"/>
        <source>Select predefined settings</source>
        <translation>Seleziona le impostazioni predefinite</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="892"/>
        <source>Custom</source>
        <translation>Personalizzato</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="903"/>
        <source>Test Data Migration</source>
        <translation>Prova Migrazione Dati</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="917"/>
        <source>Start Data Migration</source>
        <translation>Inizio Migrazione Dati</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="250"/>
        <source>Choose Data Source for Migration</source>
        <translation>Scegli Origine Dati per la Migrazione</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="250"/>
        <source>CSV File (*.csv)</source>
        <translation>CSV File (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="303"/>
        <source>%L1 Bytes</source>
        <translation>%L1 Bytes</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%1 Lines</source>
        <translation>%1 Linee</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="318"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossibile aprire &quot;%1&quot;!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="340"/>
        <source>Startline can&apos;t be empty.</source>
        <translation>La linea di partenza non può essere vuota.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="346"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation>La linea di partenza deve essere inferiore al numero di righe.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="354"/>
        <source>Line %1 = %2</source>
        <translation>Linea %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="359"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>Il delimitatore non può essere vuoto.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="366"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation>Linea di partenza non contiene il delimitatore &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="377"/>
        <source>Elements can&apos;t be empty.</source>
        <translation>Gli elementi non possono essere vuoti.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="383"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation>Gli elementi non possono essere inferiori a 4.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="389"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation>Elementi non corrispondenti (trovati %1).</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="396"/>
        <source>Date format can&apos;t be empty.</source>
        <translation>Il Formato della Data non può essere vuoto.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="402"/>
        <source>Single date format must also contain a time format.</source>
        <translation>Il formato della data singola deve contenere anche un formato dell&apos;ora.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="409"/>
        <source>Date position can&apos;t be empty.</source>
        <translation>La posizione della data non può essere vuota.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="415"/>
        <source>Date position must be smaller than elements.</source>
        <translation>La posizione della data deve essere inferiore agli elementi.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="422"/>
        <source>Time position requires also a time parameter.</source>
        <translation>La posizione Ora richiede anche un parametro Ora.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="428"/>
        <source>Time format requires also a time position.</source>
        <translation>Il formato dell&apos;ora richiede anche una posizione dell&apos;ora.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="434"/>
        <source>Time position must be smaller than elements.</source>
        <translation>La posizione Ora deve essere più piccola degli elementi.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="441"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation>La posizione sistolica non può essere vuota.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="447"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation>La posizione sistolica deve essere più piccola degli elementi.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="454"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation>La posizione diastolica non può essere vuota.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="460"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation>La posizione diastolica deve essere più piccola degli elementi.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="467"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation>La posizione del battito cardiaco non può essere vuota.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="473"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation>La posizione del battito cardiaco deve essere inferiore agli elementi.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="480"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation>La posizione irregolare deve essere più piccola degli elementi.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="486"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation>La posizione irregolare richiede anche un parametro irregolare.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="493"/>
        <source>Movement position must be smaller than elements.</source>
        <translation>La posizione movimento deve essere più piccola degli elementi.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="499"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation>La posizione di movimento richiede anche un parametro di movimento.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="506"/>
        <source>Comment position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="522"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation>Le posizioni non possono esistere due volte.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="538"/>
        <source>Date format is invalid.</source>
        <translation>Formato Data non è valido.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="545"/>
        <source>Time format is invalid.</source>
        <translation>Formato Ora non è valido.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="555"/>
        <source>Date/Time format is invalid.</source>
        <translation>Formato Data/Ora non è valido.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="745"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation>
            <numerusform>Migrazione riuscita di %n record per l&apos;utente %1.</numerusform>
            <numerusform>Migrazione riuscita di %n record per l&apos;utente %1.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="749"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n record non valido ignorato!</numerusform>
            <numerusform>%n record non validi ignorati!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="754"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n record duplicato saltato!</numerusform>
            <numerusform>%n record duplicati saltati!</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Registrazione Manuale</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Record Dati</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Aggiungi record per %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Seleziona Data &amp; Ora</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Immettere SYS</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>Immettere DIA</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Immettere BPM</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Battito Cardiaco Irregolare</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Movimento</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Immettere Commento Facoltativo</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Mostra messaggio in caso di riuscita creazione / cancellazione / modifica record</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Crea</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Utente 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Utente 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="33"/>
        <location filename="../DialogRecord.cpp" line="186"/>
        <source>Modify</source>
        <translation>Modificare</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="131"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Impossibile eliminare il record di dati!

Un valore per questa Data e Ora non esiste.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="137"/>
        <source>Data record successfully deleted.</source>
        <translation>Record dati eliminato con successo.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="149"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Inserisci prima un valore valido per &quot;SYS&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="159"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Si prega di inserire prima un valore valido per &quot;DIA&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="168"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Inserisci prima un valore valido per &quot;BPM&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="190"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Impossibile modificare il record di dati

Un valore per questa Data e Ora non esiste.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="196"/>
        <source>Data Record successfully modified.</source>
        <translation>Record dati modificato con successo.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="205"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Impossibile creare il record di dati!

Esiste già una voce per questa data e ora.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="211"/>
        <source>Data Record successfully created.</source>
        <translation>Record dati creato con successo.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="159"/>
        <source>Choose Database Location</source>
        <translation>Scegli percorso Database</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="169"/>
        <source>Choose Database Backup Location</source>
        <translation>Scegli la posizione del backup della base di dati</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="205"/>
        <source>Could not display manual!

%1</source>
        <translation>Impossibile visualizzare il manuale!

%1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="328"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation>La banca dati selezionata esiste già.

Scegli l&apos;azione preferita.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="330"/>
        <source>Overwrite</source>
        <translation>Sovrascrivi</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="331"/>
        <source>Merge</source>
        <translation>Unisci</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="332"/>
        <source>Swap</source>
        <translation>Scambia</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="334"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation>Sovrascrivi:

Sovrascrive la banca dati selezionata con la banca dati corrente.

Unisci:

Unisce la banca dati selezionato alla banca dati corrente.

Scambia:

Elimina la banca dati corrente e utilizza la banca data selezionata.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="342"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation>La banca dati corrente è vuoto.

La banca dati selezionata verrà eliminata all&apos;uscita, se non verranno aggiunti dati.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="372"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>La crittografia SQL non può essere abilitata senza password e sarà disabilitata!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="379"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation>Il backup della base di dati deve trovarsi su un altro disco rigido, partizione o directory.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="384"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Si prega di inserire valori validi per ulteriori informazioni sull&apos;utente 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="391"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Si prega di inserire valori validi per ulteriori informazioni sull&apos;utente 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="398"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>L&apos;età inserita non corrisponde al gruppo di età selezionato per l&apos;utente 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="405"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>L&apos;età inserita non corrisponde al gruppo di età selezionato per l&apos;utente 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="413"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Si prega di abilitare simboli o linee per il grafico!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="420"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Si prega di inserire un indirizzo e-mail valido</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="427"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Si prega di inserire un oggetto e-mail!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="434"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>Il messaggio di posta elettronica deve contenere $CHART, $TABLE e/o $STATS!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="545"/>
        <source>User 1</source>
        <translation>Utente 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="559"/>
        <source>User 2</source>
        <translation>Utente 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="672"/>
        <source>Blood Pressure Report</source>
        <translation>Rapporto pressione sanguigna</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="673"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Egr. dr. Medico di famiglia,

trova in allegato i dati della mia pressione sanguigna per questo mese.

Distinti saluti,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="715"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Annullare la configurazione e annullare tutte le modifiche?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Banca dati</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="52"/>
        <source>Location</source>
        <translation>Percorso</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="58"/>
        <source>Current Location</source>
        <translation>Percorso attuale</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="74"/>
        <source>Change Location</source>
        <translation>Cambia percorso</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="100"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Crittografa con SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="103"/>
        <source>Encryption</source>
        <translation>Crittografia</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="115"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="128"/>
        <source>Show Password</source>
        <translation>Mostra password</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="282"/>
        <source>User</source>
        <translation>Utente</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="317"/>
        <location filename="../DialogSettings.ui" line="580"/>
        <source>Male</source>
        <translation>Maschio</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="346"/>
        <location filename="../DialogSettings.ui" line="609"/>
        <source>Female</source>
        <translation>Femmina</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="442"/>
        <location filename="../DialogSettings.ui" line="714"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <location filename="../DialogSettings.ui" line="574"/>
        <source>Mandatory Information</source>
        <translation>Informazione obbligatoria</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="372"/>
        <location filename="../DialogSettings.ui" line="644"/>
        <source>Age Group</source>
        <translation>Fascia di età</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="458"/>
        <location filename="../DialogSettings.ui" line="730"/>
        <source>Additional Information</source>
        <translation>Informazioni aggiuntive</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Height</source>
        <translation>Altezza</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="547"/>
        <location filename="../DialogSettings.ui" line="819"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="844"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="850"/>
        <location filename="../DialogSettings.cpp" line="62"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Importa i plugin [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="880"/>
        <source>Show Device Image</source>
        <translation>Mostra immagine del dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="903"/>
        <source>Show Device Manual</source>
        <translation>Mostra il manuale del dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="948"/>
        <source>Open Website</source>
        <translation>Apri il sito web</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="961"/>
        <source>Maintainer</source>
        <translation>Manutentore</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="989"/>
        <source>Version</source>
        <translation>Versione</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="996"/>
        <source>Send E-Mail</source>
        <translation>Invia l&apos;e-mail</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1009"/>
        <source>Model</source>
        <translation>Modello</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1016"/>
        <source>Producer</source>
        <translation>Produttore</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1033"/>
        <source>Chart</source>
        <translation>Grafico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1160"/>
        <source>X-Axis Range</source>
        <translation>Intervallo Asse X</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1166"/>
        <source>Dynamic Scaling</source>
        <translation>Ridimensionamento dinamico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1147"/>
        <source>Healthy Ranges</source>
        <translation>Intervalli ideali</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1064"/>
        <source>Symbols</source>
        <translation>Simboli</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1141"/>
        <source>Colored Areas</source>
        <translation>Aree colorate</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="865"/>
        <source>Please choose Device Plugin…</source>
        <translation>Per favore scegli plugin dispositivo…</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1288"/>
        <location filename="../DialogSettings.ui" line="1563"/>
        <source>Systolic</source>
        <translation>Sistolica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1376"/>
        <location filename="../DialogSettings.ui" line="1651"/>
        <source>Diastolic</source>
        <translation>Diastolica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1464"/>
        <location filename="../DialogSettings.ui" line="1739"/>
        <source>Heartrate</source>
        <translation>Pulsazioni</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1836"/>
        <source>Table</source>
        <translation>Tabella</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1921"/>
        <location filename="../DialogSettings.ui" line="2104"/>
        <source>Systolic Warnlevel</source>
        <translation>Avviso Livello Sistolico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1867"/>
        <location filename="../DialogSettings.ui" line="2164"/>
        <source>Diastolic Warnlevel</source>
        <translation>Avviso Livello Diastolico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="153"/>
        <source>Automatic Backup</source>
        <translation>Backup automatico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="156"/>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="173"/>
        <source>Current Backup Location</source>
        <translation>Posizione attuale del backup</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="186"/>
        <source>Change Backup Location</source>
        <translation>Cambia la posizione del backup</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="213"/>
        <source>Backup Mode</source>
        <translation>Modalità di backup</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="223"/>
        <source>Daily</source>
        <translation>Giornaliera</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="232"/>
        <source>Weekly</source>
        <translation>Settimanale</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="241"/>
        <source>Monthly</source>
        <translation>Mensile</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="253"/>
        <source>Keep Copies</source>
        <translation>Conserva copie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="486"/>
        <location filename="../DialogSettings.ui" line="758"/>
        <source>Birthday</source>
        <translation>Compleanno</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1073"/>
        <source>Color</source>
        <translation>Colore</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1097"/>
        <location filename="../DialogSettings.ui" line="1125"/>
        <source>Size</source>
        <translation>Grandezza</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1179"/>
        <source>Lines</source>
        <translation>Linee</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1188"/>
        <location filename="../DialogSettings.ui" line="1216"/>
        <source>Width</source>
        <translation>Spessore</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1245"/>
        <source>Show Heartrate</source>
        <translation>Mostra Battito Cardiaco</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1251"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Mostra le Pulsazioni nella vista grafica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1261"/>
        <source>Print Heartrate</source>
        <translation>Stampa Battito Cardiaco</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1267"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Stampa le Pulsazioni su un foglio separato</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1981"/>
        <location filename="../DialogSettings.ui" line="2218"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Avviso Livello Pressione P.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2035"/>
        <location filename="../DialogSettings.ui" line="2272"/>
        <source>Heartrate Warnlevel</source>
        <translation>Avviso Livello Pulsazioni</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2337"/>
        <source>Statistic</source>
        <translation>Statistica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2343"/>
        <source>Bar Type</source>
        <translation>Barra Tipo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2349"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Mostra la mediana invece delle barre medie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2359"/>
        <source>Legend Type</source>
        <translation>Tipo di legenda</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2365"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Mostra i valori come legenda anziché come descrizioni</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2380"/>
        <source>E-Mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2388"/>
        <source>Address</source>
        <translation>Indirizzo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2407"/>
        <source>Subject</source>
        <translation>Argomento</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2425"/>
        <source>Message</source>
        <translation>Messaggio</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2446"/>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2452"/>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2458"/>
        <source>Log Device Communication to File</source>
        <translation>Registra la comunicazione del Dispositivo su File</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Import Measurements automatically</source>
        <translation>Importa Misurazioni automaticamente</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2475"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2481"/>
        <source>Discover Device automatically</source>
        <translation>Scopri il Dispositivo automaticamente</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2488"/>
        <source>Connect Device automatically</source>
        <translation>Connetti il Dispositivo automaticamente</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2503"/>
        <source>Update</source>
        <translation>Aggiornare</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2509"/>
        <source>Autostart</source>
        <translation>Avvio automatico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2515"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Verificare la presenza di aggiornamenti in linea all&apos;avvio del programma</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2528"/>
        <source>Notification</source>
        <translation>Notifica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2534"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Mostra sempre il risultato dopo il controllo dell&apos;aggiornamento online</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2550"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2567"/>
        <source>Reset</source>
        <translation>Ripristina</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2584"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Aggiornamento Online</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Versione disponibile</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Aggiorna Dimensione File</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Versione installata</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Dettagli</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="199"/>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="219"/>
        <source>Ignore</source>
        <translation>Ignorare</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="167"/>
        <source>No new version found.</source>
        <translation>Nessuna nuova versione trovata.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! AVVISO SSL - LEGGERE ATTENTAMENTE !!!

Problema di connessione di rete:

%1
Vuoi continuare comunque?</numerusform>
            <numerusform>!!! AVVISO SSL - LEGGERE ATTENTAMENTE !!!

Problemi di connessione di rete:

%1
Vuoi continuare comunque?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Downloading update failed!

%1</source>
        <translation>Scaricamento dell&apos;aggiornamento non riuscito!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Checking update failed!

%1</source>
        <translation>Controllo aggiornamento fallito!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="140"/>
        <source>Unexpected response from update server!</source>
        <translation>Risposta imprevista dal server di aggiornamento!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="157"/>
        <source>*%1 not found</source>
        <translation>*%1 non trovato</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="187"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>L&apos;aggiornamento non ha le dimensioni previste!

%L1 : %L2

Riprova a scaricare…</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="191"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Aggiornamento salvato in %1.

Avvia ora la nuova versione?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="204"/>
        <source>Could not start new version!</source>
        <translation>Impossibile avviare la nuova versione!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>Really abort download?</source>
        <translation>Interrompere davvero il download?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="211"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Impossibile salvare l&apos;aggiornamento in %1!

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gestore Universale Pressione Sanguigna</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Mostra Periodo Precedente</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Mostra il Prossimo Periodo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Vista Grafico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Vista Tabella</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Ora</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="126"/>
        <location filename="../MainWindow.cpp" line="4313"/>
        <source>Systolic</source>
        <translation>Sistolica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="127"/>
        <location filename="../MainWindow.cpp" line="4314"/>
        <source>Diastolic</source>
        <translation>Diastolica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pressione P.</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Irregolare</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Movimento</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Invisibile</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Commento</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Vista Statistica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>¼ Ora</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>½ Ora</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Oraria</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>6 Ore</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>12 Ore</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Giornaliera</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation>Settimanale</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation>Mensile</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation>Trimestrale</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation>Semestrale</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation>Annuale</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation>Ultimi 7 giorni</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation>Ultimi 14 giorni</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation>Ultimi 21 giorni</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation>Ultimi 28 giorni</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation>Ultimi 3 mesi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation>Ultimi 6 mesi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation>Ultimi 9 mesi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation>Ultimi 12 mesi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation>Tutti i Record</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation>Stampa</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation>Aiuto</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Configuration</source>
        <translation>Configurazione</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1161"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1170"/>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1179"/>
        <source>Style</source>
        <translation>Stile</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1188"/>
        <source>Charts</source>
        <translation>Grafici</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1213"/>
        <source>Database</source>
        <translation>Banca dati</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1217"/>
        <source>Import</source>
        <translation>Importa</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1229"/>
        <source>Export</source>
        <translation>Esporta</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1244"/>
        <source>Clear</source>
        <translation>Pulisci</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1331"/>
        <source>Quit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1334"/>
        <location filename="../MainWindow.ui" line="1337"/>
        <source>Quit Program</source>
        <translation>Esci dal programma</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1349"/>
        <source>About</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1352"/>
        <location filename="../MainWindow.ui" line="1355"/>
        <source>About Program</source>
        <translation>Informazioni sul programma</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1364"/>
        <source>Guide</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1367"/>
        <location filename="../MainWindow.ui" line="1370"/>
        <source>Show Guide</source>
        <translation>Mostra Guida</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Check Update</source>
        <translation>Controlla Aggiornamenti</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1544"/>
        <source>To PDF</source>
        <translation>In PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Export To PDF</source>
        <translation>Esporta in PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1559"/>
        <source>Migration</source>
        <translation>Migrazione</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Migrate from Vendor</source>
        <translation>Migra dal Fornitore</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1809"/>
        <source>PayPal</source>
        <translation>PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1812"/>
        <location filename="../MainWindow.ui" line="1815"/>
        <source>Donate via PayPal</source>
        <translation>Dona tramite PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1827"/>
        <location filename="../MainWindow.ui" line="1830"/>
        <source>Donate via Liberapay</source>
        <translation>Dona tramite Liberapay</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Donate via Amazon</source>
        <translation>Dona tramite Amazon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <location filename="../MainWindow.ui" line="1860"/>
        <source>Donate via SEPA</source>
        <translation>Dona tramite SEPA</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1869"/>
        <source>Translation</source>
        <translation>Traduzione</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <location filename="../MainWindow.ui" line="1875"/>
        <source>Contribute Translation</source>
        <translation>Contribuisci alla traduzione</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1884"/>
        <source>E-Mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1887"/>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Send E-Mail</source>
        <translation>Invia un&apos;e-mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1902"/>
        <source>Icons</source>
        <translation>Icone</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1905"/>
        <location filename="../MainWindow.ui" line="1908"/>
        <source>Change Icon Color</source>
        <translation>Cambia la colore dell&apos;icona</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1919"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1922"/>
        <location filename="../MainWindow.ui" line="1925"/>
        <source>System Colors</source>
        <translation>Colori del sistema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1933"/>
        <source>Light</source>
        <translation>Chiaro</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1936"/>
        <location filename="../MainWindow.ui" line="1939"/>
        <source>Light Colors</source>
        <translation>Colori chiari</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1947"/>
        <source>Dark</source>
        <translation>Scuro</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <location filename="../MainWindow.ui" line="1953"/>
        <source>Dark Colors</source>
        <translation>Colori scuri</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1132"/>
        <source>Donation</source>
        <translation>Donazione</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="128"/>
        <location filename="../MainWindow.cpp" line="4315"/>
        <source>Heartrate</source>
        <translation>Pulsazioni</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1129"/>
        <source>Make Donation</source>
        <translation>Fai una donazione</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>Bugreport</source>
        <translation>Riportare un errore</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>Send Bugreport</source>
        <translation>Invia segnalazione bug</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Change Settings</source>
        <translation>Cambia impostazioni</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>From Device</source>
        <translation>Dal Dispositivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1436"/>
        <location filename="../MainWindow.ui" line="1439"/>
        <source>Import From Device</source>
        <translation>Importa dal Dispositivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>From File</source>
        <translation>Dal File</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1454"/>
        <location filename="../MainWindow.ui" line="1457"/>
        <source>Import From File</source>
        <translation>Importa da File</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>From Input</source>
        <translation>Da immissione</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1469"/>
        <location filename="../MainWindow.ui" line="1472"/>
        <source>Import From Input</source>
        <translation>Importa da immissione</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>To CSV</source>
        <translation>In CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Export To CSV</source>
        <translation>Esporta in CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>To XML</source>
        <translation>In XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Export To XML</source>
        <translation>Esporta in XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>To JSON</source>
        <translation>In JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Export To JSON</source>
        <translation>Esporta in JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To SQL</source>
        <translation>In SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To SQL</source>
        <translation>Esporta in SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>Print Chart</source>
        <translation>Stampa Grafico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Print Chart View</source>
        <translation>Stampa vista grafico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>Print Table</source>
        <translation>Stampa Tabella</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Print Table View</source>
        <translation>Stampa vista tabella</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Print Statistic</source>
        <translation>Stampa Statistica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Print Statistic View</source>
        <translation>Stampa visualizzazione statistica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1622"/>
        <source>Preview Chart</source>
        <translation>Anteprima Grafico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <location filename="../MainWindow.ui" line="1628"/>
        <source>Preview Chart View</source>
        <translation>Anteprima vista grafico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1637"/>
        <source>Preview Table</source>
        <translation>Anteprima Tabella</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <location filename="../MainWindow.ui" line="1643"/>
        <source>Preview Table View</source>
        <translation>Anteprima vista tabella</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1652"/>
        <source>Preview Statistic</source>
        <translation>Anteprima Statistica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <location filename="../MainWindow.ui" line="1658"/>
        <source>Preview Statistic View</source>
        <translation>Anteprima vista statistica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1667"/>
        <location filename="../MainWindow.ui" line="1670"/>
        <location filename="../MainWindow.ui" line="1673"/>
        <source>Clear All</source>
        <translation>Cancella tutto</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Clear User 1</source>
        <translation>Cancella Utente 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Clear User 2</source>
        <translation>Cancella Utente 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1766"/>
        <source>Analysis</source>
        <translation>Analisi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1769"/>
        <location filename="../MainWindow.ui" line="1772"/>
        <source>Analyze Records</source>
        <translation>Analizza i Record</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1791"/>
        <location filename="../MainWindow.ui" line="1794"/>
        <location filename="../MainWindow.ui" line="1797"/>
        <source>Time Mode</source>
        <translation>Modalità Tempo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="76"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="4283"/>
        <location filename="../MainWindow.cpp" line="4284"/>
        <source>Records For Selected User</source>
        <translation>Record per l&apos;utente selezionato</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="84"/>
        <location filename="../MainWindow.cpp" line="85"/>
        <location filename="../MainWindow.cpp" line="4286"/>
        <location filename="../MainWindow.cpp" line="4287"/>
        <source>Select Date &amp; Time</source>
        <translation>Seleziona Data &amp; Ora</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="136"/>
        <location filename="../MainWindow.cpp" line="4317"/>
        <source>Systolic - Value Range</source>
        <translation>Sistolica - Intervallo di valori</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="137"/>
        <location filename="../MainWindow.cpp" line="4318"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastolica - Intervallo di valori</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="140"/>
        <location filename="../MainWindow.cpp" line="4320"/>
        <source>Systolic - Target Area</source>
        <translation>Sistolica - Area Obiettivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="141"/>
        <location filename="../MainWindow.cpp" line="4321"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastolica - Area Obiettivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <source>Athlete</source>
        <translation>Atleta</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1515"/>
        <source>Excellent</source>
        <translation>Eccellente</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1515"/>
        <source>Optimal</source>
        <translation>Ottimale</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1516"/>
        <source>Great</source>
        <translation>Ideale</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1517"/>
        <source>Good</source>
        <translation>Buona</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1516"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1727"/>
        <location filename="../MainWindow.ui" line="1730"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="4094"/>
        <location filename="../MainWindow.cpp" line="4095"/>
        <location filename="../MainWindow.cpp" line="4096"/>
        <location filename="../MainWindow.cpp" line="4097"/>
        <location filename="../MainWindow.cpp" line="4347"/>
        <location filename="../MainWindow.cpp" line="4348"/>
        <location filename="../MainWindow.cpp" line="4349"/>
        <location filename="../MainWindow.cpp" line="4350"/>
        <source>Switch To %1</source>
        <translation>Passa a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="488"/>
        <location filename="../MainWindow.cpp" line="4094"/>
        <location filename="../MainWindow.cpp" line="4095"/>
        <location filename="../MainWindow.cpp" line="4347"/>
        <location filename="../MainWindow.cpp" line="4348"/>
        <source>User 1</source>
        <translation>Utente 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="498"/>
        <location filename="../MainWindow.cpp" line="4096"/>
        <location filename="../MainWindow.cpp" line="4097"/>
        <location filename="../MainWindow.cpp" line="4349"/>
        <location filename="../MainWindow.cpp" line="4350"/>
        <source>User 2</source>
        <translation>Utente 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="138"/>
        <location filename="../MainWindow.cpp" line="4319"/>
        <source>Heartrate - Value Range</source>
        <translation>Pulsazioni (FC) - Intervallo di valori</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="142"/>
        <location filename="../MainWindow.cpp" line="4322"/>
        <source>Heartrate - Target Area</source>
        <translation>Pulsazioni (FC) - Area Obiettivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="354"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation>Impossibile creare la directory di backup.

Controlla l&apos;impostazione della posizione del backup.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="383"/>
        <location filename="../MainWindow.cpp" line="406"/>
        <source>Could not backup database:

%1</source>
        <translation>Impossibile eseguire il backup della base di dati:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="396"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation>Impossibile eliminare il backup obsoleto:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="423"/>
        <source>Could not register icons.</source>
        <translation>Impossibile registrare le icone.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="551"/>
        <source>Blood Pressure Report</source>
        <translation>Rapporto sulla Pressione Sanguigna</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1026"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : σ %1 / x̃ %4  |  DIA : σ %2 / x̃ %5  |  BPM : σ %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1031"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : σ 0 / x̃ 0  |  DIA : σ 0 / x̃ 0  |  BPM : σ 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <source>Low</source>
        <translation>Basso</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1517"/>
        <source>High Normal</source>
        <translation>Normale Alta</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1518"/>
        <location filename="../MainWindow.cpp" line="1985"/>
        <location filename="../MainWindow.cpp" line="4326"/>
        <location filename="../MainWindow.cpp" line="4330"/>
        <location filename="../MainWindow.cpp" line="4334"/>
        <location filename="../MainWindow.h" line="211"/>
        <location filename="../MainWindow.h" line="215"/>
        <location filename="../MainWindow.h" line="219"/>
        <source>Average</source>
        <translation>Medio</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1518"/>
        <source>Hyper 1</source>
        <translation>Iper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <source>Below Average</source>
        <translation>Sotto la Media</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <source>Hyper 2</source>
        <translation>Iper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <source>Poor</source>
        <translation>Bassa</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <source>Hyper 3</source>
        <translation>Iper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1665"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>Scansione importazione del plug-in &quot;%1&quot; non riuscita!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1678"/>
        <location filename="../MainWindow.cpp" line="1700"/>
        <location filename="../MainWindow.cpp" line="4293"/>
        <source>Switch Language to %1</source>
        <translation>Cambia lingua a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1725"/>
        <location filename="../MainWindow.cpp" line="1740"/>
        <location filename="../MainWindow.cpp" line="4301"/>
        <source>Switch Theme to %1</source>
        <translation>Cambia il tema a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1765"/>
        <location filename="../MainWindow.cpp" line="1785"/>
        <location filename="../MainWindow.cpp" line="4309"/>
        <source>Switch Style to %1</source>
        <translation>Cambia stile a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1856"/>
        <location filename="../MainWindow.cpp" line="1881"/>
        <source>Edit record</source>
        <translation>Modifica Record</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1985"/>
        <location filename="../MainWindow.cpp" line="4327"/>
        <location filename="../MainWindow.cpp" line="4331"/>
        <location filename="../MainWindow.cpp" line="4335"/>
        <location filename="../MainWindow.h" line="212"/>
        <location filename="../MainWindow.h" line="216"/>
        <location filename="../MainWindow.h" line="220"/>
        <source>Median</source>
        <translation>Mediano</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1987"/>
        <source>Click to swap Average and Median</source>
        <translation>Click per scambiare Medio e Mediano</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2082"/>
        <source>Click to swap Legend and Label</source>
        <translation>Click per scambiare legenda ed etichetta</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2159"/>
        <source>No records to preview for selected time range!</source>
        <translation>Nessun record da visualizzare in anteprima per l&apos;intervallo di tempo selezionato!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2191"/>
        <source>No records to print for selected time range!</source>
        <translation>Nessun record da stampare per l&apos;intervallo di tempo selezionato!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2237"/>
        <location filename="../MainWindow.cpp" line="2287"/>
        <location filename="../MainWindow.cpp" line="2363"/>
        <location filename="../MainWindow.cpp" line="2494"/>
        <source>User %1</source>
        <translation>Utente %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>DATE</source>
        <translation>DATA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>TIME</source>
        <translation>ORA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>IHB</source>
        <translation>IHB</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>COMMENT</source>
        <translation>COMMENTO</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>PPR</source>
        <translation>PPR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>MOV</source>
        <translation>MOV</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2559"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>Impossibile creare un&apos;e-mail perché la generazione base64 per l&apos;allegato «%1» non è riuscita!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4007"/>
        <source>Chart</source>
        <translation>Grafico</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4024"/>
        <source>Table</source>
        <translation>Tabella</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4041"/>
        <source>Statistic</source>
        <translation>Statistica</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4059"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>Impossibile aprire l&apos;e-mail «%1»!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4069"/>
        <source>Could not start e-mail client!</source>
        <translation>Impossibile avviare il client di posta elettronica!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4075"/>
        <source>Select Icon Color</source>
        <translation>Seleziona il colore dell&apos;icona</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4081"/>
        <source>Restart application to apply new icon color.</source>
        <translation>Riavvia l&apos;applicazione per applicare il nuovo colore dell&apos;icona.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4167"/>
        <source>Thanks to all translators:</source>
        <translation>Grazie a tutti i traduttori:</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4167"/>
        <source>Version</source>
        <translation>Versione</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4187"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation>Acquista un buono dell&apos;importo desiderato tramite il tuo account Amazon, seleziona e-mail a lazyt@mailbox.org come metodo di consegna e specifica UBPM come messaggio.

Grazie mille!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4204"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation>Inviami una richiesta di posta elettronica a lazyt@mailbox.org in modo che io possa fornirti le informazioni sul mio conto corrente bancario.

Grazie mille!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2231"/>
        <location filename="../MainWindow.cpp" line="2281"/>
        <location filename="../MainWindow.cpp" line="2357"/>
        <location filename="../MainWindow.cpp" line="2488"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Creato con UBPM per
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2232"/>
        <location filename="../MainWindow.cpp" line="2282"/>
        <location filename="../MainWindow.cpp" line="2358"/>
        <location filename="../MainWindow.cpp" line="2489"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Gratuito e Open Source
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2625"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importa da CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2625"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>File CSV (*.csv);;File XML (*.xml);;File JSON (*.json);;File SQL (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2681"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossibile aprire &quot;%1&quot;!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1025"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Misure : %1 |  Irregolare : %2 |  Movimento: %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="552"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Egr. dr. Medico di famiglia,

trova in allegato i dati della mia pressione sanguigna per questo mese.

Distinti saluti,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1030"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Misure : 0 |  Irregolare : 0 |  Movimento: 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2237"/>
        <location filename="../MainWindow.cpp" line="2287"/>
        <location filename="../MainWindow.cpp" line="2363"/>
        <location filename="../MainWindow.cpp" line="2494"/>
        <source>%1 (Age: %2, Height: %3cm, Weight: %4Kg, BMI: %5)</source>
        <translation>%1 (Età: %2, Altezza: %3cm, Peso: %4Kg, BMI: %5)</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2663"/>
        <location filename="../MainWindow.cpp" line="3896"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>%n record importato con successo da %1.

     User 1 : %2
     User 2 : %3</numerusform>
            <numerusform>%n record importati con successo da %1.

     User 1 : %2
     User 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2667"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n record non valido ignorato!</numerusform>
            <numerusform>%n record non validi ignorati!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2672"/>
        <location filename="../MainWindow.cpp" line="3900"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n record duplicato saltato!</numerusform>
            <numerusform>%n record duplicati saltati!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3051"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Non sembra un database UBPM!

Forse impostazioni/password di crittografia errate?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3104"/>
        <location filename="../MainWindow.cpp" line="3108"/>
        <source>Export to %1</source>
        <translation>Esporta in %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3104"/>
        <location filename="../MainWindow.cpp" line="3108"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 File (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3144"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossibile creare &quot;%1&quot;!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3150"/>
        <source>The database is empty, no records to export!</source>
        <translation>Il database è vuoto, nessun record da esportare!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3501"/>
        <source>Morning</source>
        <translation>Mattina</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3501"/>
        <source>Afternoon</source>
        <translation>Pomeriggio</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3540"/>
        <source>Week</source>
        <translation>Settimana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3595"/>
        <source>Quarter</source>
        <translation>Trimestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3624"/>
        <source>Half Year</source>
        <translation>Metà Anno</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3642"/>
        <source>Year</source>
        <translation>Anno</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3830"/>
        <source>Really delete all records for user %1?</source>
        <translation>Eliminare davvero tutti i record per l&apos;utente %1?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3845"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Tutti i record per l&apos;utente %1 sono stati eliminati e il database esistente è stato salvato in &quot;ubpm.sql.bak&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3953"/>
        <source>Really delete all records?</source>
        <translation>Cancellare davvero tutti i record?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3964"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Tutti i record sono stati eliminati e il database esistente &quot;ubpm.sql&quot; è stato spostato nel cestino.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4324"/>
        <location filename="../MainWindow.cpp" line="4328"/>
        <location filename="../MainWindow.cpp" line="4332"/>
        <location filename="../MainWindow.h" line="209"/>
        <location filename="../MainWindow.h" line="213"/>
        <location filename="../MainWindow.h" line="217"/>
        <source>Minimum</source>
        <translation>Minimo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4325"/>
        <location filename="../MainWindow.cpp" line="4329"/>
        <location filename="../MainWindow.cpp" line="4333"/>
        <location filename="../MainWindow.h" line="210"/>
        <location filename="../MainWindow.h" line="214"/>
        <location filename="../MainWindow.h" line="218"/>
        <source>Maximum</source>
        <translation>Massimo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4398"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Impossibile aprire il file del tema &quot;%1&quot;!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1853"/>
        <location filename="../MainWindow.cpp" line="1870"/>
        <location filename="../MainWindow.cpp" line="5230"/>
        <source>Delete record</source>
        <translation>Elimina Record</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4251"/>
        <source>Could not load application translation for &quot;%1&quot;.</source>
        <translation>Impossibile caricare la traduzione dell&apos;applicazione per &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4260"/>
        <source>Could not load plugin translation for &quot;%1&quot;.</source>
        <translation>Impossibile caricare la traduzione del plug-in per &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4269"/>
        <source>Could not load base translation for &quot;%1&quot;.

Internal base translations (like &quot;Yes/No&quot;) are not available.

Don&apos;t show this warning again?</source>
        <translation>Impossibile caricare la traduzione di base per &quot;%1&quot;.

Le traduzioni di base interne (come &quot;Sì/No&quot;) non sono disponibili.

Non mostrare più questo avviso?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5157"/>
        <source>Show Heartrate</source>
        <translation>Mostra Battito Cardiaco</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5193"/>
        <location filename="../MainWindow.cpp" line="5204"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>Simboli e linee non possono essere disabilitati entrambi!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5232"/>
        <source>Show record</source>
        <translation>Mostra Record</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1854"/>
        <location filename="../MainWindow.cpp" line="1877"/>
        <location filename="../MainWindow.cpp" line="5233"/>
        <source>Hide record</source>
        <translation>Nascondi Record</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1872"/>
        <location filename="../MainWindow.cpp" line="5244"/>
        <source>Really delete selected record?</source>
        <translation>Eliminare davvero il record selezionato?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5149"/>
        <source>Dynamic Scaling</source>
        <translation>Ridimensionamento Dinamico</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5150"/>
        <source>Colored Stripes</source>
        <translation>Fasce Soglie Colorate</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5152"/>
        <source>Show Symbols</source>
        <translation>Mostra Simboli</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5153"/>
        <source>Show Lines</source>
        <translation>Mostra Linee</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5155"/>
        <source>Colored Symbols</source>
        <translation>Simboli Colorati</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5265"/>
        <source>Show Median</source>
        <translation>Mostra Mediani</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5266"/>
        <source>Show Values</source>
        <translation>Mostra Valori</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5317"/>
        <source>Really quit program?</source>
        <translation>Vuoi uscire dal programma?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gestore Universale Pressione Sanguigna</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation>L&apos;applicazione è già in esecuzione e non sono consentite istanze multiple.

Passa all&apos;istanza in esecuzione o chiudila e riprova.</translation>
    </message>
</context>
</TS>
