<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Анализ данных</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Запрос</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="206"/>
        <source>Results</source>
        <translation>Полученные результаты</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation>Систолическое</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation>Диастолическое</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation>P.Давление</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation>Пульс</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation>Нерегулярный</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Movement</source>
        <translation>Движение</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Invisible</source>
        <translation>Невидимый</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="169"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="199"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="48"/>
        <source>Could not create memory database!

%1</source>
        <translation>Не удалось создать базу данных памяти!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="208"/>
        <source>No results for this query found!</source>
        <translation>По этому запросу ничего не найдено!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 из %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Результат</numerusform>
            <numerusform>Результата</numerusform>
            <numerusform>Результатов</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>User %1</source>
        <translation>Пользователь %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Совпадение</numerusform>
            <numerusform>Совпадения</numerusform>
            <numerusform>Совпадений</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Запись</numerusform>
            <numerusform>Записи</numerusform>
            <numerusform>Записей</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Гид</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="60"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>Руководство %1 не найдено, вместо него отображается руководство на английском языке.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="52"/>
        <source>%1 guide not found!</source>
        <translation>%1 руководство не найдено!</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Перенос данных</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Источник</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation>Открыть источнык данных</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Формат даты</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Элементов на строку</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Разделитель</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Формат времени</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation>Язык данных</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation>Систолическое</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation>Пульс</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation>Движение</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation>Диастолическое</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Результат</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="783"/>
        <source>Migrate User 1</source>
        <translation>Перенос пользователя 1</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="812"/>
        <source>Migrate User 2</source>
        <translation>Перенос пользователя 2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="838"/>
        <source>Copy to Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="858"/>
        <source>Select predefined settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="892"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="903"/>
        <source>Test Data Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="917"/>
        <source>Start Data Migration</source>
        <translation>Начать перенос данных</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="250"/>
        <source>Choose Data Source for Migration</source>
        <translation>Выберите источник данных для переноса</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="250"/>
        <source>CSV File (*.csv)</source>
        <translation>Файл CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="303"/>
        <source>%L1 Bytes</source>
        <translation>%L1 байт</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%1 Lines</source>
        <translation>%1 строк</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="318"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Не удалось открыть &quot;%1&quot;!

Причина: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="340"/>
        <source>Startline can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="346"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="354"/>
        <source>Line %1 = %2</source>
        <translation>Строка %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="359"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>Надо указать разделитель</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="366"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="377"/>
        <source>Elements can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="383"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="389"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="396"/>
        <source>Date format can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="402"/>
        <source>Single date format must also contain a time format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="409"/>
        <source>Date position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="415"/>
        <source>Date position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="422"/>
        <source>Time position requires also a time parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="428"/>
        <source>Time format requires also a time position.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="434"/>
        <source>Time position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="441"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="447"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="454"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="460"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="467"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="473"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="480"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="486"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="493"/>
        <source>Movement position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="499"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="506"/>
        <source>Comment position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="522"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="538"/>
        <source>Date format is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="545"/>
        <source>Time format is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="555"/>
        <source>Date/Time format is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="745"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="749"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation type="unfinished">
            <numerusform>Пропущена %n неправильная запись!</numerusform>
            <numerusform>Пропущено %n неправильных записей!</numerusform>
            <numerusform>Пропущено %n неправильных записей!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="754"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation type="unfinished">
            <numerusform>Пропущен %n дубликат записи!</numerusform>
            <numerusform>Пропущено %n дубликатов записей!</numerusform>
            <numerusform>Пропущено %n дубликатов записей!</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Ручная запись</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Запись данных</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Добавить запись для %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Выберите дату и время</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Введите SYS</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>Введите DIA</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Введите BPM</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Аритмия</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>движение</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Введите необязательный комментарий</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Показать сообщение об успешном создании / удалении / изменении записи</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Создавать</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Закрывать</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Пользователь 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Пользователь 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="33"/>
        <location filename="../DialogRecord.cpp" line="186"/>
        <source>Modify</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="131"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Запись данных не может быть удалена! 

Запись для этой даты и времени не существует.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="137"/>
        <source>Data record successfully deleted.</source>
        <translation>Запись данных успешно удалена.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="149"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Пожалуйста, сначала введите допустимое значение для &quot;SYS&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="159"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Пожалуйста, сначала введите допустимое значение для &quot;DIA&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="168"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Пожалуйста, сначала введите допустимое значение для &quot;BPM&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="190"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Запись данных не может быть изменена! 

Запись для этой даты и времени не существует.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="196"/>
        <source>Data Record successfully modified.</source>
        <translation>Запись данных успешно изменена.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="205"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Запись данных не может быть создана! 

Запись для этой даты и времени уже существует.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="211"/>
        <source>Data Record successfully created.</source>
        <translation>Запись данных успешно создана.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>База данных</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="52"/>
        <source>Location</source>
        <translation>Место расположения</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="58"/>
        <source>Current Location</source>
        <translation>Текущее местоположение</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="74"/>
        <source>Change Location</source>
        <translation>Изменить местоположение</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="100"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Шифровать с помощью SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="103"/>
        <source>Encryption</source>
        <translation>Шифрование</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="115"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="128"/>
        <source>Show Password</source>
        <translation>Показать пароль</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="282"/>
        <source>User</source>
        <translation>Пользователь</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <location filename="../DialogSettings.ui" line="574"/>
        <source>Mandatory Information</source>
        <translation>Обязательная информация</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="317"/>
        <location filename="../DialogSettings.ui" line="580"/>
        <source>Male</source>
        <translation>Мужской</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="346"/>
        <location filename="../DialogSettings.ui" line="609"/>
        <source>Female</source>
        <translation>женский</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="372"/>
        <location filename="../DialogSettings.ui" line="644"/>
        <source>Age Group</source>
        <translation>Возрастная группа</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="442"/>
        <location filename="../DialogSettings.ui" line="714"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="458"/>
        <location filename="../DialogSettings.ui" line="730"/>
        <source>Additional Information</source>
        <translation>Дополнительная информация</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Height</source>
        <translation>Высота</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="547"/>
        <location filename="../DialogSettings.ui" line="819"/>
        <source>Weight</source>
        <translation>Масса</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="844"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="850"/>
        <location filename="../DialogSettings.cpp" line="62"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Импортировать плагины [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="865"/>
        <source>Please choose Device Plugin…</source>
        <translation>Пожалуйста, выберите Плагин устройства…</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="880"/>
        <source>Show Device Image</source>
        <translation>Показать изображение устройства</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="903"/>
        <source>Show Device Manual</source>
        <translation>Показать руководство к устройству</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="948"/>
        <source>Open Website</source>
        <translation>Открыть веб-сайт</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="961"/>
        <source>Maintainer</source>
        <translation>Поддержка</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="989"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="996"/>
        <source>Send E-Mail</source>
        <translation>Отправить электронное письмо</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1009"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1016"/>
        <source>Producer</source>
        <translation>Производитель</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1033"/>
        <source>Chart</source>
        <translation>Диаграмма</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1073"/>
        <source>Color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1160"/>
        <source>X-Axis Range</source>
        <translation>Диапазон оси X</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1166"/>
        <source>Dynamic Scaling</source>
        <translation>Динамическое масштабирование</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1141"/>
        <source>Colored Areas</source>
        <translation>Цветные области</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1147"/>
        <source>Healthy Ranges</source>
        <translation>Здоровые диапазоны</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1064"/>
        <source>Symbols</source>
        <translation>Символы</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1288"/>
        <location filename="../DialogSettings.ui" line="1563"/>
        <source>Systolic</source>
        <translation>Систолическое</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1376"/>
        <location filename="../DialogSettings.ui" line="1651"/>
        <source>Diastolic</source>
        <translation>Диастолическое</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1464"/>
        <location filename="../DialogSettings.ui" line="1739"/>
        <source>Heartrate</source>
        <translation>Частота сердцебиения</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1836"/>
        <source>Table</source>
        <translation>Стол</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1921"/>
        <location filename="../DialogSettings.ui" line="2104"/>
        <source>Systolic Warnlevel</source>
        <translation>Уровень систолического предупреждения</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1867"/>
        <location filename="../DialogSettings.ui" line="2164"/>
        <source>Diastolic Warnlevel</source>
        <translation>Уровень диастолического предупреждения</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="153"/>
        <source>Automatic Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="156"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="173"/>
        <source>Current Backup Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="186"/>
        <source>Change Backup Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="213"/>
        <source>Backup Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="223"/>
        <source>Daily</source>
        <translation type="unfinished">День</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="232"/>
        <source>Weekly</source>
        <translation type="unfinished">Неделя</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="241"/>
        <source>Monthly</source>
        <translation type="unfinished">Месяц</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="253"/>
        <source>Keep Copies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="486"/>
        <location filename="../DialogSettings.ui" line="758"/>
        <source>Birthday</source>
        <translation>День Рождения</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1097"/>
        <location filename="../DialogSettings.ui" line="1125"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1179"/>
        <source>Lines</source>
        <translation>Линии</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1188"/>
        <location filename="../DialogSettings.ui" line="1216"/>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1245"/>
        <source>Show Heartrate</source>
        <translation>Показать Ч.С.С.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1251"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Показать Ч.С.С. на Графике</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1261"/>
        <source>Print Heartrate</source>
        <translation>Распечатать Ч.С.С.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1267"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Распечатать Ч.С.С. на другой странице</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1981"/>
        <location filename="../DialogSettings.ui" line="2218"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Уровень предупреждения о давлении</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2035"/>
        <location filename="../DialogSettings.ui" line="2272"/>
        <source>Heartrate Warnlevel</source>
        <translation>Уровень предупреждения сердечного ритма</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2337"/>
        <source>Statistic</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2343"/>
        <source>Bar Type</source>
        <translation>Тип бара</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2349"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Показывать медиану вместо столбцов средних значений</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2359"/>
        <source>Legend Type</source>
        <translation>Тип легенды</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2365"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Показывать значения в виде легенды вместо описаний</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2380"/>
        <source>E-Mail</source>
        <translation>Эл.Почта</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2388"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2407"/>
        <source>Subject</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2425"/>
        <source>Message</source>
        <translation>Сообщение</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2446"/>
        <source>Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2452"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2458"/>
        <source>Log Device Communication to File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Import Measurements automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2475"/>
        <source>Bluetooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2481"/>
        <source>Discover Device automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2488"/>
        <source>Connect Device automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2503"/>
        <source>Update</source>
        <translation>Обновлять</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2509"/>
        <source>Autostart</source>
        <translation>Автоматический старт</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2515"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Проверяйте наличие обновлений в Интернете при запуске программы</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2528"/>
        <source>Notification</source>
        <translation>Уведомление</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2534"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Всегда показывать результат после онлайн-проверки обновлений</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2550"/>
        <source>Save</source>
        <translation>Сохранять</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2567"/>
        <source>Reset</source>
        <translation>Перезагрузить</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2584"/>
        <source>Close</source>
        <translation>Закрывать</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="159"/>
        <source>Choose Database Location</source>
        <translation>Выберите расположение базы данных</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="169"/>
        <source>Choose Database Backup Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="205"/>
        <source>Could not display manual!

%1</source>
        <translation>Не удалось отобразить руководство! 

%1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="328"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="330"/>
        <source>Overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="331"/>
        <source>Merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="332"/>
        <source>Swap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="334"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="342"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="372"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>Шифрование SQL невозможно включить без пароля, оно будет отключено!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="379"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="384"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Пожалуйста, введите действительные значения для дополнительной информации о пользователе 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="391"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Пожалуйста, введите действительные значения для дополнительной информации о пользователе 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="398"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>Указанный возраст не соответствует возрастной группе пользователя 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="405"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>Указанный возраст не соответствует возрастной группе пользователя 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="413"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Пожалуйста, включите символы или линии для графика!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="420"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Пожалуйста, введите правильный адрес эл. почты!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="427"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Пожалуйста, введите тему сообщения!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="434"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>Сообшение должно содержать $CHART, $TABLE и/или $STATS!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="545"/>
        <source>User 1</source>
        <translation>Пользователь 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="559"/>
        <source>User 2</source>
        <translation>Пользователь 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="672"/>
        <source>Blood Pressure Report</source>
        <translation>Отчет об Артериальном Давлении</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="673"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Уважаемый Доктор,

Посылаю Вам отчет о моем артериальном давлении за текущий месяц.

С уважением,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="715"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Прервать установку и отменить все изменения?</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Онлайн-обновление</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Доступная версия</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Обновить размер файла</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Установленная версия</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Подробности</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="199"/>
        <source>Download</source>
        <translation>Скачать</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="219"/>
        <source>Ignore</source>
        <translation>Игнорировать</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! ПРЕДУПРЕЖДЕНИЕ SSL - ПРОЧИТАЙТЕ ВНИМАТЕЛЬНО !!!

Проблема сетевого подключения:

%1
Вы все равно хотите продолжить?</numerusform>
            <numerusform>!!! ПРЕДУПРЕЖДЕНИЕ SSL - ПРОЧИТАЙТЕ ВНИМАТЕЛЬНО !!!

Проблемы сетевого подключения:

%1
Вы все равно хотите продолжить?</numerusform>
            <numerusform>!!! ПРЕДУПРЕЖДЕНИЕ SSL - ПРОЧИТАЙТЕ ВНИМАТЕЛЬНО !!!

Проблемы сетевого подключения:

%1
Вы все равно хотите продолжить?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Downloading update failed!

%1</source>
        <translation>Не удалось загрузить обновление! 

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Checking update failed!

%1</source>
        <translation>Ошибка проверки обновления! 

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="140"/>
        <source>Unexpected response from update server!</source>
        <translation>Неожиданный ответ от сервера обновлений!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="157"/>
        <source>*%1 not found</source>
        <translation>*%1 не найдено</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="167"/>
        <source>No new version found.</source>
        <translation>Новых версий не найдено.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="187"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>У обновления не ожидаемый размер! 

%L1:%L2 

Повторите загрузку …</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="191"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Обновление сохранено в %1. 

Начать новую версию сейчас?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="204"/>
        <source>Could not start new version!</source>
        <translation>Не удалось запустить новую версию!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="211"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Не удалось сохранить обновление в %1! 

%2</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>Really abort download?</source>
        <translation>Действительно прервать загрузку?</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Универсальный менеджер артериального давления</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Показать Предыдущий Период</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Показать Следующий Период</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>График</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Таблица</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="126"/>
        <location filename="../MainWindow.cpp" line="4313"/>
        <source>Systolic</source>
        <translation>Систолическое</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="127"/>
        <location filename="../MainWindow.cpp" line="4314"/>
        <source>Diastolic</source>
        <translation>Диастолическое</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Арт.Давление</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="128"/>
        <location filename="../MainWindow.cpp" line="4315"/>
        <source>Heartrate</source>
        <translation>Частота сердцебиения</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Нерегулярное</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>движение</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Не Показывать</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>¼ Часа</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>½ Часа</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Почасовой</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>6 Часов</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>12 Часов</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>День</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation>Неделя</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation>Месяц</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation>Квартал</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation>6 Месяцев</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation>Год</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation>Последние 7 дней</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation>Последние 14 дней</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation>Последние 21 день</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation>Последние 28 дней</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation>Последние 3 месяца</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation>Последние 6 месяцев</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation>Последние 9 месяцев</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation>Последние 12 месяцев</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation>Все записи</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation>Распечатать</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1129"/>
        <source>Make Donation</source>
        <translation>Сделать пожертвование</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1132"/>
        <source>Donation</source>
        <translation>Пожертвование</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Configuration</source>
        <translation>Конфигурация</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1161"/>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1170"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1179"/>
        <source>Style</source>
        <translation>Стиль</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1188"/>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1213"/>
        <source>Database</source>
        <translation>База данных</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1217"/>
        <source>Import</source>
        <translation>Импорт</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1229"/>
        <source>Export</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1244"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1331"/>
        <source>Quit</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1334"/>
        <location filename="../MainWindow.ui" line="1337"/>
        <source>Quit Program</source>
        <translation>Выйти из программы</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1349"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1352"/>
        <location filename="../MainWindow.ui" line="1355"/>
        <source>About Program</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1364"/>
        <source>Guide</source>
        <translation>Гид</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1367"/>
        <location filename="../MainWindow.ui" line="1370"/>
        <source>Show Guide</source>
        <translation>Показать руководство</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Update</source>
        <translation>Обновлять</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Check Update</source>
        <translation>Проверить обновление</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>Bugreport</source>
        <translation>Отчет об ошибке</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>Send Bugreport</source>
        <translation>Отправить отчет об ошибке</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Change Settings</source>
        <translation>Изменить настройки</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>From Device</source>
        <translation>С устройства</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1436"/>
        <location filename="../MainWindow.ui" line="1439"/>
        <source>Import From Device</source>
        <translation>Импорт с устройства</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>From File</source>
        <translation>Из файла</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1454"/>
        <location filename="../MainWindow.ui" line="1457"/>
        <source>Import From File</source>
        <translation>Импортировать из файла</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>From Input</source>
        <translation>Ручной Ввод</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1469"/>
        <location filename="../MainWindow.ui" line="1472"/>
        <source>Import From Input</source>
        <translation>Ручной Ввод</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>To CSV</source>
        <translation>В CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Export To CSV</source>
        <translation>Экспорт в CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>To XML</source>
        <translation>В XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Export To XML</source>
        <translation>Экспорт в XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>To JSON</source>
        <translation>В JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Export To JSON</source>
        <translation>Экспорт в JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To SQL</source>
        <translation>В SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To SQL</source>
        <translation>Экспорт в SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1544"/>
        <source>To PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Export To PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1559"/>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Migrate from Vendor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>Print Chart</source>
        <translation>Распечатать диаграмму</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Print Chart View</source>
        <translation>Распечатать диаграмму</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>Print Table</source>
        <translation>Распечатать таблицу</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Print Table View</source>
        <translation>Печать в виде таблицы</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Print Statistic</source>
        <translation>Статистика печати</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Print Statistic View</source>
        <translation>Просмотр статистики печати</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1622"/>
        <source>Preview Chart</source>
        <translation>Предварительный просмотр диаграммы</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <location filename="../MainWindow.ui" line="1628"/>
        <source>Preview Chart View</source>
        <translation>Предварительный просмотр диаграммы</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1637"/>
        <source>Preview Table</source>
        <translation>Таблица предварительного просмотра</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <location filename="../MainWindow.ui" line="1643"/>
        <source>Preview Table View</source>
        <translation>Предварительный просмотр табличного представления</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1652"/>
        <source>Preview Statistic</source>
        <translation>Предварительный просмотр статистики</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <location filename="../MainWindow.ui" line="1658"/>
        <source>Preview Statistic View</source>
        <translation>Предварительный просмотр статистики</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1667"/>
        <location filename="../MainWindow.ui" line="1670"/>
        <location filename="../MainWindow.ui" line="1673"/>
        <source>Clear All</source>
        <translation>Очистить все</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Clear User 1</source>
        <translation>Очистить пользователя 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Clear User 2</source>
        <translation>Очистить пользователя 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1727"/>
        <location filename="../MainWindow.ui" line="1730"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="4094"/>
        <location filename="../MainWindow.cpp" line="4095"/>
        <location filename="../MainWindow.cpp" line="4096"/>
        <location filename="../MainWindow.cpp" line="4097"/>
        <location filename="../MainWindow.cpp" line="4347"/>
        <location filename="../MainWindow.cpp" line="4348"/>
        <location filename="../MainWindow.cpp" line="4349"/>
        <location filename="../MainWindow.cpp" line="4350"/>
        <source>Switch To %1</source>
        <translation>Перейти на %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1766"/>
        <source>Analysis</source>
        <translation>Анализ</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1769"/>
        <location filename="../MainWindow.ui" line="1772"/>
        <source>Analyze Records</source>
        <translation>Анализировать записи</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1791"/>
        <location filename="../MainWindow.ui" line="1794"/>
        <location filename="../MainWindow.ui" line="1797"/>
        <source>Time Mode</source>
        <translation>Временной режим</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1809"/>
        <source>PayPal</source>
        <translation>PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1812"/>
        <location filename="../MainWindow.ui" line="1815"/>
        <source>Donate via PayPal</source>
        <translation>Пожертвовать через PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1827"/>
        <location filename="../MainWindow.ui" line="1830"/>
        <source>Donate via Liberapay</source>
        <translation>Пожертвовать через Liberapay</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Donate via Amazon</source>
        <translation>Пожертвовать через Amazon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <location filename="../MainWindow.ui" line="1860"/>
        <source>Donate via SEPA</source>
        <translation>Пожертвовать через SEPA</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1869"/>
        <source>Translation</source>
        <translation>Перевод</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <location filename="../MainWindow.ui" line="1875"/>
        <source>Contribute Translation</source>
        <translation>Добавить Перевод</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1884"/>
        <source>E-Mail</source>
        <translation>Эл.Почта</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1887"/>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Send E-Mail</source>
        <translation>Отправить эл. письмо</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1902"/>
        <source>Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1905"/>
        <location filename="../MainWindow.ui" line="1908"/>
        <source>Change Icon Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1919"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1922"/>
        <location filename="../MainWindow.ui" line="1925"/>
        <source>System Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1933"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1936"/>
        <location filename="../MainWindow.ui" line="1939"/>
        <source>Light Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1947"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <location filename="../MainWindow.ui" line="1953"/>
        <source>Dark Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="488"/>
        <location filename="../MainWindow.cpp" line="4094"/>
        <location filename="../MainWindow.cpp" line="4095"/>
        <location filename="../MainWindow.cpp" line="4347"/>
        <location filename="../MainWindow.cpp" line="4348"/>
        <source>User 1</source>
        <translation>Пользователь 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="498"/>
        <location filename="../MainWindow.cpp" line="4096"/>
        <location filename="../MainWindow.cpp" line="4097"/>
        <location filename="../MainWindow.cpp" line="4349"/>
        <location filename="../MainWindow.cpp" line="4350"/>
        <source>User 2</source>
        <translation>Пользователь 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="76"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="4283"/>
        <location filename="../MainWindow.cpp" line="4284"/>
        <source>Records For Selected User</source>
        <translation>Записи для выбранного пользователя</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="84"/>
        <location filename="../MainWindow.cpp" line="85"/>
        <location filename="../MainWindow.cpp" line="4286"/>
        <location filename="../MainWindow.cpp" line="4287"/>
        <source>Select Date &amp; Time</source>
        <translation>Выберите дату и время</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="136"/>
        <location filename="../MainWindow.cpp" line="4317"/>
        <source>Systolic - Value Range</source>
        <translation>Систолическое - диапазон значений</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="137"/>
        <location filename="../MainWindow.cpp" line="4318"/>
        <source>Diastolic - Value Range</source>
        <translation>Диастолическое - диапазон значений</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="138"/>
        <location filename="../MainWindow.cpp" line="4319"/>
        <source>Heartrate - Value Range</source>
        <translation>Пульс - диапазон значений</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="140"/>
        <location filename="../MainWindow.cpp" line="4320"/>
        <source>Systolic - Target Area</source>
        <translation>Систолическое - Целевая область</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="141"/>
        <location filename="../MainWindow.cpp" line="4321"/>
        <source>Diastolic - Target Area</source>
        <translation>Диастолическое - Целевая область</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="142"/>
        <location filename="../MainWindow.cpp" line="4322"/>
        <source>Heartrate - Target Area</source>
        <translation>Частота пульса - Целевая область</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="354"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="383"/>
        <location filename="../MainWindow.cpp" line="406"/>
        <source>Could not backup database:

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="396"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="423"/>
        <source>Could not register icons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="551"/>
        <source>Blood Pressure Report</source>
        <translation>Отчет об Артериальном Давлении</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1026"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>СИС: Ø%1 / x̃%4 | ДИА: Ø%2 / x̃%5 | ПУЛЬС: Ø%3 / x̃%6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1031"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>СИС: Ø 0 / x̃ 0 | ДИА: Ø 0 / x̃ 0 | ПУЛЬС: Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <source>Athlete</source>
        <translation>Спортсмен</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1515"/>
        <source>Excellent</source>
        <translation>Отлично</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1515"/>
        <source>Optimal</source>
        <translation>Оптимально</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1516"/>
        <source>Great</source>
        <translation>Большой</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1516"/>
        <source>Normal</source>
        <translation>Нормальный</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1517"/>
        <source>Good</source>
        <translation>Хороший</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1517"/>
        <source>High Normal</source>
        <translation>Высокий Нормальный</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1518"/>
        <location filename="../MainWindow.cpp" line="1985"/>
        <location filename="../MainWindow.cpp" line="4326"/>
        <location filename="../MainWindow.cpp" line="4330"/>
        <location filename="../MainWindow.cpp" line="4334"/>
        <location filename="../MainWindow.h" line="211"/>
        <location filename="../MainWindow.h" line="215"/>
        <location filename="../MainWindow.h" line="219"/>
        <source>Average</source>
        <translation>Средний</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1518"/>
        <source>Hyper 1</source>
        <translation>Гипер 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <source>Below Average</source>
        <translation>Ниже среднего</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <source>Hyper 2</source>
        <translation>Гипер 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <source>Poor</source>
        <translation>Бедные</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <source>Hyper 3</source>
        <translation>Гипер 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1665"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>Не удалось сканировать плагин импорта &quot;%1&quot;! 

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1678"/>
        <location filename="../MainWindow.cpp" line="1700"/>
        <location filename="../MainWindow.cpp" line="4293"/>
        <source>Switch Language to %1</source>
        <translation>Переключить язык на %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1725"/>
        <location filename="../MainWindow.cpp" line="1740"/>
        <location filename="../MainWindow.cpp" line="4301"/>
        <source>Switch Theme to %1</source>
        <translation>Переключить тему на %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1765"/>
        <location filename="../MainWindow.cpp" line="1785"/>
        <location filename="../MainWindow.cpp" line="4309"/>
        <source>Switch Style to %1</source>
        <translation>Переключить стиль на %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1853"/>
        <location filename="../MainWindow.cpp" line="1870"/>
        <location filename="../MainWindow.cpp" line="5230"/>
        <source>Delete record</source>
        <translation>Удалить запись</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1854"/>
        <location filename="../MainWindow.cpp" line="1877"/>
        <location filename="../MainWindow.cpp" line="5233"/>
        <source>Hide record</source>
        <translation>Скрыть запись</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1856"/>
        <location filename="../MainWindow.cpp" line="1881"/>
        <source>Edit record</source>
        <translation>Редактировать запись</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1872"/>
        <location filename="../MainWindow.cpp" line="5244"/>
        <source>Really delete selected record?</source>
        <translation>Действительно удалить выбранную запись?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1985"/>
        <location filename="../MainWindow.cpp" line="4327"/>
        <location filename="../MainWindow.cpp" line="4331"/>
        <location filename="../MainWindow.cpp" line="4335"/>
        <location filename="../MainWindow.h" line="212"/>
        <location filename="../MainWindow.h" line="216"/>
        <location filename="../MainWindow.h" line="220"/>
        <source>Median</source>
        <translation>Медиана</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1987"/>
        <source>Click to swap Average and Median</source>
        <translation>Нажмите, чтобы поменять местами Среднее и Медианное</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2082"/>
        <source>Click to swap Legend and Label</source>
        <translation>Нажмите, чтобы поменять местами легенду и метку</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2159"/>
        <source>No records to preview for selected time range!</source>
        <translation>Нет записей для предварительного просмотра за выбранный временной диапазон!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2191"/>
        <source>No records to print for selected time range!</source>
        <translation>Нет записей для печати за выбранный временной диапазон!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2231"/>
        <location filename="../MainWindow.cpp" line="2281"/>
        <location filename="../MainWindow.cpp" line="2357"/>
        <location filename="../MainWindow.cpp" line="2488"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Создано с помощью UBPM для 
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2232"/>
        <location filename="../MainWindow.cpp" line="2282"/>
        <location filename="../MainWindow.cpp" line="2358"/>
        <location filename="../MainWindow.cpp" line="2489"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Бесплатно и с открытым исходным кодом 
https: //codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2237"/>
        <location filename="../MainWindow.cpp" line="2287"/>
        <location filename="../MainWindow.cpp" line="2363"/>
        <location filename="../MainWindow.cpp" line="2494"/>
        <source>User %1</source>
        <translation>Пользователь %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>DATE</source>
        <translation>ДАТА</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>TIME</source>
        <translation>ВРЕМЯ</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>PPR</source>
        <translation>PPR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>IHB</source>
        <translation>IHB</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>COMMENT</source>
        <translation>КОММЕНТАРИЙ</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>MOV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2559"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>Не удалось создать сообщение электронной почты, так как не удалось создать base64 вложение &quot;%1&quot;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2625"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Импорт из CSV / XML / JSON / SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2625"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>Файл CSV (* .csv) ;; Файл XML (* .xml) ;; Файл JSON (* .json) ;; Файл SQL (* .sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2681"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Не удалось открыть &quot;%1&quot;! 

Причина:%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4007"/>
        <source>Chart</source>
        <translation>График</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4024"/>
        <source>Table</source>
        <translation>Таблица</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4041"/>
        <source>Statistic</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4059"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>Не удалось открыть сообщение эл. почты &quot;%1&quot;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4069"/>
        <source>Could not start e-mail client!</source>
        <translation>Не удалось запустить программу эл. почты!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4075"/>
        <source>Select Icon Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4081"/>
        <source>Restart application to apply new icon color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5155"/>
        <source>Colored Symbols</source>
        <translation>Цветные Символы</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5157"/>
        <source>Show Heartrate</source>
        <translation>Показать Ч.С.С.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5193"/>
        <location filename="../MainWindow.cpp" line="5204"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>Символы и линии не могут быть отключены одновременно!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1025"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Измерения:%1 | Нерегулярное сердцебиение:%2  |  движение : %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="552"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Уважаемый Доктор,

Посылаю Вам отчет о моем артериальном давлении за текущий месяц.

С уважением,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1030"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Измерения : 0  |  Нерегулярное сердцебиение: 0  |  движение : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2237"/>
        <location filename="../MainWindow.cpp" line="2287"/>
        <location filename="../MainWindow.cpp" line="2363"/>
        <location filename="../MainWindow.cpp" line="2494"/>
        <source>%1 (Age: %2, Height: %3cm, Weight: %4Kg, BMI: %5)</source>
        <translation>%1 (Возраст:%2, рост:%3cm, вес:%4Kg, BMI:%5)</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2663"/>
        <location filename="../MainWindow.cpp" line="3896"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Успешно импортировано %n запись из источника %1.

     Пользователь : %2
     Пользователь : %3</numerusform>
            <numerusform>Успешно импортировано %n записей из источника %1.

     Пользователь : %2
     Пользователь : %3</numerusform>
            <numerusform>Успешно импортировано %n записей из источника %1.

     Пользователь : %2
     Пользователь : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2667"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Пропущена %n неправильная запись!</numerusform>
            <numerusform>Пропущено %n неправильных записей!</numerusform>
            <numerusform>Пропущено %n неправильных записей!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2672"/>
        <location filename="../MainWindow.cpp" line="3900"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Пропущен %n дубликат записи!</numerusform>
            <numerusform>Пропущено %n дубликатов записей!</numerusform>
            <numerusform>Пропущено %n дубликатов записей!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3051"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Не похоже на базу данных UBPM! 

Возможно, неправильные настройки шифрования / пароль?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3104"/>
        <location filename="../MainWindow.cpp" line="3108"/>
        <source>Export to %1</source>
        <translation>Экспорт в %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3104"/>
        <location filename="../MainWindow.cpp" line="3108"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 файл (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3144"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Не удалось создать &quot;%1&quot;! 

Причина:%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3150"/>
        <source>The database is empty, no records to export!</source>
        <translation>База данных пуста, нет записей для экспорта!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3501"/>
        <source>Morning</source>
        <translation>Утро</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3501"/>
        <source>Afternoon</source>
        <translation>После полудня</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3540"/>
        <source>Week</source>
        <translation>Неделю</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3595"/>
        <source>Quarter</source>
        <translation>Четверть</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3624"/>
        <source>Half Year</source>
        <translation>Полгода</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3642"/>
        <source>Year</source>
        <translation>Год</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3830"/>
        <source>Really delete all records for user %1?</source>
        <translation>Действительно удалить все записи для пользователя %1?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3845"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Все записи для пользователя %1 удалены, а существующая база данных сохранена в «ubpm.sql.bak».</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3953"/>
        <source>Really delete all records?</source>
        <translation>Действительно удалить все записи?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3964"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Все записи удалены, а существующая база данных «ubpm.sql» перемещена в корзину.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4167"/>
        <source>Thanks to all translators:</source>
        <translation>Спасибо всем переводчикам:</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4167"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4187"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation>Пожалуйста, приобретите купон на желаемую сумму через свою учетную запись Amazon, выберите E-Mail to lazyt@mailbox.org в качестве способа доставки и укажите «UBPM» в качестве сообщения. 

Спасибо!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4204"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation>Пожалуйста, отправьте мне запрос по электронной почте на lazyt@mailbox.org, чтобы я мог предоставить вам информацию о моем текущем банковском счете. 

Спасибо!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4251"/>
        <source>Could not load application translation for &quot;%1&quot;.</source>
        <translation>Не удалось загрузить перевод для языка &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4260"/>
        <source>Could not load plugin translation for &quot;%1&quot;.</source>
        <translation>Не удалось загрузить модуль расширения &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4269"/>
        <source>Could not load base translation for &quot;%1&quot;.

Internal base translations (like &quot;Yes/No&quot;) are not available.

Don&apos;t show this warning again?</source>
        <translation>Не удалось загрузить базовый перевод для языка&quot;%1&quot;.

Внутренний базовый перевод (как то &quot;Yes/No&quot;) не доступен.

Не показывать это предупреждение еще раз?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4324"/>
        <location filename="../MainWindow.cpp" line="4328"/>
        <location filename="../MainWindow.cpp" line="4332"/>
        <location filename="../MainWindow.h" line="209"/>
        <location filename="../MainWindow.h" line="213"/>
        <location filename="../MainWindow.h" line="217"/>
        <source>Minimum</source>
        <translation>Минимум</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4325"/>
        <location filename="../MainWindow.cpp" line="4329"/>
        <location filename="../MainWindow.cpp" line="4333"/>
        <location filename="../MainWindow.h" line="210"/>
        <location filename="../MainWindow.h" line="214"/>
        <location filename="../MainWindow.h" line="218"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4398"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Не удалось открыть файл темы &quot;%1&quot;! 

Причина:%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5149"/>
        <source>Dynamic Scaling</source>
        <translation>Динамическое масштабирование</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5150"/>
        <source>Colored Stripes</source>
        <translation>Цветные полосы</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5152"/>
        <source>Show Symbols</source>
        <translation>Показать символы</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5153"/>
        <source>Show Lines</source>
        <translation>Показать Линии</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5232"/>
        <source>Show record</source>
        <translation>Показать запись</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5265"/>
        <source>Show Median</source>
        <translation>Показать медиану</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5266"/>
        <source>Show Values</source>
        <translation>Показать значения</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5317"/>
        <source>Really quit program?</source>
        <translation>Действительно выйти из программы?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Универсальный менеджер артериального давления</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
