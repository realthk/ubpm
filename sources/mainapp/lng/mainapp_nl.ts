<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Data analyse</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Vraag</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="206"/>
        <source>Results</source>
        <translation>Resultaten</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation>Pulsdruk</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation>Hartslag</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation>Onregelmatig</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Movement</source>
        <translation>Beweging</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Invisible</source>
        <translation>Onzichtbaar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="169"/>
        <source>Comment</source>
        <translation>Kommentaar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="199"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="48"/>
        <source>Could not create memory database!

%1</source>
        <translation>Kon geen database in geheugen aanmaken 

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="208"/>
        <source>No results for this query found!</source>
        <translation>Geen resultaat gevonden voor deze aanvraag!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 voor %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Resultaat</numerusform>
            <numerusform>Resultaten</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>User %1</source>
        <translation>Gebruiker %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Treffer</numerusform>
            <numerusform>Treffers</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="214"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Vermelding</numerusform>
            <numerusform>Vermeldingen</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Handboek</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="60"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>%1 Handboek niet gevonden, toon EN in plaats daarvan.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="52"/>
        <source>%1 guide not found!</source>
        <translation>%1 Handboek niet gevonden!</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Data migratie</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Bron</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Datum formaat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation>Onregelmatigheid in de herkenning</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation>Startlijn</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Elementen per lijn</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Scheidingsteken</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Tijdformaat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation>Bewegingsherkenning</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation>Datumtaal</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation>Posities</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation>Onregelmatigheid</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation>Hartslag</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation>Beweging</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation type="unfinished">Kommentaar</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Resultaat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="783"/>
        <source>Migrate User 1</source>
        <translation>Gebruiker 1 migreren</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="812"/>
        <source>Migrate User 2</source>
        <translation>Gebruiker 2 migreren</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="838"/>
        <source>Copy to Custom</source>
        <translation>Naar gebruiker gedefinieerd kopieren</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="858"/>
        <source>Select predefined settings</source>
        <translation>Selecteer voorgedefinieerde instellingen</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="892"/>
        <source>Custom</source>
        <translation>Aangepast</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="903"/>
        <source>Test Data Migration</source>
        <translation>Test van de data migratie</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="917"/>
        <source>Start Data Migration</source>
        <translation>Start van de data migratie</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="250"/>
        <source>Choose Data Source for Migration</source>
        <translation>Kies de bron-data voor migratie</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="250"/>
        <source>CSV File (*.csv)</source>
        <translation>CSV bestand (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="303"/>
        <source>%L1 Bytes</source>
        <translation>%L1 Bytes</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%1 Lines</source>
        <translation>%1 Lijnen</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="318"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Kan &quot;%1&quot; niet openen!

Reden: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="340"/>
        <source>Startline can&apos;t be empty.</source>
        <translation>Startlijn mag niet leeg zijn.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="346"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation>De startlijn moet kleiner zijn dan het aantal lijnen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="354"/>
        <source>Line %1 = %2</source>
        <translation>Lijn %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="359"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>Scheidingsteken mag niet leeg zijn.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="366"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation>Startlijn bevat geen &quot;%1&quot; scheidingsteken.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="377"/>
        <source>Elements can&apos;t be empty.</source>
        <translation>Elementen kunnen niet leeg zijn.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="383"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation>Het aantal elementen kan niet kleiner als 4 zijn.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="389"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation>Elementen (items) komen niet overeen (gevonden%1).</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="396"/>
        <source>Date format can&apos;t be empty.</source>
        <translation>Het datumformaat kan niet leeg zijn.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="402"/>
        <source>Single date format must also contain a time format.</source>
        <translation>Een enkele datumnotatie moet ook een tijdnotatie bevatten.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="409"/>
        <source>Date position can&apos;t be empty.</source>
        <translation>Een datum positie kan niet leeg zijn.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="415"/>
        <source>Date position must be smaller than elements.</source>
        <translation>De datumpositie moet kleiner zijn als de elementen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="422"/>
        <source>Time position requires also a time parameter.</source>
        <translation>Tijdpositie vereist ook een tijdparameter.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="428"/>
        <source>Time format requires also a time position.</source>
        <translation>Tijdformaat vereist ook een tijdpositie.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="434"/>
        <source>Time position must be smaller than elements.</source>
        <translation>Tijdpositie moet kleiner zijn als de elementen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="441"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation>De systolische positie kan niet leeg zijn.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="447"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation>De sytolische positie moet kleiner zijn als de elementen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="454"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation>Diastolische positie kan niet leeg zijn.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="460"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation>Diastolische positie moet kleiner zijn als de elementen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="467"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation>De hartslagpositie kan niet leeg zijn.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="473"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation>De hartslagpositie moet kleiner zijn als de elementen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="480"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation>De onregelmatige positie moet kleiner zijn als de elementen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="486"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation>Onregelmatige positie vereist ook een onregelmatige parameter.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="493"/>
        <source>Movement position must be smaller than elements.</source>
        <translation>De bewegingspositie moet kleiner zijn als de elementen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="499"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation>De bewegingspositie vereist ook een bewegingsparameter.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="506"/>
        <source>Comment position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="522"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation>Posities kunnen niet meermaals voorkomen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="538"/>
        <source>Date format is invalid.</source>
        <translation>Datumnotatie is ongeldig.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="545"/>
        <source>Time format is invalid.</source>
        <translation>Het tijdformaat is ongeldig.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="555"/>
        <source>Date/Time format is invalid.</source>
        <translation>Het datum/tijd-formaat is ongeldig.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="745"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation>
            <numerusform>Succesvol %n record voor gebruiker %1 gemigreerd.</numerusform>
            <numerusform>Succesvol %n records voor gebruiker %1 gemigreerd.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="749"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n ongeldige record overgeslagen!</numerusform>
            <numerusform>%n ongeldige records overgeslagen!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="754"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n meervoudige record overgeslagen!</numerusform>
            <numerusform>%n meervoudige records overgeslagen!</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Handmatige opname</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Gegevensrecord</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Gegevensrecord voor %1 toevoegen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Kies datum en tijd</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>SYS ingeven</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>DIA ingeven</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Hartslag ingeven</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Onregelmatige hartslag</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Beweging</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Optioneel Kommentaarveld</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Toon bericht bij succesvol creeren / verwijderen / veranderen van gegevensrecord</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Creeren</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Gebruiker 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Gebruiker 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="33"/>
        <location filename="../DialogRecord.cpp" line="186"/>
        <source>Modify</source>
        <translation>Veranderen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="131"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>De gegevensrecord kon niet verwijderd worden!

Geen record gevonden voor deze datum en tijd.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="137"/>
        <source>Data record successfully deleted.</source>
        <translation>De gegevensrecord is succesvol verwijderd.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="149"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Een geldige waarde voor &quot;SYS&quot; ingeven aub!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="159"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Eerst een geldige waarde voor &quot;DIA&quot; ingeven aub!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="168"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Eerst een geldige waarde voor &quot;Hartslag&quot; ingeven aub!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="190"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>De gegevensrecord kon niet veranderd worden!

Geen record voor deze datum en tijd gevonden.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="196"/>
        <source>Data Record successfully modified.</source>
        <translation>Gegevensrecord succesvol veranderd.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="205"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>De gegevensrecord kon niet aangemaakt worden!

Er bestaat reeds een record voor deze datum/tijd combinatie.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="211"/>
        <source>Data Record successfully created.</source>
        <translation>Gegevensrecord succesvol aangemaakt.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="159"/>
        <source>Choose Database Location</source>
        <translation>Kies database locatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="169"/>
        <source>Choose Database Backup Location</source>
        <translation>Selecteer database back-up locatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="205"/>
        <source>Could not display manual!

%1</source>
        <translation>Het handboek kan niet getoond worden!

%1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="328"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation>De geselecteerde database bestaat al.

Selecteer de gewenste actie.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="330"/>
        <source>Overwrite</source>
        <translation>Overschrijven</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="331"/>
        <source>Merge</source>
        <translation>Samenvoegen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="332"/>
        <source>Swap</source>
        <translation>Omwisselen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="334"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation>Overschrijven:

Overschrijf de geselecteerde database met de huidige database.

Samenvoegen:

Voegt de geselecteerde database samen met de huidige database.

Omwisselen:

Verwijdert de huidige database en gebruik de geselecteerde database.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="342"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation>De huidige database is leeg.

De geselecteerde database wordt bij het afsluiten verwijderd als er geen gegevens worden toegevoegd.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="372"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>SQL-codering kan niet worden ingeschakeld zonder wachtwoord en wordt uitgeschakeld!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="379"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation>De database back-up moet zich op een andere harde schijf, partitie of map bevinden.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="384"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Geldige waarden voor additionele informatie voor gebruiker 1 ingeven aub!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="391"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Geldige waarden voor additionele informatie voor gebruiker 2 ingeven aub!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="398"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>De ingevoerde leeftijd komt niet overeen met de geselecteerde leeftijdsgroep voor gebruiker 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="405"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>De ingevoerde leeftijd komt niet overeen met de geselecteerde leeftijdsgroep voor gebruiker 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="413"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Schakel alstublieft symbolen of lijnen in voor grafiek!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="420"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Gelieve een geldig e-mailadres in te geven!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="427"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Gelieve een geldig e-mail onderwerp in te geven!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="434"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>E-Mail bericht moet $CHART, $TABLE en/of $STATS bevatten!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="545"/>
        <source>User 1</source>
        <translation>Gebruiker 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="559"/>
        <source>User 2</source>
        <translation>Gebruiker 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="672"/>
        <source>Blood Pressure Report</source>
        <translation>Bloeddrukbericht</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="673"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Geachte Dr. House,

bijgevoegd vindt u mijn bloeddrukgegevens van deze maand.

Met vriendelijke groet, 
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="715"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Installatie afbreken en alle wijzigingen negeren?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="52"/>
        <source>Location</source>
        <translation>Locatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="58"/>
        <source>Current Location</source>
        <translation>Huidige locatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="74"/>
        <source>Change Location</source>
        <translation>Locatie veranderen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="100"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Versleutel met SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="103"/>
        <source>Encryption</source>
        <translation>Versleutelen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="115"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="128"/>
        <source>Show Password</source>
        <translation>Wachtwoord tonen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="282"/>
        <source>User</source>
        <translation>Gebruiker</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="317"/>
        <location filename="../DialogSettings.ui" line="580"/>
        <source>Male</source>
        <translation>Man</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="346"/>
        <location filename="../DialogSettings.ui" line="609"/>
        <source>Female</source>
        <translation>Vrouw</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="442"/>
        <location filename="../DialogSettings.ui" line="714"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <location filename="../DialogSettings.ui" line="574"/>
        <source>Mandatory Information</source>
        <translation>Verplicht veld</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="372"/>
        <location filename="../DialogSettings.ui" line="644"/>
        <source>Age Group</source>
        <translation>Leeftijdsgroep</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="458"/>
        <location filename="../DialogSettings.ui" line="730"/>
        <source>Additional Information</source>
        <translation>Extra informatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Height</source>
        <translation>Lengte</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="547"/>
        <location filename="../DialogSettings.ui" line="819"/>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="844"/>
        <source>Device</source>
        <translation>Apparaat</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="850"/>
        <location filename="../DialogSettings.cpp" line="62"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Plug-ins importeren [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="880"/>
        <source>Show Device Image</source>
        <translation>Apparaatafbeelding weergeven</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="903"/>
        <source>Show Device Manual</source>
        <translation>Apparaathandleiding weergeven</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="948"/>
        <source>Open Website</source>
        <translation>Website openen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="961"/>
        <source>Maintainer</source>
        <translation>Onderhouder</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="989"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="996"/>
        <source>Send E-Mail</source>
        <translation>E-mail versturen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1009"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1016"/>
        <source>Producer</source>
        <translation>Fabrikant</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1033"/>
        <source>Chart</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1160"/>
        <source>X-Axis Range</source>
        <translation>X-as bereik</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1166"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamisch schalen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1147"/>
        <source>Healthy Ranges</source>
        <translation>Gezond bereik</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1064"/>
        <source>Symbols</source>
        <translation>Symbolen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1141"/>
        <source>Colored Areas</source>
        <translation>Gekleurde gebieden</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="865"/>
        <source>Please choose Device Plugin…</source>
        <translation>Kies a.u.b. Device Plugin …</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1288"/>
        <location filename="../DialogSettings.ui" line="1563"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1376"/>
        <location filename="../DialogSettings.ui" line="1651"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1464"/>
        <location filename="../DialogSettings.ui" line="1739"/>
        <source>Heartrate</source>
        <translation>Hartslag</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1836"/>
        <source>Table</source>
        <translation>Tabel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1921"/>
        <location filename="../DialogSettings.ui" line="2104"/>
        <source>Systolic Warnlevel</source>
        <translation>Systolisch waarschuwingsniveau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1867"/>
        <location filename="../DialogSettings.ui" line="2164"/>
        <source>Diastolic Warnlevel</source>
        <translation>Diastolisch waarschuwingsniveau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="153"/>
        <source>Automatic Backup</source>
        <translation>Automatische back-up</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="156"/>
        <source>Backup</source>
        <translation>Back-up</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="173"/>
        <source>Current Backup Location</source>
        <translation>Actuele back-up locatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="186"/>
        <source>Change Backup Location</source>
        <translation>Verander de back-up locatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="213"/>
        <source>Backup Mode</source>
        <translation>Back-up mode</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="223"/>
        <source>Daily</source>
        <translation type="unfinished">Per dag</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="232"/>
        <source>Weekly</source>
        <translation>Per week</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="241"/>
        <source>Monthly</source>
        <translation>Per maand</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="253"/>
        <source>Keep Copies</source>
        <translation>Kopiën bewaren</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="486"/>
        <location filename="../DialogSettings.ui" line="758"/>
        <source>Birthday</source>
        <translation>Verjaardag</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1073"/>
        <source>Color</source>
        <translation>Kleur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1097"/>
        <location filename="../DialogSettings.ui" line="1125"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1179"/>
        <source>Lines</source>
        <translation>Lijnen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1188"/>
        <location filename="../DialogSettings.ui" line="1216"/>
        <source>Width</source>
        <translation>breedte</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1245"/>
        <source>Show Heartrate</source>
        <translation>Hartslag tonen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1251"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Hartslag weergeven in kaartweergave</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1261"/>
        <source>Print Heartrate</source>
        <translation>Hartslag afdrukken</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1267"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Druk de hartslag af op een aparte bladzijde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1981"/>
        <location filename="../DialogSettings.ui" line="2218"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Pulsdruk waarschuwingsniveau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2035"/>
        <location filename="../DialogSettings.ui" line="2272"/>
        <source>Heartrate Warnlevel</source>
        <translation>Hartslag waarschuwingsniveau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2337"/>
        <source>Statistic</source>
        <translation>Statistiek</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2343"/>
        <source>Bar Type</source>
        <translation>Type staaf</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2349"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Toon mediaan in plaats van gemiddelde balken</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2359"/>
        <source>Legend Type</source>
        <translation>Type legende</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2365"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Toon waarden als legenda in plaats van beschrijvingen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2380"/>
        <source>E-Mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2388"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2407"/>
        <source>Subject</source>
        <translation>Onderwerp</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2425"/>
        <source>Message</source>
        <translation>Bericht</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2446"/>
        <source>Plugin</source>
        <translation>Plug-in</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2452"/>
        <source>General</source>
        <translation>Algemeen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2458"/>
        <source>Log Device Communication to File</source>
        <translation>Apparaatcommunicatie naar bestand loggen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Import Measurements automatically</source>
        <translation>Meetwaarden automatisch importeren</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2475"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2481"/>
        <source>Discover Device automatically</source>
        <translation>Apparaat automatisch ontdekken</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2488"/>
        <source>Connect Device automatically</source>
        <translation>Apparaat automatisch verbinden</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2503"/>
        <source>Update</source>
        <translation>Aanpassen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2509"/>
        <source>Autostart</source>
        <translation>Automatische start</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2515"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Controleer op online updates bij het opstarten van het programma</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2528"/>
        <source>Notification</source>
        <translation>Kennisgeving</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2534"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Resultaat altijd weergeven na controle van online updates</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2550"/>
        <source>Save</source>
        <translation>Opslaan</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2567"/>
        <source>Reset</source>
        <translation>Terugzetten</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2584"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Online update</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Beschikbare Versie</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Update bestandsgrootte</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Geinstalleerde versie</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="199"/>
        <source>Download</source>
        <translation>Downloaden</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="219"/>
        <source>Ignore</source>
        <translation>negeren</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="167"/>
        <source>No new version found.</source>
        <translation>Geen nieuwe versie gevonden.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! SSL WAARSCHUWING - ZORGVULDIG LEZEN !!!

Netwerk verbindingsprobleem:

%1
Wil je toch doorgaan?</numerusform>
            <numerusform>!!! SSL WAARSCHUWING - ZORGVULDIG LEZEN !!!

Netwerk verbindingsproblemen:

%1
Wil je toch doorgaan?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Downloading update failed!

%1</source>
        <translation>Het downloaden van de update is mislukt!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Checking update failed!

%1</source>
        <translation>Het controleren van de update is mislukt!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="140"/>
        <source>Unexpected response from update server!</source>
        <translation>Onverwachte reactie van updateserver!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="157"/>
        <source>*%1 not found</source>
        <translation>*%1 niet gevonden</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="187"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>Update heeft niet de verwachte grootte!

%L1:%L2

Opnieuw proberen…</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="191"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Update opgeslagen in %1

Nieuwe versie starten?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="204"/>
        <source>Could not start new version!</source>
        <translation>Kan nieuwe versie niet starten!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>Really abort download?</source>
        <translation>Download echt afbreken?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="211"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Kan update niet opslagen in %1

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universele Bloeddruk Manager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Toon de vorige periode</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Volgende periode tonen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Grafiekweergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Tabel weergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="126"/>
        <location filename="../MainWindow.cpp" line="4313"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="127"/>
        <location filename="../MainWindow.cpp" line="4314"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pulsdruk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Onregelmatig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Beweging</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Onzichtbaar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Kommentaar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Statistiek weergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>per kwartier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>per half uur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>per uur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>¼ Dag</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>½ Dag</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Per dag</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation>per week</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation>per maand</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation>per kwartaal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation>halfjaarlijks</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation>Jaarlijks</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation>Laatste 7 dagen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation>Laatste 14 dagen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation>Laatste 21 dagen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation>Laatste 28 dagen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation>Laatste 3 maanden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation>Laatste 6 maanden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation>Laatste 9 maanden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation>Laatste 12 maanden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation>Alle records</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation>Afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Configuration</source>
        <translation>Konfiguratie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1161"/>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1170"/>
        <source>Language</source>
        <translation>Taal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1179"/>
        <source>Style</source>
        <translation>Stijl</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1188"/>
        <source>Charts</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1213"/>
        <source>Database</source>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1217"/>
        <source>Import</source>
        <translation>Importeren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1229"/>
        <source>Export</source>
        <translation>Exporteren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1244"/>
        <source>Clear</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1331"/>
        <source>Quit</source>
        <translation>Stoppen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1334"/>
        <location filename="../MainWindow.ui" line="1337"/>
        <source>Quit Program</source>
        <translation>Programma stoppen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1349"/>
        <source>About</source>
        <translation>Over</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1352"/>
        <location filename="../MainWindow.ui" line="1355"/>
        <source>About Program</source>
        <translation>Over dit programma</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1364"/>
        <source>Guide</source>
        <translation>Handboek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1367"/>
        <location filename="../MainWindow.ui" line="1370"/>
        <source>Show Guide</source>
        <translation>Toon handboek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Update</source>
        <translation>Aktualiseren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Check Update</source>
        <translation>Controleer Update</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1544"/>
        <source>To PDF</source>
        <translation>Naar PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Export To PDF</source>
        <translation>Exporteren naar PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1559"/>
        <source>Migration</source>
        <translation>Migratie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Migrate from Vendor</source>
        <translation>Migreren van de fabrikant</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1809"/>
        <source>PayPal</source>
        <translation>PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1812"/>
        <location filename="../MainWindow.ui" line="1815"/>
        <source>Donate via PayPal</source>
        <translation>Schenken via PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1827"/>
        <location filename="../MainWindow.ui" line="1830"/>
        <source>Donate via Liberapay</source>
        <translation>Schenken via Liberapay</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Donate via Amazon</source>
        <translation>Schenken via Amazon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <location filename="../MainWindow.ui" line="1860"/>
        <source>Donate via SEPA</source>
        <translation>Schenken via SEPA</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1869"/>
        <source>Translation</source>
        <translation>Vertaling</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <location filename="../MainWindow.ui" line="1875"/>
        <source>Contribute Translation</source>
        <translation>Meehelpen aan de vertaling</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1884"/>
        <source>E-Mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1887"/>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Send E-Mail</source>
        <translation>E-mail versturen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1902"/>
        <source>Icons</source>
        <translation>Symbool</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1905"/>
        <location filename="../MainWindow.ui" line="1908"/>
        <source>Change Icon Color</source>
        <translation>Verander de symboolkleur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1919"/>
        <source>System</source>
        <translation>Systeem</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1922"/>
        <location filename="../MainWindow.ui" line="1925"/>
        <source>System Colors</source>
        <translation>Systeemkleuren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1933"/>
        <source>Light</source>
        <translation>Licht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1936"/>
        <location filename="../MainWindow.ui" line="1939"/>
        <source>Light Colors</source>
        <translation>Felle kleuren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1947"/>
        <source>Dark</source>
        <translation>Donker</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <location filename="../MainWindow.ui" line="1953"/>
        <source>Dark Colors</source>
        <translation>Donkere kleuren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1132"/>
        <source>Donation</source>
        <translation>Schenken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="128"/>
        <location filename="../MainWindow.cpp" line="4315"/>
        <source>Heartrate</source>
        <translation>Hartslag</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1129"/>
        <source>Make Donation</source>
        <translation>Schenken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>Bugreport</source>
        <translation>Foutrapport</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>Send Bugreport</source>
        <translation>Foutrapport sturen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Change Settings</source>
        <translation>Instellingen veranderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>From Device</source>
        <translation>Van Apparaat</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1436"/>
        <location filename="../MainWindow.ui" line="1439"/>
        <source>Import From Device</source>
        <translation>Importeren van Apparaat</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>From File</source>
        <translation>Van File</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1454"/>
        <location filename="../MainWindow.ui" line="1457"/>
        <source>Import From File</source>
        <translation>Importeren van File</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>From Input</source>
        <translation>Van Input</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1469"/>
        <location filename="../MainWindow.ui" line="1472"/>
        <source>Import From Input</source>
        <translation>Importeren van Input</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>To CSV</source>
        <translation>Naar CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Export To CSV</source>
        <translation>Exporteren naar CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>To XML</source>
        <translation>Naar XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Export To XML</source>
        <translation>Exporteren naar XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>To JSON</source>
        <translation>Naar JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Export To JSON</source>
        <translation>Exporteren naar JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To SQL</source>
        <translation>Naar SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To SQL</source>
        <translation>Exporteren naar SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>Print Chart</source>
        <translation>Grafiek afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Print Chart View</source>
        <translation>Grafiekweergave afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>Print Table</source>
        <translation>Tabel afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Print Table View</source>
        <translation>Tabelweergave afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Print Statistic</source>
        <translation>Statistiek afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Print Statistic View</source>
        <translation>Statistiekweergave afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1622"/>
        <source>Preview Chart</source>
        <translation>Voorbeeld grafiek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <location filename="../MainWindow.ui" line="1628"/>
        <source>Preview Chart View</source>
        <translation>Voorbeeld van grafiekweergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1637"/>
        <source>Preview Table</source>
        <translation>Voorbeeld tabel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <location filename="../MainWindow.ui" line="1643"/>
        <source>Preview Table View</source>
        <translation>Voorbeeld van tabelweergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1652"/>
        <source>Preview Statistic</source>
        <translation>Voorbeeld statistiek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <location filename="../MainWindow.ui" line="1658"/>
        <source>Preview Statistic View</source>
        <translation>Voorbeeld van statistiekweergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1667"/>
        <location filename="../MainWindow.ui" line="1670"/>
        <location filename="../MainWindow.ui" line="1673"/>
        <source>Clear All</source>
        <translation>Alles verwijderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Clear User 1</source>
        <translation>Gebruiker 1 verwijderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Clear User 2</source>
        <translation>Gebruiker 2 verwijderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1766"/>
        <source>Analysis</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1769"/>
        <location filename="../MainWindow.ui" line="1772"/>
        <source>Analyze Records</source>
        <translation>Records analyseren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1791"/>
        <location filename="../MainWindow.ui" line="1794"/>
        <location filename="../MainWindow.ui" line="1797"/>
        <source>Time Mode</source>
        <translation>Tijd-mode</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="76"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="4283"/>
        <location filename="../MainWindow.cpp" line="4284"/>
        <source>Records For Selected User</source>
        <translation>Records voor geselecteerde gebruiker</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="84"/>
        <location filename="../MainWindow.cpp" line="85"/>
        <location filename="../MainWindow.cpp" line="4286"/>
        <location filename="../MainWindow.cpp" line="4287"/>
        <source>Select Date &amp; Time</source>
        <translation>Datum en tijd kiezen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="136"/>
        <location filename="../MainWindow.cpp" line="4317"/>
        <source>Systolic - Value Range</source>
        <translation>Systolisch - Waardebereik</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="137"/>
        <location filename="../MainWindow.cpp" line="4318"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastolisch - Waardebereik</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="140"/>
        <location filename="../MainWindow.cpp" line="4320"/>
        <source>Systolic - Target Area</source>
        <translation>Systolisch - Doelgebied</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="141"/>
        <location filename="../MainWindow.cpp" line="4321"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastolisch - Doelgebied</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1025"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Meetwaarden : %1  |  Onregelmatig : %2  |  Beweging : %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1030"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Meetwaarden : 0  |  Onregelmatig : 0  |  Beweging : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <source>Athlete</source>
        <translation>Athletisch</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1515"/>
        <source>Excellent</source>
        <translation>Uitstekend</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1515"/>
        <source>Optimal</source>
        <translation>Optimaal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1516"/>
        <source>Great</source>
        <translation>Zeer goed</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1517"/>
        <source>Good</source>
        <translation>Goed</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1516"/>
        <source>Normal</source>
        <translation>Normaal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1727"/>
        <location filename="../MainWindow.ui" line="1730"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="4094"/>
        <location filename="../MainWindow.cpp" line="4095"/>
        <location filename="../MainWindow.cpp" line="4096"/>
        <location filename="../MainWindow.cpp" line="4097"/>
        <location filename="../MainWindow.cpp" line="4347"/>
        <location filename="../MainWindow.cpp" line="4348"/>
        <location filename="../MainWindow.cpp" line="4349"/>
        <location filename="../MainWindow.cpp" line="4350"/>
        <source>Switch To %1</source>
        <translation>Naar %1 omschakelen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="488"/>
        <location filename="../MainWindow.cpp" line="4094"/>
        <location filename="../MainWindow.cpp" line="4095"/>
        <location filename="../MainWindow.cpp" line="4347"/>
        <location filename="../MainWindow.cpp" line="4348"/>
        <source>User 1</source>
        <translation>Gebruiker 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="498"/>
        <location filename="../MainWindow.cpp" line="4096"/>
        <location filename="../MainWindow.cpp" line="4097"/>
        <location filename="../MainWindow.cpp" line="4349"/>
        <location filename="../MainWindow.cpp" line="4350"/>
        <source>User 2</source>
        <translation>Gebruiker 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="138"/>
        <location filename="../MainWindow.cpp" line="4319"/>
        <source>Heartrate - Value Range</source>
        <translation>Hartslag - Waardebereik</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="142"/>
        <location filename="../MainWindow.cpp" line="4322"/>
        <source>Heartrate - Target Area</source>
        <translation>Hartslag - Doelgebied</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="354"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation>Kan geen back-up folder creëren

Kontroleer de back-up folder locatie.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="383"/>
        <location filename="../MainWindow.cpp" line="406"/>
        <source>Could not backup database:

%1</source>
        <translation>Kan geen back-up maken van de database:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="396"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation>Kan de verouderde back-up niet verwijderen:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="423"/>
        <source>Could not register icons.</source>
        <translation>Symbolen konden niet geregistreerd worden</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="551"/>
        <source>Blood Pressure Report</source>
        <translation>Bloeddrukbericht</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1026"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4 | DIA : Ø %2 / x̃ %5 | Hartslag : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1031"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  Hartslag : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <source>Low</source>
        <translation>Laag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1517"/>
        <source>High Normal</source>
        <translation>Hoog normaal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1518"/>
        <location filename="../MainWindow.cpp" line="1985"/>
        <location filename="../MainWindow.cpp" line="4326"/>
        <location filename="../MainWindow.cpp" line="4330"/>
        <location filename="../MainWindow.cpp" line="4334"/>
        <location filename="../MainWindow.h" line="211"/>
        <location filename="../MainWindow.h" line="215"/>
        <location filename="../MainWindow.h" line="219"/>
        <source>Average</source>
        <translation>Gemiddeld</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1518"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <source>Below Average</source>
        <translation>Lager dan het gemiddelde</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <source>Poor</source>
        <translation>Slecht</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1665"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>Het scannen van de importplug-in &quot;%1&quot; is mislukt!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1678"/>
        <location filename="../MainWindow.cpp" line="1700"/>
        <location filename="../MainWindow.cpp" line="4293"/>
        <source>Switch Language to %1</source>
        <translation>Taal wisselen naar %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1725"/>
        <location filename="../MainWindow.cpp" line="1740"/>
        <location filename="../MainWindow.cpp" line="4301"/>
        <source>Switch Theme to %1</source>
        <translation>Thema wisselen naar %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1765"/>
        <location filename="../MainWindow.cpp" line="1785"/>
        <location filename="../MainWindow.cpp" line="4309"/>
        <source>Switch Style to %1</source>
        <translation>Stijl wisselen naar %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1856"/>
        <location filename="../MainWindow.cpp" line="1881"/>
        <source>Edit record</source>
        <translation>Record aanpassen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1985"/>
        <location filename="../MainWindow.cpp" line="4327"/>
        <location filename="../MainWindow.cpp" line="4331"/>
        <location filename="../MainWindow.cpp" line="4335"/>
        <location filename="../MainWindow.h" line="212"/>
        <location filename="../MainWindow.h" line="216"/>
        <location filename="../MainWindow.h" line="220"/>
        <source>Median</source>
        <translation>Mediaan</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1987"/>
        <source>Click to swap Average and Median</source>
        <translation>Klik om Gemiddelde en Mediaan te wisselen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2082"/>
        <source>Click to swap Legend and Label</source>
        <translation>Klik om legende en label om te wisselen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2159"/>
        <source>No records to preview for selected time range!</source>
        <translation>Geen records beschikbaar voor het geselecteerde tijdbereik!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2191"/>
        <source>No records to print for selected time range!</source>
        <translation>Geen records om af te drukken voor het geselecteerde tijdbereik!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2237"/>
        <location filename="../MainWindow.cpp" line="2287"/>
        <location filename="../MainWindow.cpp" line="2363"/>
        <location filename="../MainWindow.cpp" line="2494"/>
        <source>User %1</source>
        <translation>Gebruiker %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>DATE</source>
        <translation>DATUM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>TIME</source>
        <translation>TIJD</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>BPM</source>
        <translation>Hartslag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>IHB</source>
        <translation>OHS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>COMMENT</source>
        <translation>KOMMENTAAR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>PPR</source>
        <translation>PDR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2399"/>
        <source>MOV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2559"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>Kan geen e-mail maken omdat het genereren van Base64 voor bijlage &quot;%1&quot; is mislukt! 

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4007"/>
        <source>Chart</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4024"/>
        <source>Table</source>
        <translation>Tabel</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3294"/>
        <location filename="../MainWindow.cpp" line="4041"/>
        <source>Statistic</source>
        <translation>Statistiek</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4059"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>Kan E-mail &quot;%1&quot; niet openen!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4069"/>
        <source>Could not start e-mail client!</source>
        <translation>Kan E-mail client niet starten!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4075"/>
        <source>Select Icon Color</source>
        <translation>Symboolkleur kiezen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4081"/>
        <source>Restart application to apply new icon color.</source>
        <translation>Applicatie opnieuw starten om de nieuwe symboolkleuren te gebruiken</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4167"/>
        <source>Thanks to all translators:</source>
        <translation>Dank aan alle vertalers:</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4167"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4187"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation>Koop een voucher in het gewenste bedrag via uw Amazon-account, selecteer een E-mail aan lazyt@mailbox.org als leveringsmethode en specificeer &quot;UBPM&quot; als bericht. 

Hartelijk bedankt!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4204"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation>Stuur me een e-mail aan lazyt@mailbox.org zodat ik je mijn bankrekeninggegevens kan meedelen.

Hartelijk bedankt!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2231"/>
        <location filename="../MainWindow.cpp" line="2281"/>
        <location filename="../MainWindow.cpp" line="2357"/>
        <location filename="../MainWindow.cpp" line="2488"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Gemaakt met UBPM voor
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="552"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Geachte Dr. House,

bijgevoegd vindt u mijn bloeddrukgegevens van deze maand.

Met vriendelijke groet, 
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2232"/>
        <location filename="../MainWindow.cpp" line="2282"/>
        <location filename="../MainWindow.cpp" line="2358"/>
        <location filename="../MainWindow.cpp" line="2489"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Gratis en OpenSource 
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2237"/>
        <location filename="../MainWindow.cpp" line="2287"/>
        <location filename="../MainWindow.cpp" line="2363"/>
        <location filename="../MainWindow.cpp" line="2494"/>
        <source>%1 (Age: %2, Height: %3cm, Weight: %4Kg, BMI: %5)</source>
        <translation>%1 (Leeftijd: %2, Grootte: %3cm, Gewicht: %4Kg, BMI: %5)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2625"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importeren van CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2625"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>CSV Bestand (*.csv);;XML Bestand (*.xml);;JSON Bestand (*.json);;SQL Bestand (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2681"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Kan &quot;%1&quot; niet openen!

Reden: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3051"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Ziet er niet uit naar een UBPM-database!

Misschien verkeerde coderingsinstellingen / wachtwoord?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3104"/>
        <location filename="../MainWindow.cpp" line="3108"/>
        <source>Export to %1</source>
        <translation>Exporteren als %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3104"/>
        <location filename="../MainWindow.cpp" line="3108"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 File (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3144"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Kan &quot;%1&quot; niet creeren!

Reden: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3150"/>
        <source>The database is empty, no records to export!</source>
        <translation>De database is leeg, er zijn geen records om te exporteren!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3501"/>
        <source>Morning</source>
        <translation>Voormiddag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3501"/>
        <source>Afternoon</source>
        <translation>Namiddag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3540"/>
        <source>Week</source>
        <translation>Week</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3595"/>
        <source>Quarter</source>
        <translation>Kwartaal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3624"/>
        <source>Half Year</source>
        <translation>Half jaar</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3642"/>
        <source>Year</source>
        <translation>Jaar</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3830"/>
        <source>Really delete all records for user %1?</source>
        <translation>Ben je zeker om alle records voor gebruiker %1 te verwijderen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3845"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Alle records voor gebruiker %1 zijn verwijderd en de bestaande database is opgeslagen in &quot;ubpm.sql.bak&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3953"/>
        <source>Really delete all records?</source>
        <translation>Ben je zeker alle records te willen verwijderen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3964"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Alle records zijn verwijderd en de bestaande database is verplaatst naar &quot;ubpm.sql.bak&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4324"/>
        <location filename="../MainWindow.cpp" line="4328"/>
        <location filename="../MainWindow.cpp" line="4332"/>
        <location filename="../MainWindow.h" line="209"/>
        <location filename="../MainWindow.h" line="213"/>
        <location filename="../MainWindow.h" line="217"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4325"/>
        <location filename="../MainWindow.cpp" line="4329"/>
        <location filename="../MainWindow.cpp" line="4333"/>
        <location filename="../MainWindow.h" line="210"/>
        <location filename="../MainWindow.h" line="214"/>
        <location filename="../MainWindow.h" line="218"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4398"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Kan de thema file &quot;%1&quot; niet openen!

Reden: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1853"/>
        <location filename="../MainWindow.cpp" line="1870"/>
        <location filename="../MainWindow.cpp" line="5230"/>
        <source>Delete record</source>
        <translation>Record verwijderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4251"/>
        <source>Could not load application translation for &quot;%1&quot;.</source>
        <translation>Kan de vertalingstoepassing voor &quot;%1&quot; niet laden.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4260"/>
        <source>Could not load plugin translation for &quot;%1&quot;.</source>
        <translation>Kan de vertalings-plugin voor &quot;%1&quot; niet laden.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4269"/>
        <source>Could not load base translation for &quot;%1&quot;.

Internal base translations (like &quot;Yes/No&quot;) are not available.

Don&apos;t show this warning again?</source>
        <translation>Kan de basis-vertaling voor &quot;%1&quot; niet laden.

Interne basis-vertalingen (zoals &quot;Yes/No&quot;) zijn niet beschikbaar.

Deze waarschuwing niet meer laten zien?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5157"/>
        <source>Show Heartrate</source>
        <translation>Hartslag tonen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5193"/>
        <location filename="../MainWindow.cpp" line="5204"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>Symbolen en lijnen kunnen niet beiden uitgeschakeld zijn!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5232"/>
        <source>Show record</source>
        <translation>Toon gegevensrecord</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1854"/>
        <location filename="../MainWindow.cpp" line="1877"/>
        <location filename="../MainWindow.cpp" line="5233"/>
        <source>Hide record</source>
        <translation>Verberg gegevensrecord</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1872"/>
        <location filename="../MainWindow.cpp" line="5244"/>
        <source>Really delete selected record?</source>
        <translation>Wil je echt de gelecteerde records verwijderen?</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2663"/>
        <location filename="../MainWindow.cpp" line="3896"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Succesvol %n record van %1 geimporteerd.

     Gebruiker 1 : %2
     Gebruiker 2 : %3</numerusform>
            <numerusform>Succesvol %n records van %1 geimporteerd.

     Gebruiker 1 : %2
     Gebruiker 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2667"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n ongeldige record overgeslagen!</numerusform>
            <numerusform>%n ongeldige records overgeslagen!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2672"/>
        <location filename="../MainWindow.cpp" line="3900"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n meervoudige record overgeslagen!</numerusform>
            <numerusform>%n meervoudige records overgeslagen!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5149"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamisch schalen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5150"/>
        <source>Colored Stripes</source>
        <translation>Gekleurde strepen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5152"/>
        <source>Show Symbols</source>
        <translation>Toon symbolen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5153"/>
        <source>Show Lines</source>
        <translation>Lijnen tonen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5155"/>
        <source>Colored Symbols</source>
        <translation>Kleursymbolen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5265"/>
        <source>Show Median</source>
        <translation>Toon mediaan</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5266"/>
        <source>Show Values</source>
        <translation>Waarden weergeven</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5317"/>
        <source>Really quit program?</source>
        <translation>Echt stoppen met programma?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universele Bloeddruk Manager</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation>De applicatie is al actief en meerdere instanties zijn niet toegestaan.

Schakel over naar de actieve instantie of sluit af en probeer het opnieuw.</translation>
    </message>
</context>
</TS>
